/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict';


const { Author, Email } = require('../config')




module.exports = {
    description: 'Tạo màn hình mới',
    prompts: [
        {
            type: 'input',
            name: 'name',
            message: 'Màn hình này tên là gì?',
            default: 'Home'
        },
        {
            type: 'confirm',
            name: 'hasRedux',
            default: false,
            message: 'Bạn có muốn tích hợp redux?',
        }
    ],
    actions: (data) => {
        // throw Error(JSON.stringify(require('os').userInfo()))
        // throw Error(JSON.stringify(data))

        const actions = [
            {
                type: 'add',
                path: '../../src/screens/{{lowerCase name}}/index.tsx',
                templateFile: './screen/container.js.hbs',
                abortOnFail: true,
            }
        ];

        // if (data.hasMultiScreen) {
        //     actions.push(
        //         {
        //             type: 'add',
        //             path: '../../src/views/{{lowerCase name}}/index-portrait.js',
        //             templateFile: './screen/view.js.hbs',
        //             abortOnFail: true,
        //         }
        //     )
        //     actions.push(
        //         {
        //             type: 'add',
        //             path: '../../src/views/{{lowerCase name}}/index-landscape.js',
        //             templateFile: './screen/view.js.hbs',
        //             abortOnFail: true,
        //         }
        //     )
        // } else {
        //     actions.push(
        //         {
        //             type: 'add',
        //             path: '../../src/views/{{lowerCase name}}/index.js',
        //             templateFile: './screen/view.js.hbs',
        //             abortOnFail: true,
        //         }
        //     )
        // }

        if (data.hasRedux) {
            // actions.push(
            //     {
            //         type: 'add',
            //         path: '../../src/reducers/{{lowerCase name}}.js',
            //         templateFile: './screen/reducer.js.hbs',
            //         abortOnFail: true,
            //     }
            // )
            // actions.push({
            //     type: 'modify',
            //     path: '../../src/sagas/index.js',
            //     pattern: /(effects';\n)(?!.*import')/g,
            //     templateFile: './screen/saga-import.js.hbs',
            // })
            // actions.push({
            //     type: 'modify',
            //     path: '../../src/sagas/index.js',
            //     pattern: /(all\(\[\n)(?!.*...')/g,
            //     templateFile: './screen/saga-used.js.hbs',
            // })
            // actions.push(
            //     {
            //         type: 'add',
            //         path: '../../src/actions/{{lowerCase name}}.js',
            //         templateFile: './screen/action.js.hbs',
            //         abortOnFail: true,
            //     }
            // )
            // actions.push(
            //     {
            //         type: 'add',
            //         path: '../../src/sagas/{{lowerCase name}}.js',
            //         templateFile: './screen/saga.js.hbs',
            //         abortOnFail: true,
            //     }
            // )
            // actions.push(
            //     {
            //         type: 'modify',
            //         path: '../../src/reducers/index.js',
            //         pattern: /(combineReducers\(\{\n)(?!.*\n')/g,
            //         templateFile: './screen/reducer-used.js.hbs',
            //     }
            // )
            // actions.push(
            //     {
            //         type: 'modify',
            //         path: '../../src/reducers/index.js',
            //         pattern: /('redux';\n)(?!.*import')/g,
            //         templateFile: './screen/reducer-import.js.hbs',
            //     }
            // )
        }
        // if (data.hasLanguage) {
        //     actions.push(
        //         {
        //             type: 'add',
        //             path: '../../src/languages/{{lowerCase name}}.js',
        //             templateFile: './screen/language.js.hbs',
        //             abortOnFail: true,
        //         }
        //     )
        //     actions.push(
        //         {
        //             type: 'modify',
        //             path: '../../src/languages/index.js',
        //             pattern: /(configlang = \[\n)(?!.*\n')/g,
        //             templateFile: './screen/language-used.js.hbs',
        //         }
        //     )
        // }


        return actions;
    },
};
