/**
 * generator/index.js
 *
 * Exports the generators so plop knows them
 */

const fs = require('fs');
const path = require('path');
const screenGenerator = require('./screen');

module.exports = (plop) => {
    plop.setGenerator('Tạo màn hình', screenGenerator);
    // plop.setGenerator('language', screenGenerator);
    // plop.setGenerator('container', screenGenerator);
};
