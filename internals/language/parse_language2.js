var fs = require('fs'),
    readline = require('readline');
const set = require('set-value');

var rd = readline.createInterface({
    input: fs.createReadStream('./lang2.txt'),
    // output: process.stdout,
    // console: false
});
let langs = [{}, {}, {}]


// const VI = 0
const EN = 0
const KO = 1

rd.on('line', function (line) {
    let lineSplit = line.split('\t')
    if (lineSplit.length == 3) {
        // set(langs[VI], lineSplit[0].trim(), lineSplit[VI + 1].trim())
        set(langs[EN], lineSplit[0].trim(), lineSplit[EN + 1].trim())
        set(langs[KO], lineSplit[0].trim(), lineSplit[KO + 1].trim())
    }
});
rd.on('close', () => {
    // console.log(JSON.stringify(langs))

    // fs.writeFileSync("vi.js", `export default ${JSON.stringify(langs[VI])}`)
    fs.writeFileSync("en.js", `export default ${JSON.stringify(langs[EN])}`)
    fs.writeFileSync("vi.js", `export default ${JSON.stringify(langs[KO])}`)

})