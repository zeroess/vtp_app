var fs = require('fs'),
    readline = require('readline');
const set = require('set-value');

var rd = readline.createInterface({
    input: fs.createReadStream('./lang.txt'),
    // output: process.stdout,
    // console: false
});
let langs = {}
let langsArray = {}

rd.on('line', function (line) {
    let first = 0
    while (first >= 0) {
        let ffffff = line.indexOf('I18n.t(\'', Math.max(0, first))
        if (ffffff >= 0) {
            let key = line.substr(ffffff + 8, line.indexOf('\'', ffffff + 8) - ffffff - 8).trim()

            let firstDefault = line.indexOf('defaultValue: \'')
            let defaultValue = ''
            if (firstDefault == -1) {
                firstDefault = line.indexOf('defaultValue: `')
                defaultValue = firstDefault > 0 ? line.substr(firstDefault + 15, line.indexOf('`', firstDefault + 15) - (firstDefault + 15)) : ''
            } else
                defaultValue = firstDefault > 0 ? line.substr(firstDefault + 15, line.indexOf('\'', firstDefault + 15) - (firstDefault + 15)) : ''

            first = ffffff + 10
            set(langs, key, defaultValue)
            // if (!!langsArray[key]) {
            //     console.log(`trung---`, langsArray[key], defaultValue)
            // }
            langsArray[key] = defaultValue
        } else {
            ffffff = line.indexOf('I18n.t(`', Math.max(0, first))
            if (ffffff >= 0) {
                let key = line.substr(ffffff + 8, line.indexOf('`', ffffff + 8) - ffffff - 8).trim()

                let firstDefault = line.indexOf('defaultValue: \'')
                let defaultValue = ''
                if (firstDefault == -1) {
                    firstDefault = line.indexOf('defaultValue: `')
                    defaultValue = firstDefault > 0 ? line.substr(firstDefault + 15, line.indexOf('`', firstDefault + 15) - (firstDefault + 15)) : ''
                } else
                    defaultValue = firstDefault > 0 ? line.substr(firstDefault + 15, line.indexOf('\'', firstDefault + 15) - (firstDefault + 15)) : ''

                first = ffffff + 10
                set(langs, key, defaultValue)
                // if (!!langsArray[key]) {
                //     console.log(`trung---`, langsArray[key], defaultValue)
                // }
                langsArray[key] = defaultValue
            } else {
                first = -1
            }
        }
    }
});
rd.on('close', () => {
    // console.log(`close`, JSON.stringify(langsArray))
    console.log('------------------------------------')
    Object.keys(langsArray).forEach(key => {
        // console.log(`${key}:${langsArray[key]}`)
        console.log(`${key}`)
    })
    console.log('------------------------------------')
    Object.keys(langsArray).forEach(key => {
        // console.log(`${key}:${langsArray[key]}`)
        console.log(`${langsArray[key]}`)
    })
})