/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import { AppRegistry } from 'react-native';
 import App from './src/App';
 import { name as appName } from './app.json';
 global.Buffer = global.Buffer || require('buffer').Buffer
 console.disableYellowBox = true;
 AppRegistry.registerComponent(appName, () => App);