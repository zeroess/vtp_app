/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
export const welcomeBg = require('./imgs/bitmap.jpg')
export const wp1 = require('./imgs/wp1.jpg')
export const wp2 = require('./imgs/wp2.jpg')
export const wp3 = require('./imgs/wp3.jpg')
export const wp4 = require('./imgs/wp4.jpg')
export const wp5 = require('./imgs/wp5.jpg')
export const icNodevice = require('./imgs/icNodevice.png')
export const icAdd = require('./imgs/icAdd.png')
export const icDelete = require('./imgs/icDelete.png')
export const modeParty = require('./imgs/modeParty.jpg')
export const modeReadingbook = require('./imgs/modeReadingbook.jpg')
export const modeRomantic = require('./imgs/modeRomantic.jpg')
export const modeWorking = require('./imgs/modeWorking.jpg')
export const icTimer = require('./imgs/icTimer.png')

export const icRefresh = require('./imgs/icRefresh.png')
export const ic_white_back = require('./img2/ic_white_back.png')
export const ic_arr_right_gray = require('./img2/ic_arr_right_gray.png')
export const icon_ava = require('./img2/icon_ava.png')
export const icCompactActive = require('./img2/icCompactActive.png')
export const ic_setting = require('./img2/ic_setting.png')
export const icCurtainHozActive = require('./img2/icCurtainHozActive.png')
export const icLightDropActive = require('./img2/icLightDropActive.png')
export const ic_zoom_out = require('./img2/ic_zoom_out.png')
export const ic_play = require('./img2/ic_play.png')
export const ic_enlarge = require('./img2/ic_enlarge.png')
export const icFan = require('./img2/icFan.png')
export const ic_schedule = require('./img2/ic_schedule.png')

export const ic_zigbee_success = require('./img2/ic_zigbee_success.png')
export const ic_blink_slowy = require('./img2/ic_blink_slowy.png')
export const ic_blink_quickly = require('./img2/ic_blink_quickly.png')
export const ic_wifi = require('./img2/ic_wifi.png')
export const ic_bitmap = require('./img2/ic_bitmap.png')
export const ic_bluetooth = require('./img2/ic_bluetooth.png')
export const ic_gray_close = require('./img2/ic_gray_close.png')
export const ic_whtite_search = require('./img2/ic_whtite_search.png')

export const ic_black_wifi = require('./img2/ic_black_wifi.png')
export const icLock = require('./img2/icLock.png')
export const icGraySwitch = require('./img2/icGraySwitch.png')
export const icActiveEye = require('./img2/icActiveEye.png')
export const icMoreHoriz = require('./img2/icMoreHoriz.png')
export const icWhiteSuccess = require('./img2/icWhiteSuccess.png')

export const icGreenSearch = require('./img2/icGreenSearch.png')
export const icGrayCloud = require('./img2/icGrayCloud.png')
export const icGraySetting = require('./img2/icGraySetting.png')
export const icGreenSetting = require('./img2/icGreenSetting.png')
export const icGreenCloud = require('./img2/icGreenCloud.png')

export const icWhiteCloud = require('./img2/icWhiteCloud.png')
export const icWhiteSetting = require('./img2/icWhiteSetting.png')
export const imWf = require('./img2/imWf.png')
export const imWf2 = require('./img2/imWf2.png')









export const IC_SCENE = [
    require('./imgs/icScene1.png'),
    require('./imgs/icScene2.png'),
    require('./imgs/icScene3.png'),
    require('./imgs/icScene4.png'),
    require('./imgs/icScene5.png'),
]




export const bgCt = require('./imgs/bgCt.png')
export const CURTAIN = [
    require('./imgs/cr190.png'),
    require('./imgs/cr180.png'),
    require('./imgs/cr170.png'),
    require('./imgs/cr160.png'),
    require('./imgs/cr150.png'),
    require('./imgs/cr140.png'),
    require('./imgs/cr130.png'),
    require('./imgs/cr120.png'),
    require('./imgs/cr110.png'),
    require('./imgs/cr100.png'),
]