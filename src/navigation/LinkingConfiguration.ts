import { LinkingOptions, getStateFromPath, getPathFromState } from '@react-navigation/native';

const linkingConfig: LinkingOptions = {
  prefixes: ['feapp://'],
  config: {
    screens: {
      Home: {
        path: 'home',
      },
      Login: {
        path: 'login',
      },
      NotFound: {
        path: '*',
      },
    },
  },
};

export default linkingConfig;
