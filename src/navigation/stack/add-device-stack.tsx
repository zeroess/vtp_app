/*
 * Created by duydatpham@gmail.com on 26/12/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import React, { memo } from 'react'
import { SceneContextProvider } from '../../context/SceneContext';

import { forFadeScreen } from '../effect/forFadeScreen';
import wifiAddDeviceTutorial from '../../screens/add-device/wifi/wifi-add-device-tutorial'
import wifiAddDevice from '../../screens/add-device/wifi/wifi-add-device'
import wifiAddDeviceConfirm from '../../screens/add-device/wifi/wifi-add-device-confirm'
import wifiSearchDevice from '../../screens/add-device/wifi/wifi-search-device'
import wifiPopupSelectStatusAddDevice from '../../screens/add-device/wifi/wifi-popup-selecet-status'
import wifiAddDeviceSuccess from '../../screens/add-device/wifi/wifi-add-device-success'
import zigbeeList from '../../screens/add-device/zigbee/list-zigbee'
import zigbeePopupTutorial from '../../screens/add-device/zigbee/zigbee-popup-tutorial'
import zigbeeSearchDevice from '../../screens/add-device/zigbee/zigbee-search-device'
import zigbeeAddDeviceSuccess from '../../screens/add-device/zigbee/zigbee-add-device-success'
import popupAddDevice from '../../screens/add-device/popup-add-device'

const Stack = createStackNavigator<any>();
export default memo(() => {
    return <SceneContextProvider>
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
            initialRouteName={"PopupAddDevice"}
            >
            <Stack.Screen name="PopupAddDevice" component={popupAddDevice} options={{
                cardStyleInterpolator: forFadeScreen,
                gestureEnabled: false,
                cardStyle: {
                    backgroundColor: 'transparent'
                }
            }} />
            <Stack.Screen name="WifiAddDeviceTutorial" component={wifiAddDeviceTutorial} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="WifiAddDevice" component={wifiAddDevice} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="WifiAddDeviceConfirm" component={wifiAddDeviceConfirm} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="WifiSearchDevice" component={wifiSearchDevice} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="WifiAddDeviceSuccess" component={wifiAddDeviceSuccess} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="ZigbeeList" component={zigbeeList} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="ZigbeeSearchDevice" component={zigbeeSearchDevice} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            <Stack.Screen name="ZigbeeAddDeviceSuccess" component={zigbeeAddDeviceSuccess} options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }} />
            





            <Stack.Screen name="WifiPopupSelectStatusAddDevice" component={wifiPopupSelectStatusAddDevice} options={{
                cardStyleInterpolator: forFadeScreen,
                gestureEnabled: false,
                cardStyle: {
                    backgroundColor: 'transparent'
                }
            }} />
            <Stack.Screen name="ZigbeePopupTutorial" component={zigbeePopupTutorial} options={{
                cardStyleInterpolator: forFadeScreen,
                gestureEnabled: false,
                cardStyle: {
                    backgroundColor: 'transparent'
                }
            }} />
            
            
            




        </Stack.Navigator>
    </SceneContextProvider>
})