/*
 * Created by duydatpham@gmail.com on 03/10/2020
 * Copyright (c) 2020 duydatpham@gmail.com
 */

import * as React from 'react';
import { createBottomTabNavigator, BottomTabBarProps } from '@react-navigation/bottom-tabs';


import { StyleSheet, Text, View, ImageSourcePropType, Animated } from 'react-native';
import { TabBarSize } from '../util';
import { Icon, TouchableOpacity } from '../components/core';
import { ImageSource } from 'react-native-vector-icons/Icon';
import { MainStackParamList } from './types';
import home from '../screens/home';
import { ThemeContext } from '../context/theme';
import smart from '../screens/smart';
interface TabbarItemType {
  title: string,
  icon?: string
}
const Tab = createBottomTabNavigator<MainStackParamList>();
const tabbar: Array<TabbarItemType> = [
  { title: "Home", icon: 'icHomeActive' },
  { title: "Smart", icon: 'icSmart' },
]

const BottomTabBar = ({ navigation, state }: BottomTabBarProps) => {

  const { theme, fontSize } = React.useContext(ThemeContext)

  const buildTabbar = (index: number, title: string, icon?: string) => {
    return (
      <TouchableOpacity
        key={`tabbaar-${index}`}
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={() => {
          navigation.navigate(state.routeNames[index])
        }}>
        {
          !!icon && <Icon name={icon} style={[{ fontSize: fontSize?.fontIconApp }, index == state.index ? { color: theme?.tabIconActiveColor } : { color: theme?.tabIconInactiveColor }]} />
        }
        <Text style={[styles.active, index == state.index ? { color: theme?.tabTextActiveColor } : { color: theme?.tabTextInactiveColor }]} >
          {title}
        </Text>
      </TouchableOpacity>
    )
  }

  const styleTabbar: any = React.useMemo(() => {
    if (state.index != 0) {
      return {
        backgroundColor: theme?.backgroundColor,
        shadowColor: "rgba(0, 0, 0, 0.04)",
        shadowOffset: {
          width: 0,
          height: -8
        },
        shadowRadius: 16,
        shadowOpacity: 1,
        height: TabBarSize.height + TabBarSize.paddingBottom,
        paddingBottom: TabBarSize.paddingBottom,
        // elevation: 8,
        flexDirection: 'row',
        paddingTop: 8,
        position: 'absolute', left: 0, right: 0, bottom: 0,
      }
    }
    return {
      backgroundColor: theme?.tabBackground,
      height: TabBarSize.height + TabBarSize.paddingBottom,
      paddingBottom: TabBarSize.paddingBottom,
      flexDirection: 'row',
      paddingTop: 8,
      position: 'absolute', left: 0, right: 0, bottom: 0,
      borderTopWidth: StyleSheet.hairlineWidth * 2,
      borderTopColor: "#edf0f4"
    }
  }, [state.index, theme])

  return (
    <View style={styleTabbar} >
      {tabbar.map((item: TabbarItemType, index: number) => {
        return (
          buildTabbar(index, item.title, item.icon)
        )
      })}
    </View >
  );
};

export default function TabbarNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBar={(props) => <BottomTabBar {...props} />}
      lazy={false}
    >
      <Tab.Screen name="Home" component={home} />
      <Tab.Screen name="Smart" component={smart} />
    </Tab.Navigator>
  );
}
const styles = StyleSheet.create({
  active: {
    textTransform: 'capitalize',
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    marginTop: 5
  },
});