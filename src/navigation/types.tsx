export type RootStackParamList = {
  Main: undefined;
  Alert: undefined
  Welcome: undefined
  Login: undefined
  Register: undefined
  ForgotPassword: undefined
  ForgotPasswordSetPass: undefined
  ConfirmOtp: {
    email: string
    countryCode: string
    password: string
  }
  HomeEdit: undefined
  HomeEditAddMember: undefined
  HomeEditAddMemberChooseRole: undefined
  HomeAdd: undefined
  HomeAddChooseImage: undefined
  HomeAddChooseImageConfirm: undefined
  DeviceControl: undefined
  DeviceSetting: undefined
  ChooseTime: undefined
  CountdownTimer: undefined
  AddSchedule: undefined
  AddScheduleAction: undefined
  Loading: undefined
  SelectLocation: undefined;
  SceneStack: undefined;
  AddDeviceStack: undefined;
  AddSceneStep2: undefined;
  AddSceneListDevice: undefined;
  AddSceneSelectFunction: undefined;
  NotFound: undefined;
  Setting : undefined;
  EditInfoUser : undefined,
  HomeManagement : undefined,
  AddSceneListDeviceIn : undefined,
  AddSceneSelectFunctionIn : undefined,
  AddSceneEditFunctionIn : undefined,
  Schedule : undefined,
  PopupAddDevice : undefined
};

export type MainStackParamList = {
  Home: undefined;
  Smart: undefined;
};
