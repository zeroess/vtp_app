import * as React from 'react';
import { NavigationContainer, DefaultTheme, Theme } from '@react-navigation/native';
import { CardStyleInterpolators, createStackNavigator, TransitionPresets } from '@react-navigation/stack';

import { RootStackParamList } from './types';
import LinkingConfiguration from './LinkingConfiguration';

import NotFoundScreen from '../screens/NotFound';
import { Text } from 'react-native';
import Main from './MainNavigator'
import { forFadeScreen } from './effect/forFadeScreen';
import welcome from '../screens/welcome';
import Login from '../screens/login';
import register from '../screens/register';
import confirmOtp from '../screens/confirm-otp';
import loading from '../screens/loading';
import forgotPassword from '../screens/forgot-password';
import forgotPasswordSetpass from '../screens/forgot-password/setpass';
import { CommonContext } from '../context/Common';
import homeAdd from '../screens/home-add';
import homeAddChooseimage from '../screens/home-add/chooseimage';
import homeAddChooseimageConfirm from '../screens/home-add/chooseimage-confirm';
import homeEdit from '../screens/home-edit';
import homeEditAddMember from '../screens/home-edit/add-member';
import homeEditAddMemberChooseRole from '../screens/home-edit/add-member-choose-role';
import addDeviceStack from './stack/add-device-stack'
import setting from '../screens/setting'
import editInfoUser from '../screens/setting/edit-info-user'
import homeManagement from '../screens/home-management'
const navigatorTheme = (): Theme => {
  return {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
    },
  };
};

export default function Navigation() {
  return (
    <NavigationContainer
      theme={navigatorTheme()}
      linking={LinkingConfiguration}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {

  const { currentUser } = React.useContext(CommonContext)

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={
        !currentUser ? 'Welcome' : 'Main'
      }>
      {/* initialRouteName={!currentUser ? 'Welcome' : 'DeviceControl'}> */}

      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Welcome" component={welcome} />
      <Stack.Screen name="Register" component={register} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="ForgotPassword" component={forgotPassword} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="ConfirmOtp" component={confirmOtp} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="ForgotPasswordSetPass" component={forgotPasswordSetpass} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeAdd" component={homeAdd} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeEdit" component={homeEdit} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="Setting" component={setting} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="EditInfoUser" component={editInfoUser} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeManagement" component={homeManagement} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeEditAddMember" component={homeEditAddMember} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeEditAddMemberChooseRole" component={homeEditAddMemberChooseRole} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeAddChooseImage" component={homeAddChooseimage} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }} />
      <Stack.Screen name="HomeAddChooseImageConfirm" component={homeAddChooseimageConfirm} options={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        gestureEnabled: false,
      }} />

      <Stack.Screen name="Main" component={Main} options={{ title: 'Home' }} />

      <Stack.Screen name="AddDeviceStack" component={addDeviceStack} options={{
        // cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        cardStyleInterpolator: forFadeScreen,
        // gestureEnabled: false,
        cardStyle: {
          backgroundColor: 'transparent'
        }
      }} />

      <Stack.Screen name="Loading" component={loading} options={{
        cardStyleInterpolator: forFadeScreen,
        gestureEnabled: false,
        cardStyle: {
          backgroundColor: 'transparent'
        }
      }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
    </Stack.Navigator>
  );
}
