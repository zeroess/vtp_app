'use strick'
// const KEY_API_APP = '98CPB8ITIRGHVO3OJ5QT';
const KEY_API_APP = "Y537Z9L6IU67JVOVF5CP";

export function UPLOAD(files) {
  return new Promise((resolve, reject) => {

    if (!files) {
      resolve({
        "success": true,
        "statusCode": 200,
        "message": "Upload thành công",
        "data": {
          "files": [

          ]
        }
      })
      return
    }

    let formData = new FormData();
    if (files instanceof Array) {
      if (files.length == 0) {
        resolve({
          "success": true,
          "statusCode": 200,
          "message": "Upload thành công",
          "data": {
            "files": [

            ]
          }
        })
        return
      }
      for (let i = 0; i < files.length; i++) {

        let name = (files[i].name || files[i].filename)
        let uri = (files[i].url || files[i].path)

        if (!!!name) {
          let urrrr = uri.split('/')
          name = urrrr[urrrr.length - 1]
        }

        formData.append('UploadForm[files][]', { uri, name: (name || '').replace(/ /gi, '_'), type: 'multipart/form-data' });

      }
    } else {

      let name = (files.name || files.filename)
      let uri = (files.url || files.path)

      if (!!!name) {
        let urrrr = uri.split('/')
        name = urrrr[urrrr.length - 1]
      }
      console.log(`files`, files)
      formData.append('UploadForm[files][]', { uri, name: (name || '').replace(/ /gi, '_'), type: 'multipart/form-data' });
    }

    window.connection.POST('/file/upload', formData)
      .then(json => {
        resolve(json)
      })
      .catch(e => {
        reject(e)
      })
  })
}

