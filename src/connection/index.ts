/**
* Created by duydatpham@gmail.com on Thu Jul 19 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
"use strict";
import * as API from './api';
import _ from 'lodash';
import { UPLOAD } from './fileServer';
import { API_TYPES, api_key } from './api_type';
import { logout } from '../redux/actions/config';
import { RootState } from '../redux/reducers';

export declare const global: {
    window: {
        env: {
            URL_API: string
        },
        connection: Connection,
        alertCustom: any
    }
};

const selectorToken = (state: RootState) => state.config.access_token

export const getFullLinkImage = (url?: string) => {
    if (!!!url)
        return undefined
    if (url.startsWith('http') || url.startsWith(global.window.env.URL_API))
        return url
    return `${global.window.env.URL_API}${url.startsWith('/') ? url : `/${url}`}`
}

export default class Connection {
    static init(store: any) {
        let con = new Connection(store)
        global.window.connection = con


    }

    _token: string | undefined;
    _store: any | undefined
    tokenExpire: () => void;

    constructor(store: any) {
        this._token = undefined
        this._store = store
        this._store.subscribe(this.listenerChange)


        this.tokenExpire = _.throttle(() => {
            console.log('tokenExpire')
            this.dispatch(logout())
        }, 5000, { 'trailing': false });
    }

    listenerChange = () => {
        let token = selectorToken(this._store.getState())
        if (this._token != token) {
            this._token = token
        }
    }

    dispatch = (action: any) => {
        this._store.dispatch(action)
    }


    POST = (url: string, data?: any, token = this._token, noAlert?: boolean) => {
        return new Promise((resolve, reject) => {
            let query = ''
            API.POST(`${global.window.env.URL_API}${url}${query}`, data, token, api_key).then(res => {
                if (!res.success && !noAlert) {
                    global.window.alertCustom.showError(res.message)
                }
                resolve(res)

            }).catch(e => {
                resolve({})
            })
            //   window.alertCustom.alertWithType('error', 'Error', 'error.message')
        })
    }

    DELETE = (url: string, data?: any, token = this._token) => {
        return new Promise((resolve, reject) => {
            let query = ''
            API.DELETE(`${global.window.env.URL_API}${url}${query}`, data, token, api_key).then(res => {
                if (!res.success) {
                    global.window.alertCustom.showError(res.message)
                }
                resolve(res)

            }).catch(e => {
                resolve({})
            })
            //   window.alertCustom.alertWithType('error', 'Error', 'error.message')
        })
    }
    GET = (url: string, data?: any, token = this._token, fullUrl?: boolean) => {
        return new Promise((resolve, reject) => {
            API.GET(fullUrl ? url : `${global.window.env.URL_API}${url}`, data, token, api_key).then(res => {
                if (!res.success) {
                    if (res.statusCode == 401) {
                        global.window.alertCustom.showError('Tài khoản bị khoá hoặc hết hạn phiên đăng nhập. Xin vui lòng đăng nhập lại.')
                        this.tokenExpire()
                    } else {
                        global.window.alertCustom.showError(res.message)
                    }
                }
                resolve(res)
            }).catch(e => {
                resolve({})
            })
            //   window.alertCustom.alertWithType('error', 'Error', 'error.message')
        })
    }

    UploadFiles = (files: Array<any>) => {
        return UPLOAD(files)
    }
}
