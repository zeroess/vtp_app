/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useState } from 'react';
import { ActivityIndicator, Keyboard, StatusBar, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../components/core';
import { ThemeContext } from '../../context/theme';
import { HeaderSize } from '../../util';
import CountryPicker, { Country, CountryCode } from 'react-native-country-picker-modal'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import { Kohana } from 'react-native-textinput-effects';
import { isValidEmail } from '../../util/validate';
import { showMessage } from 'react-native-flash-message';

export default memo(() => {
  const { fontSize, theme } = useContext(ThemeContext)
  const navigation = useNavigation()
  const [countryCode, setCountryCode] = useState<CountryCode>('IN')
  const [country, setCountry] = useState<Country>({
    callingCode: ['91']
  } as Country)
  const [email, setEmail] = useState('')
  const [loading, setLoading] = useState(false)

  React.useEffect(
    () =>
      navigation.addListener('beforeRemove', (e) => {
        // Prevent default behavior of leaving the screen
        e.preventDefault();
        if (!loading)
          navigation.dispatch(e.data.action)
      }),
    [navigation, loading]
  );


  const _getVerifycode = useCallback(async () => {
    setLoading(true)
    try {
      // let res = await getEmailValidateCode({
      //   countryCode: country?.callingCode[0],
      //   email
      // });
      // console.log('res', res)
      navigation.navigate("ForgotPasswordSetPass", {
        countryCode: country?.callingCode[0],
        email
      })
    } catch (error: any) {
      console.log('error', error)
      showMessage({
        type: 'warning',
        message: error.message
      })
    } finally {
      setLoading(false)
    }
  }, [email, country])

  const canContinue = email.trim().length > 0 && isValidEmail(email) && !loading

  return (
    <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} >
      <View style={{
        flex: 1, backgroundColor: theme?.backgroundColor
      }} >
        <StatusBar barStyle='dark-content' />
        <View style={{
          height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
          paddingHorizontal: 16
        }} >
          <TouchableOpacity style={{
            height: '100%', width: HeaderSize.height - HeaderSize.paddingTop,
            alignItems: 'center', justifyContent: "center"
          }}
            onPress={navigation.goBack}
          >
            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.iconBlack }} />
          </TouchableOpacity>
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginTop: 16, marginHorizontal: 32
        }} >
          Forgot password
        </Text>
        <View style={{
          marginHorizontal: 32,
          marginTop: 32,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }}
          pointerEvents={loading ? 'none' : 'auto'}
        >
          <CountryPicker
            containerButtonStyle={{ height: '100%', justifyContent: 'center', }}
            countryCode={countryCode}
            onSelect={(country: Country) => {
              setCountryCode(country.cca2)
              setCountry(country)
            }}
            withCountryNameButton
            withFilter
            withEmoji
            withFlag
            withAlphaFilter
          />
          <View style={{
            transform: [{ rotate: '-90deg' }], position: 'absolute',
            right: 16, top: 16
          }}
            pointerEvents='none'
          >
            <Icon name='icBack' style={{
              fontSize: 14, color: theme?.iconBlack,
            }} />
          </View>
        </View>
        <View style={{
          marginHorizontal: 32,
          marginTop: 16,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }} >
          <Kohana
            style={{ backgroundColor: 'transparent', height: '100%' }}
            label={'Email address'}
            iconClass={IconMaterial}
            iconName={'email'}
            iconColor={theme?.mainColor}
            iconSize={24}
            // inputPadding={16}
            labelStyle={{ color: theme?.textBlackPlaceholder, fontWeight: 'normal' }}
            inputStyle={{ color: theme?.iconBlack }}
            labelContainerStyle={{ padding: 0 }}
            iconContainerStyle={{ padding: 0 }}
            useNativeDriver
            value={email}
            onChangeText={setEmail}
            autoCapitalize='none'
            editable={!loading}
          />
        </View>
        <TouchableOpacity style={{
          marginHorizontal: 32,
          height: 50,
          borderRadius: 12,
          backgroundColor: !canContinue ? theme?.buttonBackgroundWhiteDisable : theme?.buttonBackgroundMain,
          marginTop: 24,
          alignItems: 'center', justifyContent: 'center'
        }}
          disabled={!canContinue}
          onPress={_getVerifycode}
        >
          {
            !loading ? <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "center",
              color: theme?.textWhite
            }} >
              Get Verification Code
            </Text> : <ActivityIndicator color={theme?.mainColor} />
          }
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
}
)