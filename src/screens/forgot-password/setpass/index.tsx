/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { RouteProp, useNavigation, useRoute, CommonActions } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { Keyboard, StatusBar, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../../components/core';
import { ThemeContext } from '../../../context/theme';
import { HeaderSize } from '../../../util';
import { showMessage } from 'react-native-flash-message';
import CodeInput from 'react-native-confirmation-code-input';
import { RootStackParamList } from '../../../navigation/types';
import { CommonContext } from '../../../context/Common';
import { Kohana } from 'react-native-textinput-effects';
import IconMaterial from 'react-native-vector-icons/MaterialIcons'

type ScreenRouteProp = RouteProp<RootStackParamList, 'ConfirmOtp'>;

export default memo(() => {
  const { fontSize, theme } = useContext(ThemeContext)
  const { continueLogin } = useContext(CommonContext)
  const navigation = useNavigation()
  const route = useRoute<ScreenRouteProp>()
  const _refInput = useRef<any>(null)
  const _refInterval = useRef<any>(null)
  const [countDown, setCountDown] = useState(0)
  const [password, setPassword] = useState('')
  const [code, setCode] = useState('')


  useEffect(() => {
    return () => {
      if (_refInterval.current)
        clearInterval(_refInterval.current)
    }
  }, [])

  useMemo(() => {
    if (countDown == 30) {
      clearInterval(_refInterval.current)
      _refInterval.current = setInterval(() => {
        setCountDown((prev: number) => {
          return prev - 1
        })
      }, 1000)
    } else if (countDown == 0) {
      clearInterval(_refInterval.current)
      _refInterval.current = null
    }
  }, [countDown])


  const _verifycode = useCallback(async () => {
    navigation.navigate('Loading')
    console.log('registerAccountWithEmail', route?.params, {
      validateCode: code,
      email: route?.params?.email || '',
      countryCode: route?.params?.countryCode || '',
      password: password
    })
    try {
      // let res = await resetEmailPassword({
      //   validateCode: code,
      //   email: route?.params?.email || '',
      //   countryCode: route?.params?.countryCode || '',
      //   newPassword: password
      // });
      // console.log('res', res, {
      //   validateCode: code,
      //   email: route?.params?.email || '',
      //   countryCode: route?.params?.countryCode || '',
      //   password: password
      // })
      navigation.goBack()
      navigation.goBack()
      navigation.goBack()
    } catch (error: any) {
      console.log('error', error)
      showMessage({
        type: 'warning',
        message: error.message
      })
      navigation.goBack()
    } finally {

    }
  }, [route?.params?.email, route?.params?.countryCode, password, code])

  const _resendOTP = useCallback(async () => {
    navigation.navigate('Loading')
    try {
      // let res = await getEmailValidateCode({
      //   email: route?.params?.email || '',
      //   countryCode: route?.params?.countryCode || '',
      // });
      // console.log('res', res)
    } catch (error: any) {
      console.log('error', error)
      showMessage({
        type: 'warning',
        message: error.message
      })
    } finally {
      navigation.goBack()
    }
  }, [route?.params?.email, route?.params?.countryCode])

  const canContinue = password.trim().length >= 6 && code.length > 0

  return (
    <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} >
      <View style={{
        flex: 1, backgroundColor: theme?.backgroundColor
      }} >
        <StatusBar barStyle='dark-content' />
        <View style={{
          height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
          paddingHorizontal: 16
        }} >
          <TouchableOpacity style={{
            height: '100%', width: HeaderSize.height - HeaderSize.paddingTop,
            alignItems: 'center', justifyContent: "center"
          }}
            onPress={navigation.goBack}
          >
            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.iconBlack }} />
          </TouchableOpacity>
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginTop: 16, marginHorizontal: 32
        }} >
          Set Password
        </Text>
        <View style={{
          marginHorizontal: 32,
          marginTop: 32,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }} >
          <Kohana
            style={{ backgroundColor: 'transparent', height: '100%' }}
            label={'Your password'}
            iconClass={IconMaterial}
            iconName={'lock'}
            iconColor={theme?.mainColor}
            iconSize={24}
            // inputPadding={16}
            labelStyle={{ color: theme?.textBlackPlaceholder, fontWeight: 'normal' }}
            inputStyle={{ color: theme?.iconBlack }}
            labelContainerStyle={{ padding: 0 } as any}
            iconContainerStyle={{ padding: 0 }}
            useNativeDriver
            value={password}
            onChangeText={setPassword}
            autoCapitalize='none'
            secureTextEntry
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText?.f12,
          fontWeight: "normal",
          fontStyle: "normal",
          lineHeight: 21,
          letterSpacing: 0,
          color: theme?.textBlackSecondary,
          marginHorizontal: 32, marginTop: 4
        }} >Use 6-20 characters with a mix of letters and numbers</Text>

        <View style={{ paddingHorizontal: 16, flexDirection: 'row', marginBottom: 24 }}>
          <CodeInput
            ref={_refInput}
            activeColor={theme?.mainColor}
            inactiveColor={theme?.textBlackPlaceholder}
            autoFocus={true}
            ignoreCase={true}
            inputPosition='center'
            size={50}
            keyboardType='numeric'
            codeLength={6}
            onFulfill={setCode}
            containerStyle={{ borderRadius: 6, }}
            codeInputStyle={{
              // backgroundColor: '#F5F5F5',
              fontSize: 32,
              borderRadius: 6,
              height: 60
            }}
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          lineHeight: 21,
          letterSpacing: 0,
          color: theme?.textBlackSecondary,
          marginHorizontal: 32
        }} >A verification code has been sent to your email {route?.params?.email}. {countDown > 0 ? `Resend (${countDown}s)` : ''}</Text>
        {
          countDown <= 0 && <TouchableOpacity onPress={_resendOTP} >
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              color: theme?.mainColor,
              marginHorizontal: 32, marginTop: 40
            }} >
              Didn't get a code?
            </Text>
          </TouchableOpacity>
        }

        <TouchableOpacity style={{
          marginHorizontal: 32,
          height: 50,
          borderRadius: 12,
          backgroundColor: !canContinue ? theme?.buttonBackgroundWhiteDisable : theme?.buttonBackgroundMain,
          marginTop: 24,
          alignItems: 'center', justifyContent: 'center'
        }}
          disabled={!canContinue}
          onPress={_verifycode}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "500",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "center",
            color: theme?.textWhite
          }} >
            Set Password
          </Text>
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
}
)