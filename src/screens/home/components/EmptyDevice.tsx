/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import { useNavigation } from '@react-navigation/core';
import React, { memo, useContext } from 'react'
import { Image, Text, useWindowDimensions, View } from 'react-native';
import { icNodevice } from '../../../../assets/images';
import { TouchableOpacity } from '../../../components/core';
import { CommonContext } from '../../../context/Common';
import { ThemeContext } from '../../../context/theme';

export default memo(({ onAddDevice }: { onAddDevice: any }) => {
    const { theme, fontSize } = useContext(ThemeContext);
    const { height, width } = useWindowDimensions()
    const navigation = useNavigation()
    return <View style={{ alignItems: 'center', marginTop: height / 4 }} >
        <Image source={icNodevice} />
        <Text style={{
            fontSize: fontSize?.fontText.f14,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.home?.textWhite,
            marginVertical: 24
        }} >
            No device
        </Text>
        <TouchableOpacity style={{
            width: 150,
            height: 50,
            borderRadius: 12,
            backgroundColor: theme?.buttonBackgroundMain,
            alignItems: 'center', justifyContent: 'center',
        }}
            onPress={onAddDevice}
        // onPress={() => { navigation.navigate('DeviceControl') }}
        >
            <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.home?.textWhite
            }} >
                Add Device
            </Text>
        </TouchableOpacity>
    </View>
})