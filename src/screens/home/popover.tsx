/*
 * Created by duydatpham@gmail.com on 20/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import { useNavigation, CommonActions } from '@react-navigation/core';
import React, { memo, useContext, useMemo, useState } from 'react'
import { Alert, StyleSheet, Text, View } from 'react-native';
import Popover from 'react-native-popover-view';
import { useSelector } from 'react-redux';
import { Icon, TouchableOpacity } from '../../components/core';
import { CommonContext } from '../../context/Common';
import { ThemeContext } from '../../context/theme';
import { RootState } from '../../redux/reducers';
import { selectCurrentHome } from '../../redux/actions/home';
import { HeaderSize } from '../../util';
import { useDispatch } from 'react-redux';
import { GoogleSignin } from '@react-native-community/google-signin';


export const ButtonAddDevice = memo(({ onAddDevice }: { onAddDevice: () => void }) => {
    const { fontSize, theme } = useContext(ThemeContext)
    const [showPopoverAddDevice, setShowPopoverAddDevice] = useState(false);
    const navigation = useNavigation()

    return (
        <Popover
            isVisible={showPopoverAddDevice}
            onRequestClose={() => setShowPopoverAddDevice(false)}
            from={(
                <TouchableOpacity style={{
                    height: '100%', alignItems: 'center', justifyContent: 'center',
                    width: HeaderSize.height - HeaderSize.paddingTop,
                }}
                    onPress={() => setShowPopoverAddDevice(true)}
                >
                    <Icon name='icAdd' style={{ fontSize: fontSize?.fontIconApp, color: theme?.home?.textWhite }} />
                </TouchableOpacity>)}
            backgroundStyle={{ backgroundColor: 'transparent' }}
            arrowStyle={{ backgroundColor: 'transparent' }}
            popoverStyle={{
                width: 200,
                marginRight: 16,
                borderRadius: 12,
                backgroundColor: 'transparent'
            }}
            verticalOffset={-10}
        >
            <View style={{
                flex: 1,
                backgroundColor: theme?.backgroundColor,
                shadowColor: "rgba(0, 0, 0, 0.1)",
                shadowOffset: {
                    width: 0,
                    height: 5
                },
                shadowRadius: 20,
                shadowOpacity: 1,
                // borderStyle: "solid",
                elevation: 8,
                borderWidth: 1,
                borderRadius: 12,
                borderColor: theme?.borderColor,
                paddingVertical: 4
            }} >
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    paddingVertical: 10, paddingHorizontal: 16
                }}
                    onPress={() => {
                        setShowPopoverAddDevice(false)
                        setTimeout(() => {
                            onAddDevice()
                        }, 500);
                    }}
                >
                    <Text style={{
                        fontSize: fontSize?.fontText.f14,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.textBlack,
                    }} >Add Device</Text>
                    <Icon name='ic_add_device' style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }} />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    paddingVertical: 10, paddingHorizontal: 16
                }} >
                    <Text style={{
                        fontSize: fontSize?.fontText.f14,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.textBlack,
                    }} >Add Room</Text>
                    <Icon name='ic_add_room' style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }} />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    paddingVertical: 10, paddingHorizontal: 16
                }} >
                    <Text style={{
                        fontSize: fontSize?.fontText.f14,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.textBlack,
                    }} >Add People</Text>
                    <Icon name='ic_group_mb' style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }} />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    paddingVertical: 15, paddingHorizontal: 16,
                    borderTopColor: theme?.borderColor,
                    borderTopWidth: StyleSheet.hairlineWidth * 2
                }}
                    onPress={() => {
                        setShowPopoverAddDevice(false)
                        navigation.navigate('HomeAdd')
                    }}
                >
                    <Text style={{
                        fontSize: fontSize?.fontText.f14,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.textBlack,
                    }} >Add New Home</Text>
                    <Icon name='icHome' style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }} />
                </TouchableOpacity>

            </View>
        </Popover>
    )
})
export const ButtonHome = memo(({ color }: any) => {
    const { fontSize, theme } = useContext(ThemeContext)
    const { logout } = useContext(CommonContext)
    const [showPopover, setShowPopover] = useState(false);
    const navigation = useNavigation()
    const { homeList, currentHome } = useSelector((state: RootState) => state.home)
    const dispatch = useDispatch()

    return (
        <Popover
            isVisible={showPopover}
            onRequestClose={() => setShowPopover(false)}
            from={(
                <TouchableOpacity style={{
                    height: '100%', alignItems: 'center', justifyContent: 'center',
                    width: HeaderSize.height - HeaderSize.paddingTop,
                }} onPress={() => setShowPopover(true)} >
                    <Icon name='icHome' style={{ fontSize: fontSize?.fontIconApp, color: color || theme?.home?.textWhite }} />
                </TouchableOpacity>)}
            backgroundStyle={{ backgroundColor: 'transparent' }}
            arrowStyle={{ backgroundColor: 'transparent' }}
            popoverStyle={{
                width: 200,
                marginLeft: 8,
                borderRadius: 12,
                backgroundColor: 'transparent'
            }}
            verticalOffset={-10}
        >
            <View style={{
                flex: 1,
                backgroundColor: theme?.backgroundColor,
                shadowColor: "rgba(0, 0, 0, 0.1)",
                shadowOffset: {
                    width: 0,
                    height: 5
                },
                shadowRadius: 20,
                shadowOpacity: 1,
                // borderStyle: "solid",
                elevation: 8,
                borderWidth: 1,
                borderRadius: 12,
                borderColor: theme?.borderColor
            }} >
                {
                    homeList.map((_home: any, index: number) => {
                        return (
                            <TouchableOpacity key={`home-${_home.homeId}`} style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                                paddingVertical: 10, paddingHorizontal: 16
                            }}
                                onPress={() => {
                                    _home.homeId != currentHome.homeId && dispatch(selectCurrentHome(_home))
                                }}
                            >
                                <Text style={{
                                    fontSize: fontSize?.fontText.f14,
                                    fontWeight: "normal",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    color: theme?.textBlack,
                                }} >{_home.name}</Text>
                                {_home.homeId == currentHome.homeId && <Icon name='ic_check' style={{ fontSize: fontSize?.fontText.f16, color: theme?.mainColor }} />}
                            </TouchableOpacity>
                        )
                    })
                }

                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    paddingVertical: 15, paddingHorizontal: 16,
                    borderTopColor: theme?.borderColor,
                    borderTopWidth: StyleSheet.hairlineWidth * 2
                }}
                    onPress={() => {
                        setShowPopover(false)
                        setTimeout(() => {
                            // navigation.navigate('HomeEdit', { home: currentHome })
                            navigation.navigate("Setting")
                        }, 500);
                        

                    }}
                >
                    <Text style={{
                        fontSize: fontSize?.fontText.f14,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.textBlack,
                    }} >Seting</Text>
                    <Icon name='icSetting' style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }} />
                </TouchableOpacity>
            </View>
        </Popover>
    )
})