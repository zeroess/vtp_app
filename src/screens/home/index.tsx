/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { ActivityIndicator, Image, ScrollView, StatusBar, Text, TextInput, useWindowDimensions, View, Animated } from 'react-native'
import { icNodevice } from '../../../assets/images';
import Container from '../../components/Container';
import { Alert, Icon, TouchableOpacity } from '../../components/core';
import { ThemeContext } from '../../context/theme';
import { HeaderSize } from '../../util';
import Popover from 'react-native-popover-view';
import { ButtonAddDevice, ButtonHome } from './popover';
import { CommonContext } from '../../context/Common';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';
import { fetchDeviceByHome } from '../../redux/actions/device';
import EmptyDevice from './components/EmptyDevice';
import DeviceCell from '../../components/DeviceCell'
import { fetchRoom } from '../../redux/actions/home';



export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const { height, width } = useWindowDimensions()
  const _refAlert = useRef<any>(null)
  const [ssid, setSSID] = useState('')
  const [password, setPassword] = useState('')
  const [addingDevice, setAddingDevice] = useState(false)
  const currentHome = useSelector((state: RootState) => state.home.currentHome)
  const { deviceList } = useSelector((state: RootState) => state.device)
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const [shadown, setShadown] = useState(new Animated.Value(0))

  // useEffect(() => {
  //   if (!!currentHome) {
  //     dispatch(fetchDeviceByHome(currentHome.homeId))
  //   }

  //   getDeviceConditionOperationList({ devId: "d799ab52d5bee8878bhdjk" }).then(res => {
  //     console.log('getDeviceConditionOperationList', res)
  //   })
  //   getDeviceTaskOperationList({ devId: "d799ab52d5bee8878bhdjk" }).then(res => {
  //     console.log('getDeviceTaskOperationList', res)
  //   })
  //   getTaskDevList({ homeId: currentHome.homeId }).then(res => {
  //     console.log('getTaskDevList', res)
  //   })

  //   // return () => { stopConfig() }
  // }, [currentHome])

  const _openInputWifi = useCallback(async () => {
    // getCurrentWifi(ssid => {
    //   console.log('ssid', ssid)
    //   setSSID(ssid)
    // }, () => {
    //   console.log('getCurrentWifi::error')
    // })
    // setPassword('')
    // _refAlert.current.open()
    navigation.navigate("AddDeviceStack")
    // navigation.navigate("PopupAddDevice")
  }, [])


  const _addDevice = useCallback(async () => {

    setAddingDevice(true)
    try {
      // let res = await initActivator({
      //   ssid,
      //   password,
      //   homeId: currentHome.homeId,
      //   time: 120,
      //   type: 'TY_EZ'
      // })
      // console.log('resresresres', res)
      // dispatch(fetchDeviceByHome(currentHome.homeId))
    } catch (error) {

    } finally {
      setAddingDevice(false)
    }
  }, [ssid, password, currentHome])


  const _handleScroll = useCallback((event: any) => {
    if (event.nativeEvent.contentOffset.y <= 0) {
      Animated.spring(shadown, {
        toValue: 0,
        delay: 10,
        useNativeDriver: true
      }).start();
      return
    }

    Animated.spring(shadown, {
      toValue: 1,
      delay: 10,
      useNativeDriver: true
    }).start();



    // setShadown(true)

  }, [shadown])


  return (
    <>
      <StatusBar barStyle={!!theme?.backgroundImageUrl ? 'light-content' : 'dark-content'} />
      <Animated.View style={{
        height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
        backgroundColor: 'transparent',
        alignItems: 'center', flexDirection: 'row',
        paddingHorizontal: 8, position: 'absolute', left: 0, right: 0, top: 0, zIndex: 99,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: {
          width: 0,
          height: 8,
        },
        shadowRadius: 8,
        shadowOpacity: shadown,
        elevation: 4,
      }} >
        <ButtonHome />
        <View style={{ flex: 1 }} />
        <TouchableOpacity style={{
          height: '100%', alignItems: 'center', justifyContent: 'center',
          width: HeaderSize.height - HeaderSize.paddingTop,
        }} >
          <Icon name='icNoti' style={{ fontSize: fontSize?.fontIconApp, color: theme?.home?.textWhite }} />
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: '100%', alignItems: 'center', justifyContent: 'center',
          width: HeaderSize.height - HeaderSize.paddingTop,
        }} >
          <Icon name='icVoice' style={{ fontSize: fontSize?.fontIconApp, color: theme?.home?.textWhite }} />
        </TouchableOpacity>
        <ButtonAddDevice onAddDevice={_openInputWifi} />
      </Animated.View>

      <Container
        onScroll={_handleScroll}
        hasImgBackground={!!theme?.backgroundImageUrl} hasScroll={true}
        style={{
          paddingTop: HeaderSize.height, backgroundColor: !!theme?.backgroundImageUrl ? 'transparent' : theme?.backgroundColor,
        }}
      >
        <Text style={{
          fontSize: fontSize?.fontText.f24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.home?.textWhite,
          marginHorizontal: 24,
          marginTop: 8
        }} >
          {
            currentHome?.name
          }
        </Text>
        {
          deviceList.length == 0 && !addingDevice && <EmptyDevice onAddDevice={_openInputWifi} />
        }
        {
          deviceList.length > 0 && <>
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "bold",
              fontStyle: "normal",
              letterSpacing: 0,
              color: theme?.home?.textWhite,
              marginHorizontal: 24,
              marginTop: 40
            }} >Favorite Devices</Text>
            <View style={{ paddingHorizontal: 16, marginTop: 16, flexDirection: 'row', flexWrap: 'wrap' }} >
              {
                deviceList.map((_device: any, index: number) => {
                  return <DeviceCell key={`home-${_device.devId}-${index}`} padding={8} device={_device} />
                })
              }
            </View>
            <View style={{ paddingHorizontal: 16, marginTop: 16, flexDirection: 'row', flexWrap: 'wrap', marginBottom: 100 }} >
              {["gdt-as-g-1d", 'gdt-as-g-2d', 'gdt-as-g-3', 'gdt-as-g-3-1', 'gdt-as-g-5-1', 'gdt-as-g-5-2', 'gdt-as-g-5f-1', 'gdt-as-g-5f', 'gdt-in-g-6', 'gdt-in-g-10', 'gdt-in-g-10-1'].map((_device: string, index: number) => {
                return (
                  <View style={{
                    width: (width - 8 * 4) / 2,
                    padding: 8,
                    paddingBottom: 8 + 8,
                  }} key={`dev-${index}`} >
                    <TouchableOpacity style={{
                      width: '100%',
                      height: 120,
                      borderRadius: 12,
                      backgroundColor: "#ffffff",
                      shadowColor: "rgba(0, 0, 0, 0.1)",
                      shadowOffset: {
                        width: 0,
                        height: 5
                      },
                      shadowRadius: 20,
                      shadowOpacity: 1,
                      elevation: 8,
                      paddingBottom: 16,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                      onPress={() => navigation.navigate('DeviceControl', { device: { category: _device } })}
                    >
                      <Text>{_device}</Text>

                    </TouchableOpacity>
                  </View>
                )
              })

              }
            </View>
          </>
        }

      </Container>
      {
        addingDevice && <View style={{
          backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center', justifyContent: 'center',
          alignSelf: 'center', position: 'absolute',
          top: 0, left: 0, right: 0, bottom: 0, zIndex: 99999
        }} >
          <ActivityIndicator color={theme?.mainColor} size='large' style={{ marginLeft: 4 }} />

          <TouchableOpacity style={{
            width: 150,
            height: 50,
            borderRadius: 12,
            backgroundColor: theme?.buttonBackgroundMain,
            alignItems: 'center', justifyContent: 'center',
            marginTop: 48
          }}
            onPress={() => {
              // stopConfig()
              setAddingDevice(false)
            }}
          >
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              color: theme?.home?.textWhite
            }} >
              Stop
            </Text>
          </TouchableOpacity>
        </View>
      }
      <Alert
        ref={_refAlert}
        width={Math.min(width - 40, 310)}
        style={{
          borderRadius: 6
        }}
        backgroundOverlay={theme?.modalBackgroundOverlayColor}
        renderContent={() => {
          return (
            <View style={{
              paddingTop: 24, backgroundColor: theme?.modalBackgroundColor,
            }} >
              <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "bold",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.textBlack,
                marginHorizontal: 16
              }} >Your wifi</Text>

              <View style={{ width: '100%', height: 44, marginTop: 16, paddingHorizontal: 16 }} >
                <TextInput placeholder='ssid' style={{ flex: 1 }} value={ssid} onChangeText={setSSID} />
              </View>
              <View style={{ width: '100%', height: 44, paddingHorizontal: 16 }} >
                <TextInput placeholder='password' style={{ flex: 1 }} value={password} onChangeText={setPassword} />
              </View>

              <View style={{
                borderTopColor: theme?.dividerColor, borderTopWidth: 1,
                height: 50, flexDirection: 'row'
              }} >
                <TouchableOpacity style={{
                  flex: 1, alignItems: 'center', justifyContent: 'center'
                }}
                  onPress={() => _refAlert.current.close()}
                >
                  <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: theme?.mainColor,
                  }} >
                    Cancel
                  </Text>
                </TouchableOpacity>
                <View style={{
                  backgroundColor: theme?.dividerColor, width: 1,
                  height: 50
                }} ></View>
                <TouchableOpacity style={{
                  flex: 1, alignItems: 'center', justifyContent: 'center'
                }}
                  onPress={() => _refAlert.current.close(_addDevice)}
                  disabled={!ssid || !password}
                >
                  <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: !ssid || !password ? theme?.textBlackSecondary : theme?.mainColor,
                  }} >
                    Continue
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )
        }}
      />
    </>
  );
}
