/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { Alert, Image, ScrollView, StatusBar, StyleSheet, Text, TextInput, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../components/core';
import HeaderTheme from '../../components/HeaderTheme';
import { ThemeContext } from '../../context/theme';

import ImagePicker from 'react-native-image-crop-picker';
import { TabBarSize } from '../../util';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCurrentHome, setListHome, updateCurrentHome } from '../../redux/actions/home';
import { showMessage } from 'react-native-flash-message';
import { RootState } from '../../redux/reducers';
import _ from 'lodash';
import { uploadFile } from '../../api';
import { saveHomeConfig } from '../../redux/actions/config';
import FastImage from 'react-native-fast-image';
import { BACKGROUD_WALLPAPER, WallpaperProps } from '../../constants';
import { HOME_MEMBER_ROLE_NAME } from '../../util/constant';
import { CommonContext } from '../../context/Common';
import mutate_update_home_configs_by_pk from '../../graphql/mutate_update_home_configs_by_pk';
import mutate_insert_home_configs_one from '../../graphql/mutate_insert_home_configs_one';

export default memo(() => {
  const navigation = useNavigation()
  const params = useRoute().params as any
  const { theme, fontSize } = useContext(ThemeContext)
  const { currentUser } = useContext(CommonContext)

  const homeConfig = useSelector((state: RootState) => state.config.homeConfig)

  const [name, setName] = useState(params?.home?.name || '')
  const [changed, setChanged] = useState(false)
  const [members, setMembers] = useState<any>([])
  const [backgrounImage, setBackgrounImage] = useState<any>(homeConfig[params?.home.homeId])
  const dispatch = useDispatch()
  const [location, setLocation] = useState<any>(!!params?.home?.geoName ? {
    name: params?.home?.geoName,
    longitude: params?.home?.lon,
    latitude: params?.home?.lat
  } : null)
  const homeList = useSelector((state: RootState) => state.home.homeList)

  console.log('members', members)

  const _takePhoto = useCallback(async () => {
    try {
      let image: any = await ImagePicker.openCamera({
        includeBase64: true,
        mediaType: 'photo',
        width: 540,
        height: 958,
        cropping: true,
      })
      setChanged(true)
      navigation.navigate('HomeAddChooseImageConfirm', {
        image: { uri: `data:${image.mime};base64,${image.data}` },
        data: {
          uri: `data:${image.mime};base64,${image.data}`,
          type: 'link',
          file: image
        },
        onChoose: (data: any) => {
          navigation.goBack()
          setBackgrounImage(data)
        }
      })
    } catch (error) {

    }
  }, [])
  const _choosePhoto = useCallback(async () => {
    try {
      let image: any = await ImagePicker.openPicker({
        includeBase64: true,
        mediaType: 'photo',
        width: 540,
        height: 958,
        cropping: true,
      })
      setChanged(true)
      navigation.navigate('HomeAddChooseImageConfirm', {
        image: { uri: `data:${image.mime};base64,${image.data}` },
        data: {
          uri: `data:${image.mime};base64,${image.data}`,
          type: 'link',
          file: image
        },
        onChoose: (data: any) => {
          navigation.goBack()
          setBackgrounImage(data)
        }
      })
    } catch (error) {

    }
  }, [])


  const _onSave = useCallback(async () => {
    navigation.navigate('Loading')
    try {
      let _bgImg = { ...(backgrounImage || {}) };
      if (_bgImg.type == 'link') {
        let res = await uploadFile(_bgImg.file)
        console.log('resresres', res)
        if (!res.success) {
          navigation.goBack()
          showMessage({
            type: 'danger',
            message: 'Upload file failed, pls try again.'
          })
          return
        }

        _bgImg = _.omit({
          ..._bgImg,
          uri: res.data.source
        }, ['file'])
      }



      try {
        dispatch(saveHomeConfig({
          homeId: params?.home.homeId,
          config: _bgImg
        }))
      } catch (error) {

      }


      // dispatch(updateCurrentHome({
      //   homeId: params?.home.homeId,
      //   name: name
      // }))
      setTimeout(() => {
        showMessage({
          type: 'success',
          message: 'Update home successfully'
        })
        navigation.goBack()
        navigation.goBack()
      }, 1000);
    } catch (error) {
      navigation.goBack()
      showMessage({
        type: 'danger',
        message: 'Update failed, pls try again.'
      })
    } finally {

    }
  }, [name, backgrounImage, params?.home])

  const _deleteHome = useCallback(() => {
    Alert.alert(
      'Confirm',
      'Are you sure to delete this home?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'Continue', onPress: async () => {
            try {
              navigation.navigate('Loading');

              dispatch(deleteCurrentHome())
              showMessage({
                type: 'success',
                message: 'Delete home successfully'
              })
              navigation.goBack()
              navigation.goBack()
            } catch (error) {
              navigation.goBack()
              showMessage({
                type: 'danger',
                message: 'Delete failed, pls try again.'
              })
            }
          }
        },
      ]
    )
  }, [params?.home, currentUser])


  const canEdit = useMemo(() => { return params?.home.name != name || changed }, [name, params?.home, changed])
  return (
    <View style={{
      flex: 1, backgroundColor: theme?.panel?.backgroundColorGray
    }} >
      <StatusBar barStyle={'dark-content'} />
      <HeaderTheme
        title='Home Setting'
        left={{
          icon: 'icBack',
          onPress: navigation.goBack
        }}
        style={{ backgroundColor: theme?.backgroundColor }}
        right={!!canEdit ? {
          title: 'Save',
          onPress: _onSave
        } : undefined}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: 8 }} >
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Home Name
        </Text>
        <View style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16
        }} >
          <TextInput style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}
            placeholderTextColor={theme?.textBlackPlaceholder}
            placeholder='Enter home name'
            value={name}
            onChangeText={setName}
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Location
        </Text>
        <TouchableOpacity style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center'
        }}
          onPress={() => {
            navigation.navigate('SelectLocation', {
              onSelected: (_value: any) => {
                setLocation(_value)
                setChanged(true)
              }
            })
          }}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            {!location ? 'Choose Location' : location.name}
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Member
        </Text>
        {
          members.map((_member: any) => {
            let isDisabled = _member.role >= 2
            return <TouchableOpacity key={_member.username || _member.uid} style={{
              borderRadius: 8,
              backgroundColor: theme?.buttonBackgroundWhite,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: theme?.borderColor,
              marginHorizontal: 24, marginTop: 8,
              paddingHorizontal: 16,
              flexDirection: 'row', alignItems: 'center',
              paddingVertical: 16
            }}
              disabled={isDisabled}
              onPress={() => navigation.navigate('HomeEditAddMember', { home: params?.home, onSuccess: () => { }, member: _member })}
            >
              <View style={{
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: "#edf0f4",
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: theme?.borderColor,
              }} >
                {!!_member.headPic && <FastImage source={{ uri: _member.headPic }} style={{ flex: 1, borderRadius: 40 }} />}
              </View>
              <View style={{ flex: 1, paddingLeft: 16 }} >
                <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                  <Text style={{
                    fontSize: fontSize?.fontText.f16,
                    fontWeight: "normal",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    color: theme?.textBlack
                  }} >
                    {_member.name}
                  </Text>
                  {
                    _member.role > 0 && <View style={{
                      height: 20,
                      borderRadius: 10,
                      backgroundColor: theme?.mainColor,
                      marginLeft: 4, paddingHorizontal: 8,
                      alignItems: 'center', justifyContent: 'center'
                    }} >
                      <Text style={{
                        fontSize: fontSize?.fontText.f10,
                        fontWeight: "500",
                        fontStyle: "normal",
                        color: theme?.textWhite
                      }} >{HOME_MEMBER_ROLE_NAME[_member.role]}</Text>
                    </View>
                  }
                </View>
                <Text style={{
                  fontSize: fontSize?.fontText.f12,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  letterSpacing: 0,
                  color: theme?.textBlackSecondary,
                  marginTop: 2
                }} >
                  {_member.username}
                </Text>
              </View>
              {!isDisabled && <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />}
            </TouchableOpacity>
          })
        }
        <TouchableOpacity style={{
          marginHorizontal: 24,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          alignItems: 'center', justifyContent: 'center',
          marginTop: 8
        }}
          onPress={() => navigation.navigate('HomeEditAddMember', { home: params?.home, onSuccess: () => { } })}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "center",
            color: theme?.mainColor,
          }} >
            Add member
          </Text>
        </TouchableOpacity>

        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Home Wallpaper
        </Text>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopLeftRadius: 8, borderTopRightRadius: 8,
          borderBottomWidth: StyleSheet.hairlineWidth
        }}
          onPress={_takePhoto}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.mainColor,
            flex: 1
          }}>
            Take Photo
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderBottomWidth: StyleSheet.hairlineWidth, borderTopWidth: StyleSheet.hairlineWidth
        }}
          onPress={_choosePhoto}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Select from Album
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopWidth: StyleSheet.hairlineWidth,
          borderBottomLeftRadius: 8, borderBottomRightRadius: 8,
        }}
          onPress={() => navigation.navigate('HomeAddChooseImage', {
            onChoose: (data: any) => {
              setChanged(true)
              setBackgrounImage(data)
            }
          })}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Choose Wallpaper
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>

        {
          !!backgrounImage && <View style={{
            borderRadius: 8,
            backgroundColor: theme?.buttonBackgroundWhite,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: theme?.borderColor,
            marginHorizontal: 24, marginTop: 16,
            paddingTop: 36, paddingBottom: 16,
            alignItems: 'center'
          }} >
            <View>
              <FastImage source={backgrounImage.type == 'link' ? { uri: backgrounImage.uri } : (BACKGROUD_WALLPAPER[backgrounImage.uri as WallpaperProps] as any)} style={{ height: 300, width: 300 * 540 / 958, borderRadius: 24 }} resizeMode='contain' />
              <View style={{
                position: 'absolute',
                top: 0, left: 0, right: 0, bottom: 0,
                backgroundColor: 'black', opacity: backgrounImage.opacity || 0.1,
                borderRadius: 24,
              }} />
            </View>
            <TouchableOpacity style={{ padding: 16 }}
              onPress={() => setBackgrounImage('')}
            >
              <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                textAlign: "center",
                color: theme?.textDanger,
              }} >
                Remove
              </Text>
            </TouchableOpacity>
          </View>
        }
      </ScrollView>
      {
        homeList.length > 1 && <TouchableOpacity style={{
          marginHorizontal: 24,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginBottom: TabBarSize.paddingBottom + 16,
          alignItems: 'center', justifyContent: 'center'
        }}
          onPress={_deleteHome}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "center",
            color: theme?.textDanger,
          }} >
            Delete Home
          </Text>
        </TouchableOpacity>
      }
    </View>
  );
}
)