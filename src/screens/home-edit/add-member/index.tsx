/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useMemo, useState } from 'react';
import { Alert, ScrollView, StatusBar, Text, TextInput, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../../components/core';
import HeaderTheme from '../../../components/HeaderTheme';
import { ThemeContext } from '../../../context/theme';

import { TabBarSize, validateEmail } from '../../../util';
import { useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { RootState } from '../../../redux/reducers';
import _ from 'lodash';
// import ImagePicker from 'react-native-image-crop-picker';
import FastImage from 'react-native-fast-image';
import { uploadFile } from '../../../api';

export default memo(() => {
  const navigation = useNavigation()
  const params = useRoute().params as any
  const { theme, fontSize } = useContext(ThemeContext)

  const [name, setName] = useState(params?.member?.name || '')
  const [email, setEmail] = useState(params?.member?.username || '')
  const [admin, setAdmin] = useState(params?.member?.admin || false)
  const [backgrounImage, setBackgrounImage] = useState<any>(params?.member?.headPic ? { uri: params?.member?.headPic } : null)

  const _onSave = useCallback(async () => {
    navigation.navigate('Loading')
    try {

      let headPic = backgrounImage?.uri || ''

      // if (!!backgrounImage?.file) {
      //   let resUpload = await uploadFile(backgrounImage.file)
      //   if (!resUpload.success) {
      //     navigation.goBack()
      //     showMessage({
      //       type: 'danger',
      //       message: 'Upload file failed, pls try again.'
      //     })
      //     return
      //   }
      //   headPic = resUpload.data.source
      // }
      console.log('add', {
        homeId: params?.home.homeId,
        userAccount: email,
        name,
        countryCode: '91',
        admin,
        headPic
      })


      // let res = await addMember({
      //   homeId: params?.home.homeId,
      //   userAccount: email,
      //   name,
      //   countryCode: '91',
      //   admin,
      //   headPic
      // })
      // console.log('resAddmebner', res)
      !!params?.onSuccess && params?.onSuccess(params?.home.homeId)
      navigation.goBack()
      navigation.goBack()
      showMessage({
        type: 'success',
        message: 'Member added.'
      })
    } catch (error: any) {
      console.log('resAddmebner', error)
      navigation.goBack()
      showMessage({
        type: 'danger',
        message: error?.message || 'Add member failed, pls try again.'
      })
    } finally {

    }
  }, [name, params?.home, name, email, params?.onSuccess, admin, backgrounImage])

  const _onUpdate = useCallback(async () => {
    navigation.navigate('Loading')
    try {
      let headPic = backgrounImage?.uri

      if (!!backgrounImage?.file) {
        let resUpload = await uploadFile(backgrounImage.file)
        if (!resUpload.success) {
          navigation.goBack()
          showMessage({
            type: 'danger',
            message: 'Upload file failed, pls try again.'
          })
          return
        }
        headPic = resUpload.data.source
      }



      // let res = await updateMember({
      //   homeId: params?.home.homeId,
      //   name,
      //   admin,
      //   memberId: params?.member.id,
      //   headPic
      // })
      // console.log('resAddmebner', res)
      !!params?.onSuccess && params?.onSuccess(params?.home.homeId)
      navigation.goBack()
      navigation.goBack()
      showMessage({
        type: 'success',
        message: 'Member updated.'
      })
    } catch (error: any) {
      console.log('resAddmebner', error)
      navigation.goBack()
      showMessage({
        type: 'danger',
        message: error?.message || 'Update member failed, pls try again.'
      })
    } finally {

    }
  }, [name, params?.home, name, email, params?.onSuccess, admin, params?.member, backgrounImage])

  const _onRemove = useCallback(async () => {
    navigation.navigate('Loading')
    try {
      // let res = await removeMember({
      //   memberId: params?.member.id
      // })
      // console.log('resAddmebner', res)
      !!params?.onSuccess && params?.onSuccess(params?.home.homeId)
      navigation.goBack()
      navigation.goBack()
      showMessage({
        type: 'success',
        message: 'Member removed.'
      })
    } catch (error: any) {
      console.log('resAddmebner', error)
      navigation.goBack()
      showMessage({
        type: 'danger',
        message: error?.message || 'Remove member failed, pls try again.'
      })
    } finally {

    }
  }, [params?.member])


  const _choosePhoto = useCallback(async () => {
    try {
      // let image: any = await ImagePicker.openPicker({
      //   includeBase64: true,
      //   mediaType: 'photo',
      //   width: 512,
      //   height: 512,
      //   cropping: true,
      // })
      // console.log('image', image)
      // setBackgrounImage({
      //   uri: `data:${image.mime};base64,${image.data}`,
      //   file: image,
      //   path: image.path
      // })
    } catch (error) {

    }
  }, [])

  const canEdit = useMemo(() => { return !!name && !!email && (!!params?.member?.username || validateEmail(email)) }, [name, params?.home, email, params?.member?.username])
  return (
    <View style={{
      flex: 1, backgroundColor: theme?.panel?.backgroundColorGray
    }} >
      <StatusBar barStyle={'dark-content'} />
      <HeaderTheme
        title='Home Member'
        left={{
          icon: 'icBack',
          onPress: navigation.goBack
        }}
        right={!!canEdit ? {
          title: 'Save',
          onPress: !!params?.member ? _onUpdate : _onSave
        } : undefined}
        style={{backgroundColor : theme?.backgroundColor}}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: 8 }} >
        {
          !!params?.member && <View style={{
            borderRadius: 8,
            backgroundColor: theme?.buttonBackgroundWhite,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: theme?.borderColor,
            marginHorizontal: 24, marginTop: 24,
            padding: 24,
            alignItems: 'center', justifyContent: 'center'
          }} >
            <View style={{
              width: 80,
              height: 80,
              backgroundColor: "#edf0f4",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: theme?.borderColor,
              borderRadius: 80
            }} >
              <FastImage source={{ uri: backgrounImage?.uri }} style={{ flex: 1, borderRadius: 80 }} />
            </View>
            <TouchableOpacity style={{ marginTop: 16 }}
              onPress={_choosePhoto}>
              <Text style={{
                fontSize: 14,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                textAlign: "center",
                color: theme?.mainColor
              }}
              >Change avatar</Text>
            </TouchableOpacity>
          </View>
        }
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Name
        </Text>
        <View style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16
        }} >
          <TextInput style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}
            placeholderTextColor={theme?.textBlackPlaceholder}
            placeholder='Enter member name'
            value={name}
            onChangeText={setName}
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Email
        </Text>
        <View style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: !!params?.member?.username ? theme?.borderColor : theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16
        }} >
          <TextInput style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}
            autoCapitalize={'none'}
            placeholderTextColor={theme?.textBlackPlaceholder}
            placeholder='Enter email address'
            value={email}
            onChangeText={setEmail}
            editable={!params?.member?.username}
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Role Member
        </Text>
        <TouchableOpacity style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row'
        }}
          onPress={() => navigation.navigate('HomeEditAddMemberChooseRole', {
            onChangeRole: (role: boolean) => setAdmin(role),
            currentRole: admin
          })}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
          }} >{!!admin ? 'Home Admin' : 'Home Member'}</Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>
        <Text style={{
          fontSize: 14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          color: theme?.textBlackSecondary,
          marginTop: 24
        }} >
          This user can control accesories while at this location.
        </Text>
        {
          !!params?.member && (
            <TouchableOpacity style={{
              marginHorizontal: 24,
              height: 50,
              borderRadius: 8,
              backgroundColor: theme?.buttonBackgroundWhite,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: theme?.borderColor,
              marginBottom: TabBarSize.paddingBottom + 16,
              alignItems: 'center', justifyContent: 'center',
              marginTop: 24
            }}
              onPress={() => {
                Alert.alert(
                  'Confirm',
                  'Are you sure to remove this member?',
                  [
                    { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                      text: 'Continue', onPress: _onRemove
                    },
                  ]
                )
              }}
            >
              <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                textAlign: "center",
                color: theme?.textDanger,
              }} >
                Remove Member
              </Text>
            </TouchableOpacity>
          )
        }
      </ScrollView>
    </View>
  );
}
)