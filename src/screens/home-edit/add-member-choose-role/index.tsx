/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useContext } from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../../components/core';
import HeaderTheme from '../../../components/HeaderTheme';
import { ThemeContext } from '../../../context/theme';

import _ from 'lodash';

export default memo(() => {
  const navigation = useNavigation()
  const params = useRoute().params as any
  const { theme, fontSize } = useContext(ThemeContext)

  return (
    <View style={{
      flex: 1, backgroundColor: theme?.panel?.backgroundColorGray
    }} >
      <StatusBar barStyle={'dark-content'} />
      <HeaderTheme
        title='Role Member'
        left={{
          icon: 'icBack',
          onPress: navigation.goBack
        }}
        style={{backgroundColor : theme?.backgroundColor}}
      />

      <View style={{ flex: 1, padding: 24 }} >
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopLeftRadius: 8, borderTopRightRadius: 8,
          borderBottomWidth: StyleSheet.hairlineWidth
        }}
          onPress={() => {
            !!params?.onChangeRole && params?.onChangeRole(true)
            navigation.goBack()
          }}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Home Admin
          </Text>
          {!!params?.currentRole && <Icon name='ic_check' style={{ fontSize: fontSize?.fontText.f16, color: theme?.mainColor }} />}
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopWidth: StyleSheet.hairlineWidth,
          borderBottomLeftRadius: 8, borderBottomRightRadius: 8,
        }}
          onPress={() => {
            !!params?.onChangeRole && params?.onChangeRole(false)
            navigation.goBack()
          }}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Home Member
          </Text>
          {!params?.currentRole && <Icon name='ic_check' style={{ fontSize: fontSize?.fontText.f16, color: theme?.mainColor }} />}
        </TouchableOpacity>
      </View>
    </View>
  );
}
)