/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, CommonActions } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useEffect, useState, } from 'react';
import { ActivityIndicator, Keyboard, StatusBar, Text, TextInput, TouchableWithoutFeedback, View, Platform } from 'react-native'
import { Icon, TouchableOpacity } from '../../components/core';
import { ThemeContext } from '../../context/theme';
import { HeaderSize, TabBarSize } from '../../util';
import CountryPicker, { Country, CountryCode, DARK_THEME, DEFAULT_THEME } from 'react-native-country-picker-modal'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import { Kohana } from 'react-native-textinput-effects';
import { isValidEmail } from '../../util/validate';

import { showMessage } from 'react-native-flash-message';
import SocialLogin from '../../components/SocialLogin';
import { CommonContext } from '../../context/Common';
import DARK from '../../context/theme/dark';
import LIGHT from '../../context/theme/light';

export default memo(() => {
  const { fontSize, theme } = useContext(ThemeContext)
  const { continueLogin } = useContext(CommonContext)
  const navigation = useNavigation()
  const [countryCode, setCountryCode] = useState<CountryCode>('IN')
  const [country, setCountry] = useState<Country>({
    callingCode: ['91']
  } as Country)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')


  // useEffect(() => {
  //   GoogleSignin.configure({
  //     // iosClientId: `17065212809-0fm5qef1i7iacekc6oaepeujurlcdlk6.apps.googleusercontent.com`, //android
  //     webClientId: `778491039333-1qbgarkohle45mbfqtps35dbhfejb1oa.apps.googleusercontent.com`,
  //     offlineAccess: false,
  //     // iosClientId: '205545966412-bno0kafvno0ku11kbgfkn9in5gc96d9b.apps.googleusercontent.com'
  //   });
  // }, []);


  const _login = useCallback(async () => {
    console.log('login', {
      countryCode: country?.callingCode[0] || '',
      email,
      password
    })
    navigation.navigate('Loading')
    try {
      // let res = await loginWithEmail({
      //   countryCode: country?.callingCode[0] || '',
      //   email,
      //   password
      // });

      !!continueLogin && await continueLogin()
      navigation.dispatch(CommonActions.reset({
        index: 0,//the stack index
        routes: [
          { name: 'Main' },//to go to initial stack screen
        ],
      }))
    } catch (error: any) {
      navigation.goBack()
      console.log('error', error)
      showMessage({
        type: 'warning',
        message: error.message
      })
    } finally {
    }
  }, [email, country, password])

  const canContinue = email.trim().length > 0 && isValidEmail(email) && password.trim().length > 0

  return (
    <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} >
      <View style={{
        flex: 1, backgroundColor: theme?.backgroundColor
      }} >
        <StatusBar barStyle='dark-content' />
        <View style={{
          height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
          paddingHorizontal: 16
        }} >
          <TouchableOpacity style={{
            height: '100%', width: HeaderSize.height - HeaderSize.paddingTop,
            alignItems: 'center', justifyContent: "center"
          }}
            onPress={navigation.goBack}
          >
            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.iconBlack }} />
          </TouchableOpacity>
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginTop: 16, marginHorizontal: 32
        }} >
          Log In
        </Text>
        <View style={{
          marginHorizontal: 32,
          marginTop: 32,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }}
        >
          <CountryPicker
            containerButtonStyle={{ height: '100%', justifyContent: 'center' }}
            countryCode={countryCode}
            onSelect={(country: Country) => {
              setCountryCode(country.cca2)
              setCountry(country)
            }}
            withCountryNameButton
            withFilter
            withEmoji
            withFlag
            withAlphaFilter
            theme={DEFAULT_THEME}  // check biến để chuyển thành màu `DARK_THEME`
          />
          <View style={{
            transform: [{ rotate: '-90deg' }], position: 'absolute',
            right: 16, top: 16
          }}
            pointerEvents='none'
          >
            <Icon name='icBack' style={{
              fontSize: 14, color: theme?.iconBlack,
            }} />
          </View>
        </View>
        <View style={{
          marginHorizontal: 32,
          marginTop: 16,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }} >
          <Kohana
            style={{ backgroundColor: 'transparent', height: '100%' }}
            label={'Your email address'}
            iconClass={IconMaterial}
            iconName={'email'}
            iconColor={theme?.mainColor}
            iconSize={24}
            // inputPadding={16}
            labelStyle={{ color: theme?.textBlackPlaceholder, fontWeight: 'normal' }}
            inputStyle={{ color: theme?.iconBlack }}
            labelContainerStyle={{ padding: 0 }}
            iconContainerStyle={{ padding: 0 }}
            useNativeDriver
            value={email}
            onChangeText={setEmail}
            autoCapitalize='none'
          />
        </View>
        <View style={{
          marginHorizontal: 32,
          marginTop: 16,
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: 1,
          borderColor: theme?.borderColor,
          paddingHorizontal: 16,
        }} >
          <Kohana
            style={{ backgroundColor: 'transparent', height: '100%' }}
            label={'Your password'}
            iconClass={IconMaterial}
            iconName={'lock'}
            iconColor={theme?.mainColor}
            iconSize={24}
            // inputPadding={16}
            labelStyle={{ color: theme?.textBlackPlaceholder, fontWeight: 'normal' }}
            inputStyle={{ color: theme?.iconBlack }}
            labelContainerStyle={{ padding: 0 }}
            iconContainerStyle={{ padding: 0 }}
            useNativeDriver
            value={password}
            onChangeText={setPassword}
            autoCapitalize='none'
            secureTextEntry
          />
        </View>
        <TouchableOpacity style={{
          marginHorizontal: 32,
          height: 50,
          borderRadius: 12,
          backgroundColor: !canContinue ? theme?.buttonBackgroundWhiteDisable : theme?.buttonBackgroundMain,
          marginTop: 24,
          alignItems: 'center', justifyContent: 'center'
        }}
          disabled={!canContinue}
          onPress={_login}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "500",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "center",
            color: theme?.textWhite
          }} >
            Log In
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginHorizontal: 32, marginTop: 32 }}
          onPress={() => navigation.navigate('ForgotPassword')}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "500",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.mainColor,
            textAlign: 'center'
          }} >
            Forgot Password
          </Text>
        </TouchableOpacity>
        <View style={{
          flex: 1, alignItems: 'flex-end',
          justifyContent: 'center', flexDirection: 'row',
          paddingBottom: TabBarSize.paddingBottom + 16
        }} >
          {Platform.OS == 'ios' && <SocialLogin type='apple' country={country?.callingCode[0] || ''} style={{ marginHorizontal: 8 }} />}
          <SocialLogin type='google' country={country?.callingCode[0] || ''} style={{ marginHorizontal: 8 }} />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
)