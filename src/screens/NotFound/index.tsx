/**
 * Created by duydatpham@gmail.com 
 * Copyright (c) 2019 duydatpham@gmail.com
 *
 */

import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native'
import { useLinkTo } from '@react-navigation/native';
import { useQuery } from 'react-fetching-library';

export default () => {

  const linkTo = useLinkTo()
  const { loading, payload, error, query } = useQuery({
    method: 'GET',
    endpoint: '/users',
  });
  console.log('loading, payload, error', loading, payload, error)


  return (
    <View style={{
      alignItems: 'center', justifyContent: 'center',
      flex: 1
    }} >
      <Text onPress={() => {
        linkTo('/home')
      }} >
        404 not found
      </Text>
    </View>
  );
}
