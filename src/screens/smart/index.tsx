/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useIsFocused, useNavigation } from '@react-navigation/native';
import React, { memo, useCallback, useContext, useMemo } from 'react';
import { StatusBar, StyleSheet, Text, useWindowDimensions, View } from 'react-native'
import Animated, { interpolate } from 'react-native-reanimated';
import { TabView, SceneMap, SceneRendererProps, NavigationState } from 'react-native-tab-view';
import { useDispatch, useSelector } from 'react-redux';
import { Icon, TouchableOpacity } from '../../components/core';
import { ThemeContext } from '../../context/theme';
import { fetchSceneListAction } from '../../redux/actions/scene';
import { RootState } from '../../redux/reducers';
import { HeaderSize, TabBarSize } from '../../util';
import { ButtonHome } from '../home/popover';
import AutoScene from './AutoScene';
import TapToRun from './TapToRun';
const FirstRoute = () => (
  <TapToRun />
);

const SecondRoute = () => (
  <AutoScene />
);

const renderScene = SceneMap({
  Smart: FirstRoute,
  Automation: SecondRoute,
});
export default memo(() => {
  const layout = useWindowDimensions();
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'Smart', title: 'Smart' },
    { key: 'Automation', title: 'Automation' },
  ]);

  const dispatch = useDispatch()
  const isFocus = useIsFocused();
  const currentHome = useSelector((state: RootState) => state.home.currentHome)
  useMemo(() => {
    if (!!currentHome?.homeId)
      dispatch(fetchSceneListAction(currentHome?.homeId))
  }, [isFocus, currentHome?.homeId])

  const _renderTabBar = useCallback((props: SceneRendererProps & {
    navigationState: NavigationState<any>;
  }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={{
        flexDirection: 'row', alignItems: 'flex-end',
        paddingHorizontal: 8,
      }} >
        {
          props.navigationState.routes.map((route, i) => {
            const opacity = interpolate(props.position, {
              inputRange,
              outputRange: inputRange.map((inputIndex) =>
                inputIndex === i ? 1 : 0.5
              ),
            });

            return (
              <TouchableOpacity
                key={`tab-item-${i}`}
                style={{
                  paddingHorizontal: 16,
                  paddingBottom: 24
                }}
                onPress={() => setIndex(i)}>
                <Animated.Text style={[
                  i == props.navigationState.index ? {
                    fontSize: fontSize?.fontText.f24,
                    fontWeight: "bold",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    color: theme?.textBlack
                  } : {
                    fontSize: fontSize?.fontText.f18,
                    fontWeight: "bold",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    color: theme?.textBlack
                  },
                  { opacity }]}>{route.title}</Animated.Text>
              </TouchableOpacity>
            );
          })}
      </View>
    );
  }, [theme, fontSize])



  return (
    <View style={{
      flex: 1, backgroundColor: theme?.backgroundColor,
      paddingBottom: TabBarSize.height + TabBarSize.paddingBottom
    }} >
      <StatusBar barStyle={'dark-content'} />
      <View style={{
        height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
        backgroundColor: 'transparent',
        alignItems: 'center', flexDirection: 'row',
        paddingHorizontal: 8
      }} >
        <ButtonHome color={theme?.textBlack} />
        <View style={{ flex: 1 }} />
        <TouchableOpacity style={{
          height: '100%', alignItems: 'center', justifyContent: 'center',
          width: HeaderSize.height - HeaderSize.paddingTop,
        }} >
          <Icon name='icVoice' style={{ fontSize: fontSize?.fontIconApp, color: theme?.textBlack }} />
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: '100%', alignItems: 'center', justifyContent: 'center',
          width: HeaderSize.height - HeaderSize.paddingTop,
        }}
          onPress={() => {
            navigation.navigate('SceneStack')
          }}
        >
          <Icon name='icAdd' style={{ fontSize: fontSize?.fontIconApp, color: theme?.textBlack }} />
        </TouchableOpacity>
      </View>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        renderTabBar={_renderTabBar}
      />
    </View>
  );
})
