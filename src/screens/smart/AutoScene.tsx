/*
 * Created by duydatpham@gmail.com on 25/12/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import { useNavigation } from '@react-navigation/native'
import React, { memo, useContext, useMemo, useState, useEffect, useCallback } from 'react'
import { ActivityIndicator, Text, useWindowDimensions, View, Switch } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { FlatList, TouchableOpacity } from '../../components/core'
import { ThemeContext } from '../../context/theme'
import { RootState } from '../../redux/reducers'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { fetchEnableAutomationAction, fetchDisableAutomationAction } from '../../redux/actions/scene'

const AutoSceneItem = memo(({ scene, onToggle }: { scene: any, onToggle?: (value: boolean) => void }) => {
    const { theme, fontSize } = useContext(ThemeContext)
    const { width } = useWindowDimensions()

    const NUMBER_COLUMN = 2
    const [activing, setActiving] = useState(false)

    useEffect(() => {
        enableSceneAutomation()
    }, [])

    const enableSceneAutomation = async () => {
        // dispatch(fetchEnableAutomationAction())
        //    const payload = await enableScene({sceneId : " ag rer ser fe er" })
        //    console.log("payload ::",JSON.stringify(payload))
    }

    const toggleSwitch = useCallback(async (value: boolean) => {

        // const payload = await enableScene({sceneId : "BrKNxEefoi5fQNqd" })
        // if (!!payload) {
        //     showMessage({
        //         type : 'danger',
        //         message : JSON.stringify(payload)
        //     })
        // }
        if (!!onToggle) onToggle(value)
    }, [onToggle])


    return <View style={{
        width: (width - 32) / NUMBER_COLUMN,
        paddingHorizontal: 8,
        marginBottom: 16
    }} >
        <TouchableOpacity style={{
            borderRadius: 12,
            backgroundColor: theme?.buttonBackgroundWhite,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: theme?.borderColor,
            flex: 1,
            paddingBottom: 16
        }}
            onPress={async () => {
            }}
        >
            <View style={{
                flexDirection: 'row', justifyContent: 'space-between',
                alignItems: 'center',
                paddingLeft: 16
            }} >
                {/* <Image source={icon} /> */}
                <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    color: theme?.textBlack,
                    flex: 1
                    // marginHorizontal: 16
                }}
                // numberOfLines={1}
                >
                    {scene.name}
                </Text>
                <TouchableOpacity style={{ width: 56, height: 56, alignItems: 'center', justifyContent: 'center' }} disabled={activing} >
                    {activing ? <ActivityIndicator color={theme?.mainColor} size={'small'} /> : <MaterialIcons name='more-vert' style={{ fontSize: 24, color: theme?.textBlack, }} />}
                </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{
                    fontSize: fontSize?.fontText.f12,
                    fontWeight: "normal",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    color: theme?.textBlackSecondary,
                    marginTop: 4,
                    marginHorizontal: 16,
                    flex: 1
                }} >
                    {`${scene.actions?.length || 0} tasks`}
                </Text>
                <Switch
                    trackColor={{ false: "red", true: theme?.switchActiveBackground || "red" }}
                    thumbColor={scene?.enabled ? theme?.switchActive : theme?.backgroundHeader}
                    ios_backgroundColor={theme?.lineColor}
                    onValueChange={toggleSwitch}
                    value={scene?.enabled}
                    style={{ marginRight: 16 }}
                />

            </View>
        </TouchableOpacity>
    </View>
})


export default memo(() => {
    const { theme, fontSize } = useContext(ThemeContext)
    const { height } = useWindowDimensions()
    const NUMBER_COLUMN = 2
    const navigation = useNavigation()
    const sceneList = useSelector((state: RootState) => state.scene.sceneList)
    const dispatch = useDispatch()

    const data = useMemo(() => {
        return sceneList?.filter((_item: any) => _item?.conditions?.length > 0)
    }, [sceneList])

    const _renderitem = ({ item, index }: any) => {
        return <AutoSceneItem scene={item} onToggle={(value: boolean) => onToggle(value, index)} />
    }

    const onToggle = useCallback((value: boolean, index: number) => {
        if (value) {
            dispatch(fetchEnableAutomationAction(data[index]?.id))
        } else {
            dispatch(fetchDisableAutomationAction(data[index]?.id))
        }
    }, [data])

    return <FlatList
        data={data}
        renderItem={_renderitem}
        keyExtractor={(item, index) => `indexindex-${index}`}
        numColumns={NUMBER_COLUMN}
        horizontal={false}
        style={{ paddingHorizontal: 16 }}
        ListEmptyComponent={() => (
            <View style={{
                alignItems: 'center', justifyContent: 'center',
                marginTop: height / 4
            }} >
                <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "normal",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: theme?.textBlackSecondary
                }} >
                    Create a task and tap it to run.
                </Text>
                <TouchableOpacity style={{
                    width: 150,
                    height: 50,
                    borderRadius: 12,
                    backgroundColor: "#ff9e00",
                    alignItems: 'center', justifyContent: 'center',
                    marginTop: 24
                }}
                    onPress={() => {
                        navigation.navigate('SceneStack')
                    }}
                >
                    <Text style={{
                        fontSize: fontSize?.fontText.f16,
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "center",
                        color: theme?.textWhite
                    }} >Create Scene</Text>
                </TouchableOpacity>
            </View>
        )}
    />
})