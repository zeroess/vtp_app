/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/native';
import React, { memo, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { ActivityIndicator, Animated, Dimensions, Easing, StyleSheet, Text, View } from 'react-native'
import HeaderTheme from '../../components/HeaderTheme';
import { ThemeContext } from '../../context/theme';

// import Geolocation from 'react-native-geolocation-service';

// import MapView, {
//   Marker
// } from 'react-native-maps';
import { HeaderSize, TabBarSize } from '../../util';

// import LottieView from 'lottie-react-native';
import { requestLocation } from '../../util/location';
import { useParameterizedQuery, useQuery } from 'react-fetching-library';
import { fetchAddress } from '../../api/actions/location';


const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default memo(() => {
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)
  const [loading, setLoading] = useState(true)
  const [mapReady, setMapReady] = useState(false)
  const progress = useRef(new Animated.Value(0));
  // const _refMap = useRef<MapView>(null)
  const [currentLocation, setCurrentLocation] = useState<any>()
  const [locationName, setLocationName] = useState<any>()
  const { query, abort } = useParameterizedQuery(fetchAddress)
  const params = useRoute().params as any

  useEffect(() => {
    setTimeout(() => {
      setLoading(false)
    }, 500);

  }, [])

  useMemo(async () => {
    // if (loading || !mapReady) {
    //   return
    // }


    // let resRequestLocation = await requestLocation()
    // if (resRequestLocation) {
    //   let position: any = await new Promise((resolve, reject) => {
    //     Geolocation.getCurrentPosition(
    //       (position) => {
    //         console.log(position);
    //         resolve({
    //           latitude: position.coords.latitude,
    //           longitude: position.coords.longitude,
    //         })
    //       },
    //       (error) => {
    //         // See error code charts below.
    //         console.log(error.code, error.message);
    //         resolve(undefined)
    //       },
    //       { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    //     );
    //   })
    //   console.log(`position`, position)
    //   setCurrentLocation(position)
    //   if (!!position) {
    //     _refMap.current?.animateToRegion({
    //       latitude: position.latitude,
    //       longitude: position.longitude,
    //       latitudeDelta: LATITUDE_DELTA,
    //       longitudeDelta: LONGITUDE_DELTA,
    //     })
    //   }
    // }
  }, [loading, mapReady])


  useEffect(() => {
    setLocationName('')
    if (!!currentLocation)
      (async () => {
        let { payload } = await query({ latlng: `${currentLocation.latitude},${currentLocation.longitude}` })
        if (payload.status == 'OK' && payload.results.length > 0) {
          setLocationName(payload.results[0].formatted_address)
        } else {
        }
      })()

    return () => {
      abort()
    }
  }, [currentLocation])

  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >

      <HeaderTheme
        title='Choose Location'
        left={{
          icon: 'icBack',
          onPress: navigation.goBack,
        }}
        right={{
          title: 'Save',
          style: !!locationName ? {
            fontSize: 16,
            fontWeight: "500",
            fontStyle: "normal",
            color: theme?.buttonBackgroundMain
          } : undefined,
          disabled: !locationName,
          onPress: () => {
            !!params?.onSelected && params?.onSelected({
              ...currentLocation,
              name: locationName
            })
            navigation.goBack()
          }
        }}
      />

      <View style={{
        position: 'absolute', top: HeaderSize.height, left: 0, right: 0, bottom: 0,
        zIndex: 9, alignItems: 'center', justifyContent: 'center'
      }} >
        {/* {
          !loading && <MapView
            provider={'google'}
            ref={_refMap}
            style={StyleSheet.absoluteFillObject}
            // initialRegion={{
            //   latitude: this.props.latitude,
            //   longitude: this.props.longitude,
            //   latitudeDelta: LATITUDE_DELTA,
            //   longitudeDelta: LONGITUDE_DELTA,
            // }}
            onMapReady={() => {
              console.log(`onMapReady`)
              setMapReady(true)
            }}
            onRegionChange={region => {
              console.log(`onRegionChange`, region)
              if (mapReady) {
                Animated.timing(progress.current, {
                  toValue: 0.5,
                  duration: 1000,
                  easing: Easing.linear,
                  useNativeDriver: false
                }).start();
              }
            }}
            onRegionChangeComplete={region => {
              // this.setState({ region })
              // fetchAddressLocation(region)
              setCurrentLocation(region)
              Animated.timing(progress.current, {
                toValue: 0,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: false
              }).start();
            }}
          >
          </MapView>
        }
        {
          !loading && <LottieView source={require('../../../assets/animation/location_animation.json')}// autoPlay loop
            progress={progress.current}
            style={{
              width: 100, height: 100,
              marginBottom: 67,
            }}
          />
        } */}
      </View>
      <View style={{
        height: 120,
        borderRadius: 12,
        backgroundColor: theme?.backgroundColor,
        shadowColor: "rgba(0, 0, 0, 0.1)",
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        position: 'absolute',
        bottom: 24 + TabBarSize.paddingBottom,
        left: 24, right: 24,
        zIndex: 99,
        alignItems: 'center', justifyContent: 'center'
      }} >
        {
          !!locationName ? <>
            <Text style={{
              fontSize: fontSize?.fontText.f24,
              fontWeight: "bold",
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "center",
              color: theme?.textBlack
            }} >
              {locationName.split(',')[0]}
            </Text>
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "center",
              color: theme?.textBlack,
              marginTop: 8
            }} >
              {locationName.split(',').slice(1, 1000).join(', ')}
            </Text>
          </> : <ActivityIndicator color={theme?.mainColor} size={'large'} />
        }
      </View>
    </View>
  );
}
)