import React, { useContext } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { Header, TouchableOpacity, Icon } from '../../components/core'
import { ThemeContext } from '../../context/theme'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'

export default (() => {
    const { fontSize, theme } = useContext(ThemeContext)
    const navigation = useNavigation()
    const { homeList, currentHome } = useSelector((state: RootState) => state.home)

    return (
        <View style={{
            flex: 1,
            backgroundColor: theme?.backgroundColor
        }} >
            <Header
                title="Home Management"
                style={{
                    backgroundColor: theme?.backgroundHeader,
                    shadowColor: "rgba(0, 0, 0, 0.08)",
                    shadowOffset: {
                        width: 0,
                        height: 4
                    },
                    shadowRadius: 8,
                    shadowOpacity: 1,
                    elevation: 8
                }}
                renderLeft={() => {
                    return (
                        <TouchableOpacity onPress={navigation.goBack} style={{ marginLeft: 24 }}>
                            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.unstableText }} />
                        </TouchableOpacity>
                    )
                }}

            />

            <ScrollView style={{ paddingHorizontal: 24, backgroundColor: theme?.secondBackground }}>
                <View style={{ marginTop: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, }}>
                    {homeList.map((_item: any, index: number) => {
                        return (
                            <TouchableOpacity key={`list - ${index}`}
                                onPress={() => navigation.navigate('HomeEdit', { home: _item })}
                                // onPress={() => navigation.navigate("HomeManagement")}
                                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: index === 0 ? 0 : 1, borderColor: theme?.borderColor }}>
                                <Text style={{
                                    fontWeight: "500",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    color: theme?.textBlack,
                                    fontSize: 16
                                }}>{_item?.name}</Text>
                                <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('HomeAdd')} style={{ marginTop: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, padding: 16 }}>
                    <Text style={{ textAlign: 'center', fontSize: 16, color: theme?.mainColor }}>Add Home</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
})