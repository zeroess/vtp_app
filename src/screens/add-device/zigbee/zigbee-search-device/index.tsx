import { useNavigation, useRoute } from '@react-navigation/native'
import React, { useContext, useEffect } from 'react'
import { Image, Text, View } from 'react-native'
import { ic_whtite_search } from '../../../../../assets/images'
import { Header, Icon, TouchableOpacity } from '../../../../components/core'
import { ThemeContext } from '../../../../context/theme'
export default (() => {
    const { theme, fontSize } = useContext(ThemeContext)
    const navigation = useNavigation()
    const { devId } = useRoute().params as any || {}

    useEffect(() => {
        (async () => {
            console.log('newGwSubDevActivator', devId)
            try {
                // let res = await newGwSubDevActivator({
                //     devId,
                //     time: 120
                // })
                // console.log('resresresres', res)
                navigation.reset({
                    index: 0,//the stack index
                    routes: [
                        { name: 'ZigbeeAddDeviceSuccess' },//to go to initial stack screen
                    ],
                })
            } catch (error) {
                console.log('resresresres::error', error)
            } finally {
            }
        })()
        return () => { }
    }, [devId])
    return (
        <View style={{ flex: 1, backgroundColor: theme?.backgroundColor }}>
            <Header
                style={{ backgroundColor: theme?.backgroundColor }}
                renderLeft={() => {
                    return (
                        <TouchableOpacity onPress={navigation.goBack} style={{ marginLeft: 24 }}>
                            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.unstableText }} />
                        </TouchableOpacity>
                    )
                }}
                title="Tìm kiếm thiết bị"
            />
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textGray, marginHorizontal: 48, textAlign: 'center', marginTop: 32 }}>Đang phát hiện thiết bị Zigbee gần đó.</Text>
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.tabIconActiveColor, textAlign: 'center' }}>Đảm bảo thiết bị ở chế độ ghép nối</Text>

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 200, height: 200, borderRadius: 100, backgroundColor: '#FFF9EE', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: 160, height: 160, borderRadius: 80, backgroundColor: '#FEEDD0', justifyContent: 'center', alignItems: 'center', }}>
                        <View style={{ width: 120, height: 120, borderRadius: 60, backgroundColor: "#FED99E", justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: theme?.tabIconActiveColor, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={ic_whtite_search} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                            </View>
                        </View>
                    </View>
                </View>
            </View>


        </View>
    )
})