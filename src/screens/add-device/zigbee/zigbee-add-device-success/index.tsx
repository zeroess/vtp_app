import { useNavigation, useRoute } from '@react-navigation/native'
import React, { useContext } from 'react'
import { Image, Text, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { ic_zigbee_success } from '../../../../../assets/images'
import { Header } from '../../../../components/core'
import { ThemeContext } from '../../../../context/theme'

export default (() => {
    const { theme, fontSize } = useContext(ThemeContext)
    const navigation = useNavigation()
    const { device } = useRoute().params as any || {}
    return (
        <View style={{ flex: 1, backgroundColor: theme?.backgroundColor }}>
            <Header
                style={{ backgroundColor: theme?.backgroundColor }}
                renderRight={() => {
                    return (
                        <Text onPress={navigation.goBack} style={{ fontSize: fontSize?.fontText.f16, textAlign: 'right', marginRight: 24, color: theme?.tabIconActiveColor, fontWeight: 'bold' }}>Done</Text>
                    )
                }}
            />
            <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, textAlign: 'center', marginTop: 32, fontWeight: 'bold', marginBottom: 14 }}>Thêm thành công 1 thiết bị</Text>
            {[device].map((item, index) => {
                return (
                    <View key={`new-device-${index}`} style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: theme?.borderColor, marginTop: 12, borderRadius: 12, padding: 16, marginHorizontal: 24, backgroundColor: theme?.unstableBackground }}>
                        <FastImage source={{ uri: item?.iconUrl }} style={{ width: 40, height: 40,  marginRight: 16 }} resizeMode='contain' />
                        <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack }}>{item.name}</Text>
                    </View>
                )
            })}


        </View>
    )
})