/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { StackActions, useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useContext, useEffect, useRef } from 'react';
import { Animated, Easing, Image, Text, View } from 'react-native';
import { ic_zigbee_success } from '../../../../../assets/images';
import { TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
import { TabBarSize } from '../../../../util';

const TouchableOpacityAnimated = Animated.createAnimatedComponent(View)

export default memo(() => {
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)
  const { devId } = useRoute().params as any || {}
  const slideIn = useRef(new Animated.Value(650)).current;

  React.useEffect(
    () =>
      navigation.addListener('beforeRemove', (e) => {
        e.preventDefault();
        slideIn.stopAnimation();
        Animated.timing(slideIn, {
          toValue: 650,
          duration: 250,
          useNativeDriver: true,
          easing: Easing.out(Easing.quad)
        }).start(() => {
          navigation.dispatch(e.data.action)
        });
      }),
    [navigation]
  );


  useEffect(() => {
    slideIn.stopAnimation();
    Animated.timing(slideIn, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.in(Easing.quad)
    }).start();

  }, [])

  return (
    <View style={{
      flex: 1, justifyContent: 'flex-end',
      backgroundColor: 'rgba(0,0,0,0.8)'
    }} >
      <TouchableOpacity activeOpacity={1} onPress={() => navigation.goBack()} style={{ flex: 1 }}>
        <TouchableOpacityAnimated style={[
          {
            transform: [{ translateY: slideIn }]
          },
          {
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
            width: '100%',
            flex: 1,
            justifyContent: 'flex-end'
          }
        ]}
        >
          <TouchableOpacity activeOpacity={1} style={{ backgroundColor: theme?.unstableBackground, paddingBottom: 24 + TabBarSize.paddingBottom, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}>
            <TouchableOpacity onPress={navigation.goBack} style={{ height: 60, justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: 24 }}>
              <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, fontWeight: '500' }}>Close</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack, fontWeight: 'bold', marginHorizontal: 24, textAlign: 'center' }}>Select the status of the indicator light or hear the beep</Text>
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textGray, marginHorizontal: 24, textAlign: 'center', marginTop: 16, marginBottom: 30 }}>Nếu đèn LED không nhấp nháy, xin vui lòng đặt lại thiết bị, sau đây là một số cách phổ biến để đặt lại.</Text>
            {[1, 2, 3].map((item, index) => {
              return (
                <View key={`list-device-${index}`} style={{ flexDirection: 'row', padding: 16, borderWidth: 1, borderRadius: 12, borderColor: theme?.borderColor, marginHorizontal: 24, marginTop: 8 }}>
                  <Image source={ic_zigbee_success} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                  <View style={{ flex: 1, marginLeft: 16 }}>
                    <Text style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack }}>Cảm biến</Text>
                    <Text style={{ fontSize: fontSize?.fontText.f12, color: theme?.textGray, marginTop: 8 }}>Bật nguồn, bấm và giữ nút đặt lại (công...</Text>
                  </View>
                </View>
              )
            })}
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.tabIconActiveColor, marginHorizontal: 24, textAlign: 'center', marginTop: 24 }}>Các phương pháp đặt lại thiết bị khác</Text>
            <TouchableOpacity onPress={() =>
              navigation.dispatch(StackActions.replace('ZigbeeSearchDevice', { devId }))} style={{ backgroundColor: theme?.tabIconActiveColor, borderRadius: 12, height: 50, justifyContent: 'center', alignItems: 'center', marginTop: 24, marginHorizontal: 24 }}>
              <Text style={{ color: theme?.backgroundColor, fontWeight: '500', lineHeight: 20, fontSize: fontSize?.fontText.f16 }}>Đèn LED đã nhấp nháy</Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </TouchableOpacityAnimated>
      </TouchableOpacity>
    </View >
  );
}
)