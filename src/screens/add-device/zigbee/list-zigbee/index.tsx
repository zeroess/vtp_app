import { useNavigation } from '@react-navigation/native'
import React, { useContext } from 'react'
import { Image, Text, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { useSelector } from 'react-redux'
import { icMoreHoriz, ic_zigbee_success } from '../../../../../assets/images'
import { Header, Icon, TouchableOpacity } from '../../../../components/core'
import { ThemeContext } from '../../../../context/theme'
import { RootState } from '../../../../redux/reducers'
import { TabBarSize } from '../../../../util'

export default (() => {
    const { theme, fontSize } = useContext(ThemeContext)
    const navigation = useNavigation()
    const devices = useSelector((state: RootState) => state.device.deviceList.filter(dev => dev.isGateway))

    return (
        <View style={{ flex: 1, backgroundColor: theme?.backgroundColor }}>
            <Header
                style={{ backgroundColor: theme?.backgroundColor }}
                renderLeft={() => {
                    return (
                        <TouchableOpacity onPress={navigation.goBack} style={{ marginLeft: 24 }}>
                            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.unstableText }} />
                        </TouchableOpacity>
                    )
                }}

                renderRight={() => {
                    return (
                        <Text style={{ fontSize: fontSize?.fontText.f16, color: theme?.tabIconActiveColor, textAlign: 'right', marginHorizontal: 24, fontWeight: '500' }}>Edit</Text>
                    )
                }}
            />
            <View style={{ padding: 24, shadowColor: "rgba(0, 0, 0, 0.08)", shadowOffset: { width: 0, height: 4 }, shadowRadius: 8, shadowOpacity: 1, elevation: 8 }}>
                <Text style={{ color: theme?.textBlack, fontSize: fontSize?.fontText.f24, fontWeight: 'bold', marginTop: 4 }}>Zigbee Smart Gateway</Text>
                <Text style={{ fontSize: fontSize?.fontText.f16, color: theme?.textGray, marginTop: 16 }}>Thiết bị trực tuyến: {devices.filter(dev => dev.isOnline).length}</Text>
            </View>
            {
                devices.length >= 0 ? <View style={{ flex: 1, marginTop: 12, marginHorizontal: 24 }}>
                    {devices.map((item, index) => {
                        return (
                            <TouchableOpacity key={`list-zigbee-${index}`} style={{ flexDirection: 'row', padding: 16, borderWidth: 1, borderColor: theme?.borderColor, marginTop: 12, backgroundColor: theme?.unstableBackground, borderRadius: 12, alignItems: 'center' }}
                                onPress={() => {
                                    navigation.navigate("ZigbeePopupTutorial", { devId: item.devId })
                                }}
                            >
                                <FastImage source={{ uri: item?.iconUrl }} style={{ width: 40, height: 40 }} resizeMode='contain' />
                                <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack, flex: 1, marginHorizontal: 16 }}>{item.name}</Text>
                                <Image source={icMoreHoriz} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        )
                    })}

                </View> : <View style={{ flex: 1, justifyContent: 'center' }}>  list rỗng
                    <Text style={{ color: theme?.textGray, marginHorizontal: 48, textAlign: 'center' }}>Bạn chưa tạo thiết bị nào. Nhấn vào 'Thêm thiết bị' để bắt đầu.</Text>
                </View>
            }

            <TouchableOpacity onPress={() => {
                navigation.navigate("WifiAddDevice")
            }} style={{ height: 80 + TabBarSize.paddingBottom, backgroundColor: theme?.unstableBackground, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', shadowColor: "rgba(0, 0, 0, 0.08)", shadowOffset: { width: 0, height: 4 }, shadowRadius: 8, shadowOpacity: 1, elevation: 8 }}>
                <View style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center', borderRadius: 24, backgroundColor: theme?.tabIconActiveColor, marginRight: 16 }}>
                    <Text style={{ fontWeight: 'bold', color: theme?.textWhite }}>+</Text>
                </View>
                <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, fontWeight: '500' }}>Thêm thiết bị</Text>
            </TouchableOpacity>

        </View>
    )
})