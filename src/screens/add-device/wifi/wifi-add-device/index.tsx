/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/native';
import React, { useContext, useEffect, useMemo, useState } from 'react';
import { Image, Text, TextInput, useWindowDimensions, View, KeyboardAvoidingView } from 'react-native';
import { icActiveEye, icGraySwitch, icLock, ic_black_wifi, imWf } from '../../../../../assets/images';
import { Header, TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
declare const window: { confirm: (p1: any) => void | null };
export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const { height, width } = useWindowDimensions()
  const navigation = useNavigation()
  const [ssid, setSSID] = useState('')
  const [password, setPassword] = useState('')


  const disabled = useMemo(() => {
    return !ssid || !password
  }, [ssid, password])

  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >
      <Header
        renderLeft={() => {
          return (
            <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, marginLeft: 24, fontWeight: '500' }} onPress={navigation.goBack}>Cancel</Text>
          )
        }}
      />
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" >
        <View style={{ marginHorizontal: 24 }}>
          <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, fontWeight: 'bold', textAlign: 'center', marginTop: 32 }}>Select 2.4 GHz Wi-Fi Network and enter password.</Text>
          <Text style={{ marginTop: 16, textAlign: 'center', color: theme?.textGray, fontSize: fontSize?.fontText.f14 }}>If your Wifi is 5GHz, please set it to be 2.4GHz. </Text>
          <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.tabIconActiveColor, textAlign: 'center' }}>Common router setting method.</Text>
          <Image source={imWf} style={{ width: width - 48 * 2, height: 150, marginHorizontal: 24, marginTop: 32 }} />

          <View style={{ borderColor: theme?.lineColor, backgroundColor: theme?.unstableBackground, height: 50, flexDirection: 'row', borderWidth: 1, borderRadius: 12, paddingHorizontal: 18, alignItems: 'center', marginTop: 32 }}>
            <Image source={ic_black_wifi} style={{ width: 24, height: 24, resizeMode: "contain" }} />
            <TextInput
              placeholder="Wifi name"
              placeholderTextColor={theme?.textBlackPlaceholder}
              value={ssid}
              onChangeText={setSSID}
              style={{ color: theme?.iconBlack, flex: 1, marginHorizontal: 18, height: 50, }}
            />
            <Image source={icGraySwitch} style={{ width: 24, height: 24, resizeMode: "contain" }} />
          </View>

          <View style={{ borderColor: theme?.lineColor, backgroundColor: theme?.unstableBackground, height: 50, flexDirection: 'row', borderWidth: 1, borderRadius: 12, paddingHorizontal: 18, alignItems: 'center', marginTop: 16 }}>
            <Image source={icLock} style={{ width: 24, height: 24, resizeMode: "contain" }} />
            <TextInput
              placeholder="Password"
              placeholderTextColor={theme?.textBlackPlaceholder}
              value={password}
              onChangeText={setPassword}
              style={{ color: theme?.iconBlack, flex: 1, marginHorizontal: 18, height: 50, }}
            />
            <Image source={icActiveEye} style={{ width: 24, height: 24, resizeMode: "contain" }} />
          </View>

          <TouchableOpacity onPress={() => navigation.navigate("WifiAddDeviceTutorial", { ssid, password })}
            style={{
              backgroundColor: disabled ? theme?.buttonBackgroundBlackDisable : theme?.buttonBackgroundMain, height: 50, justifyContent: 'center',
              alignItems: 'center', borderRadius: 12, marginTop: 24
            }}
            disabled={disabled}
          >
            <Text style={{ color: theme?.backgroundColor, fontSize: fontSize?.fontText.f16, }}>Continue</Text>
          </TouchableOpacity>

        </View>
      </KeyboardAvoidingView>
    </View>
  );
}
