/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useContext } from 'react';
import { Image, Text, useWindowDimensions, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ic_zigbee_success } from '../../../../../assets/images';
import { Header } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
declare const window: { confirm: (p1: any) => void | null };
export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const { height, width } = useWindowDimensions()
  const navigation = useNavigation()
  const { device } = useRoute().params as any || {}
  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >
      <Header
        style={{ backgroundColor: theme?.backgroundColor }}
        renderRight={() => {
          return (
            <Text onPress={() => navigation.goBack()} style={{ fontSize: fontSize?.fontText.f16, textAlign: 'right', marginRight: 24, color: theme?.tabIconActiveColor, fontWeight: 'bold' }}>Done</Text>
          )
        }}
      />
      <View style={{ alignItems: 'center' }}>
        <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, textAlign: 'center', marginTop: 32, fontWeight: 'bold', marginBottom: 14 }}>Thêm thành công 1 thiết bị</Text>
        <FastImage source={{ uri: device?.iconUrl }} style={{ width: 120, height: 120, marginTop: 80 }} resizeMode='contain' />
        <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack, marginTop: 16 }}>{device?.name}</Text>
      </View>
    </View>
  );
}
