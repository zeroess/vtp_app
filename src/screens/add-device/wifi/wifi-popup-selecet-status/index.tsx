/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { StackActions, useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useContext, useEffect, useRef } from 'react';
import { Animated, Easing, Image, Text, View } from 'react-native';
import { ic_blink_quickly, ic_blink_slowy } from '../../../../../assets/images';
import { TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
import { TabBarSize } from '../../../../util';
const TouchableOpacityAnimated = Animated.createAnimatedComponent(View)

export default memo(() => {
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)
  const { ssid, password } = useRoute().params as any || {}
  const slideIn = useRef(new Animated.Value(500)).current;

  React.useEffect(
    () =>
      navigation.addListener('beforeRemove', (e) => {
        e.preventDefault();
        slideIn.stopAnimation();
        Animated.timing(slideIn, {
          toValue: 500,
          duration: 250,
          useNativeDriver: true,
          easing: Easing.out(Easing.quad)
        }).start(() => {
          navigation.dispatch(e.data.action)
        });
      }),
    [navigation]
  );


  useEffect(() => {
    slideIn.stopAnimation();
    Animated.timing(slideIn, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.in(Easing.quad)
    }).start();

  }, [])

  return (
    <View style={{
      flex: 1, justifyContent: 'flex-end',
      backgroundColor: 'rgba(0,0,0,0.7)'
    }} >
      <TouchableOpacity activeOpacity={1} onPress={() => navigation.goBack()} style={{ flex: 1 }}>
        <TouchableOpacityAnimated style={[
          {
            transform: [{ translateY: slideIn }]
          },
          {
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
            width: '100%',
            flex: 1,
            justifyContent: 'flex-end'
          }
        ]}
        >
          <TouchableOpacity activeOpacity={1} style={{ backgroundColor: theme?.unstableBackground, paddingBottom: 24 + TabBarSize.paddingBottom, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}>
            <TouchableOpacity onPress={navigation.goBack} style={{ height: 60, justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: 24 }}>
              <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, fontWeight: '500' }}>Close</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: fontSize?.fontText.f16, color: theme?.textBlack, fontWeight: 'bold', marginHorizontal: 24, textAlign: 'center' }}>Select the status of the indicator light or hear the beep</Text>
            <View style={{ flexDirection: 'row', marginHorizontal: 12, marginTop: 24 }}>
              <TouchableOpacity onPress={() => navigation.dispatch(StackActions.replace('WifiAddDeviceConfirm', { ssid, password, mode: 'TY_AP' }))} style={{ height: 120, flex: 1, borderRadius: 12, borderWidth: 1, borderColor: theme?.borderColor, marginHorizontal: 8, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={ic_blink_slowy} style={{ width: 40, height: 40, resizeMode: "contain" }} />
                <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack, marginTop: 16 }}>Blink Slowy</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.dispatch(StackActions.replace('WifiSearchDevice', { ssid, password, mode: 'TY_EZ' }))}
                style={{ height: 120, flex: 1, borderRadius: 12, borderWidth: 1, borderColor: theme?.borderColor, marginHorizontal: 8, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={ic_blink_quickly} style={{ width: 40, height: 40, resizeMode: "contain" }} />
                <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack, marginTop: 16 }}>Blink Quickly</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>


        </TouchableOpacityAnimated>
      </TouchableOpacity>
    </View >
  );
}
)