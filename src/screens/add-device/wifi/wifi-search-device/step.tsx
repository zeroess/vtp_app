/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/native';
import React, { memo, useContext } from 'react';
import { Image, Text, View } from 'react-native';
import { icActiveEye, icWhiteSuccess } from '../../../../../assets/images';
import { ThemeContext } from '../../../../context/theme';
declare const window: { confirm: (p1: any) => void | null };

export default memo(({ data, indexStep }: { data: any, indexStep: number }) => {
  const { theme, fontSize } = useContext(ThemeContext);
  const navigation = useNavigation()
  
  return (
    <>
      <View style={{ height: 300 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {data?.map((item: any, index: number) => {
            return (
              <View key={`tab-icon-${index}`} style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ height: 1, flex: 1, backgroundColor: index == 0 ? theme?.backgroundColor : index <= indexStep ? theme?.stepActive : theme?.borderColor }}></View>
                <View style={{ width: 30, height: 30, borderRadius: 15, borderWidth: 1, borderColor: index <= indexStep ? theme?.stepActive : theme?.borderColor, justifyContent: 'center', alignItems: 'center', backgroundColor: index < indexStep ? theme?.stepActive : theme?.backgroundColor }}>
                  <Image source={
                    index < indexStep ? icWhiteSuccess :
                      index == indexStep ? item?.ic_tab_active :
                        index > indexStep ? item?.ic_tab_unActive : icActiveEye
                  } style={{ width: 16, height: 16, resizeMode: 'contain' }} />
                </View>
                <View style={{ height: 1, backgroundColor: index == data?.length - 1 ? theme?.backgroundColor : index < indexStep ? theme?.stepActive : theme?.borderColor, flex: 1 }}></View>
              </View>
            )
          })}
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 16 }}>
          {data?.map((item: any, index: number) => {
            return (
              <View key={`text-tab-${index}`} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ textAlign: 'center', fontSize: fontSize?.fontText.f14, marginHorizontal: 16, color: index <= indexStep ? theme?.textBlack : theme?.textGray }}>{item?.title}</Text>
              </View>
            )
          })}
        </View>
      </View>

    </>
  );
})
