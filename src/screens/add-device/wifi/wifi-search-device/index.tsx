/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { StackActions, useNavigation, useRoute } from '@react-navigation/native';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { Image, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  icGrayCloud, icGraySetting, icGreenCloud, icGreenSearch,
  icGreenSetting, icWhiteCloud, icWhiteSetting, ic_whtite_search
} from '../../../../../assets/images';
import { Header, TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
import { changeStatusWaittingScan } from '../../../../redux/actions/device';
import { RootState } from '../../../../redux/reducers';
import Step from './step';

const DATA = [
  {
    ic_tab_active: icGreenSearch,
    ic_tab_unActive: icGreenSearch,
    ic: ic_whtite_search,
    title: "Tìm kiếm thiết bị"
  },
  {
    ic_tab_active: icGreenCloud,
    ic_tab_unActive: icGrayCloud,
    ic: icWhiteCloud,
    title: "Đăng ký trên cloud"
  },
  {
    ic_tab_active: icGreenSetting,
    ic_tab_unActive: icGraySetting,
    ic: icWhiteSetting,
    title: "Khởi tạo thiết bị"
  }
]
export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const [indexStep, setIndexStep] = useState<number>(0)
  const { ssid, password, mode } = useRoute().params as any || {}
  const currentHome = useSelector((state: RootState) => state.home.currentHome)
  const waittingScan = useSelector((state: RootState) => state.device.waittingScane)

  useEffect(() => {
    _scanDevice(mode)
    return () => {
      dispatch(changeStatusWaittingScan(true))
      // stopConfig()
    }
  }, [mode])

  useEffect(() => {
    if (!waittingScan) {
      setIndexStep(1)
    }
  }, [waittingScan])

  const _scanDevice = useCallback(async (type: 'TY_EZ' | 'TY_AP') => {
    console.log('_scanDevice', ssid,
      password,
      currentHome.homeId)
    try {
      setIndexStep(2)
      setTimeout(() => {
        navigation.reset({
          index: 0,//the stack index
          routes: [
            { name: 'WifiAddDeviceSuccess' },//to go to initial stack screen
          ],
        })
      }, 2000);
    } catch (error) {
      console.log('resresresres::error', error)
    } finally {
    }
  }, [ssid, password, currentHome])

  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >
      <Header
        renderLeft={indexStep == 0 ? () => {
          return (
            <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, marginLeft: 24, fontWeight: '500' }} onPress={navigation.goBack}>Cancel</Text>
          )
        } : undefined}
      />
      <View style={{ marginHorizontal: 24, flex: 1 }}>
        <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, fontWeight: 'bold', textAlign: 'center', marginTop: 32 }}>Đang thêm thiết bị...</Text>
        <Text style={{ marginTop: 16, textAlign: 'center', color: theme?.textGray, fontSize: fontSize?.fontText.f14 }}>Đảm bảo thiết bị của bạn đã bật nguồn và tín hiệu wifi tốt.</Text>

        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity style={{ width: 200, height: 200, borderRadius: 100, backgroundColor: '#FFF9EE', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ width: 160, height: 160, borderRadius: 80, backgroundColor: '#FEEDD0', justifyContent: 'center', alignItems: 'center', }}>
              <View style={{ width: 120, height: 120, borderRadius: 60, backgroundColor: "#FED99E", justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: theme?.tabIconActiveColor, justifyContent: 'center', alignItems: 'center' }}>
                  <Image source={DATA[indexStep]?.ic} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <Step data={DATA} indexStep={indexStep} />

      </View>
    </View>
  );
}
