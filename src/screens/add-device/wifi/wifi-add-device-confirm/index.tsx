/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useContext } from 'react';
import { Image, Text, useWindowDimensions, View } from 'react-native';
import { Header, TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
import { imWf2 } from '../../../../../assets/images'
declare const window: { confirm: (p1: any) => void | null };
export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const { height, width } = useWindowDimensions()
  const { ssid, password, mode } = useRoute().params as any || {}
  const navigation = useNavigation()
  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >
      <Header
        renderLeft={() => {
          return (
            <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, marginLeft: 24, fontWeight: '500' }} onPress={navigation.goBack}>Cancel</Text>
          )
        }}
      />
      <View style={{ marginHorizontal: 24 }}>
        <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, fontWeight: 'bold', textAlign: 'center', marginTop: 32 }}>Kết nối điện thoại của bạn với điểm phát sóng</Text>
        <Text style={{ marginTop: 16, textAlign: 'center', color: theme?.textGray, fontSize: fontSize?.fontText.f14 }}>1. Vui lòng kết nối điện thoại với điểm phát sóng được hiển thị bên dưới</Text>
        <Image source={imWf2} style={{ width: width - 48 * 2, marginHorizontal: 24, marginTop: 32 }} />
        <Text style={{ marginTop: 32, textAlign: 'center', color: theme?.textGray, fontSize: fontSize?.fontText.f14 }}>2. Quay trở lại ứng dụng và tiếp tục thêm thiết bị</Text>
        <Text style={{ marginTop: 16, textAlign: 'center', color: theme?.textGray, fontSize: fontSize?.fontText.f14 }}><Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.tabIconActiveColor, lineHeight: 18 }}>Local Network Access</Text> Device might not be able to be connected if the access if not enabled.</Text>
        <TouchableOpacity onPress={() => {
          navigation.navigate("WifiSearchDevice", { ssid, password, mode })
        }} style={{ backgroundColor: theme?.tabIconActiveColor, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginTop: 24 }}>
          <Text style={{ color: theme?.backgroundColor, fontSize: fontSize?.fontText.f16, }}>Continue</Text>
        </TouchableOpacity>

      </View>
    </View>
  );
}
