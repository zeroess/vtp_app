/*
 * Created by duydatpham@gmail.com on 19/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useContext } from 'react';
import { Text, useWindowDimensions, View } from 'react-native';
import { Header, TouchableOpacity } from '../../../../components/core';
import { ThemeContext } from '../../../../context/theme';
declare const window: { confirm: (p1: any) => void | null };
export default () => {
  const { theme, fontSize } = useContext(ThemeContext);
  const { height, width } = useWindowDimensions()
  const navigation = useNavigation()
  const { ssid, password } = useRoute().params as any || {}
  return (
    <View style={{
      flex: 1,
      backgroundColor: theme?.backgroundColor
    }} >
      <Header
        renderLeft={() => {
          return (
            <Text style={{ color: theme?.tabIconActiveColor, fontSize: fontSize?.fontText.f16, marginLeft: 24, fontWeight: '500' }} onPress={navigation.goBack}>Cancel</Text>
          )
        }}
      />
      <View style={{ marginHorizontal: 24 }}>
        <Text style={{ fontSize: fontSize?.fontText.f18, fontWeight: 'bold', color: theme?.textBlack, textAlign: 'center', marginTop: 28 }}>Vui lòng đặt lại thiết bị trước</Text>
        <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textGray, textAlign: 'center', marginTop: 16 }}>Bật nguồn thiết bị, đảm bảo đèn blue sáng và đèn đỏ nhấp nháy.</Text>
        <TouchableOpacity onPress={() => navigation.navigate("WifiPopupSelectStatusAddDevice", { ssid, password })} style={{ backgroundColor: theme?.tabIconActiveColor, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginTop: 32 }}>
          <Text style={{ color: theme?.backgroundColor, fontSize: fontSize?.fontText.f16, }}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
