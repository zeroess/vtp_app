/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute, StackActions } from '@react-navigation/core';
import React, { memo, useContext, useEffect, useRef, useState } from 'react';
import { Animated, Easing, Text, View, Image, useWindowDimensions } from 'react-native';
import { TouchableOpacity } from '../../../components/core';
import { SceneContext } from '../../../context/SceneContext';
import { ThemeContext } from '../../../context/theme';
import { TabBarSize } from '../../../util';
import { ic_arr_right_gray ,ic_wifi,ic_bluetooth,ic_bitmap,ic_gray_close} from '../../../../assets/images';

const TouchableOpacityAnimated = Animated.createAnimatedComponent(View)
const TYPE_DEVICE = [
  { title: 'Wifi Devices', route: 'WifiAddDevice',ic : ic_wifi },
  { title: 'Zigbee Devices', route: 'ZigbeeList' ,ic : ic_bitmap},
  { title: 'Bluetooth Devices', route: 'AddDeviceStack' ,ic : ic_bluetooth}
]
export default memo(() => {
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)
  const { width } = useWindowDimensions()
  const slideIn = useRef(new Animated.Value(500)).current;

  React.useEffect(
    () =>
      navigation.addListener('beforeRemove', (e) => {
        e.preventDefault();
        slideIn.stopAnimation();
        Animated.timing(slideIn, {
          toValue: 500,
          duration: 250,
          useNativeDriver: true,
          easing: Easing.out(Easing.quad)
        }).start(() => {
          navigation.dispatch(e.data.action)
        });
      }),
    [navigation]
  );


  useEffect(() => {
    slideIn.stopAnimation();
    Animated.timing(slideIn, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.in(Easing.quad)
    }).start();

  }, [])

  return (
    <View style={{
      flex: 1, justifyContent: 'flex-end',
      backgroundColor: 'rgba(0,0,0,0.7)'
    }} >
      <TouchableOpacity activeOpacity={1} onPress={() => navigation.goBack()} style={{ flex: 1 }}>
        <TouchableOpacityAnimated style={[
          {
            transform: [{ translateY: slideIn }]
          },
          {
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
            width: '100%',
            flex: 1,
            justifyContent: 'flex-end'
          }
        ]}
        >
          <TouchableOpacity activeOpacity={1} style={{ backgroundColor: theme?.backgroundColor, paddingBottom: 24 + TabBarSize.paddingBottom, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}>
            <TouchableOpacity onPress={navigation.goBack} style={{ height: 60, justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: 24 }}>
              <Image source={ic_gray_close} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
            </TouchableOpacity>
            <Text style={{ fontSize: fontSize?.fontText.f18, color: theme?.textBlack, fontWeight: 'bold', textAlign: 'center', marginTop: 18 }}>Add Device</Text>
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textGray, marginTop: 16, textAlign: 'center' }}>Scan code to add your devices.</Text>
            <Image
              style={{ width: width - 48, marginLeft: 24, height: 150, borderRadius: 16, marginTop: 16 }}
              source={{ uri: 'https://noitoiseden.com/wp-content/uploads/2018/09/anh-thien-nhien-full-hd-2k-4k.jpg' }}
            />
            <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textGray, marginTop: 32, textAlign: 'center' }}>Manually add devices</Text>
            <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 24 }}>
              {TYPE_DEVICE.map((item, index) => {
                return (
                  <TouchableOpacity onPress={() => navigation.dispatch(StackActions.replace(item?.route))} key={`list-device-${index}`} style={{ paddingVertical: 14, borderWidth: 1, borderColor: theme?.borderColor, backgroundColor: theme?.unstableBackground, width: (width - 24 * 2 - 18 * 2) / 3, height: (width - 24 * 2 - 18 * 2) / 3, borderRadius: 12, marginHorizontal: 9, justifyContent: 'center', alignItems: 'center',paddingHorizontal : 16 }}>
                    <Image source={item?.ic} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
                    <Text style={{ fontSize: fontSize?.fontText.f14, color: theme?.textBlack, marginTop: 14, textAlign: 'center' }} numberOfLines={2}>{item?.title}</Text>
                  </TouchableOpacity>
                )
              })}
            </View>
          </TouchableOpacity>


        </TouchableOpacityAnimated>
      </TouchableOpacity>
    </View >
  );
}
)