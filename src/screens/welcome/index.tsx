/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/core';
import React, { memo, useContext, useRef } from 'react';
import { Image, ImageBackground, StatusBar, Text, useWindowDimensions, View } from 'react-native'

import { icLogo, welcomeBg } from '../../../assets/images'
import { Alert, Icon, TouchableOpacity } from '../../components/core';
import { ThemeContext } from '../../context/theme';

export default memo(() => {
  const { width, height } = useWindowDimensions()
  const { theme, fontSize } = useContext(ThemeContext)
  const navigation = useNavigation()
  const _refAlert = useRef<any>(null)
  const _continueRedirect = useRef<any>(null)

  return (
    <ImageBackground source={welcomeBg} style={{ width, height, }} resizeMode='cover' >
      <StatusBar barStyle='light-content' />
      <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }} >
        {/* <Icon name='logo' style={{ color: theme?.panel?.textWhite, fontSize: 100 }} /> */}
        <Image source={icLogo} />
      </View>
      <View style={{ flex: 1, justifyContent: 'flex-end', paddingBottom: '30%' }} >
        <View style={{ flexDirection: 'row', paddingHorizontal: 24 }} >
          <TouchableOpacity style={{
            flex: 1,
            height: 50,
            borderRadius: 12,
            backgroundColor: theme?.buttonBackgroundMain,
            marginHorizontal: 8,
            alignItems: 'center',
            justifyContent: 'center'
          }}
            onPress={() => {
              _continueRedirect.current = 'Login'
              _refAlert.current.open()
            }}
          >
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "center",
              color: theme?.panel?.textWhite
            }} >Log In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{
            flex: 1,
            height: 50,
            borderRadius: 12,
            backgroundColor: theme?.panel?.backgroundColor,
            marginHorizontal: 8,
            alignItems: 'center',
            justifyContent: 'center'
          }}
            onPress={() => {
              _continueRedirect.current = 'Register'
              _refAlert.current.open()
            }}
          >
            <Text style={{
              fontSize: fontSize?.fontText.f16,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "center",
              color: theme?.panel?.textBlack
            }} >Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>

      <Alert
        ref={_refAlert}
        width={310}
        style={{
          borderRadius: 6
        }}
        backgroundOverlay={theme?.modalBackgroundOverlayColor}
        renderContent={() => {
          return (
            <View style={{
              paddingTop: 24, backgroundColor: theme?.backgroundColor,
            }} >
              <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "bold",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.textBlack,
                marginHorizontal: 16
              }} >User Agreement and Privacy Policy</Text>
              <Text style={{
                fontSize: fontSize?.fontText.f14,
                fontWeight: "normal",
                fontStyle: "normal",
                lineHeight: 21,
                letterSpacing: 0,
                color: theme?.textBlack,
                marginHorizontal: 16,
                marginTop: 16
              }} >We understand the importance of privacy. In order to more fully present our collection and use of your personal information, we have revised our privacy policy and user agreement in detail in accordance with the lastest laws and regulations. When you click [Agree], you have fully read, understood and accepted all of the updated Privacy Policy and User Agreement. Please take some time to become familiar with our privacy policy, and if you have any questions, please feel free to contact us.</Text>
              <Text style={{
                fontSize: fontSize?.fontText.f14,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.textBlack,
                marginHorizontal: 16, marginTop: 24,
                marginBottom: 28
              }} >
                <Text style={{
                  fontSize: fontSize?.fontText.f14,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  letterSpacing: 0,
                  color: theme?.panel?.mainColor,
                }} >User Agreement</Text> and <Text style={{
                  fontSize: fontSize?.fontText.f14,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  letterSpacing: 0,
                  color: theme?.panel?.mainColor,
                }} >Privacy Policy</Text>
              </Text>
              <View style={{
                borderTopColor: theme?.borderColor, borderTopWidth: 1,
                height: 50, flexDirection: 'row'
              }} >
                <TouchableOpacity style={{
                  flex: 1, alignItems: 'center', justifyContent: 'center'
                }}
                  onPress={() => _refAlert.current.close()}
                >
                  <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: theme?.mainColor,
                  }} >
                    Disagree
                  </Text>
                </TouchableOpacity>
                <View style={{
                  backgroundColor: theme?.borderColor, width: 1,
                  height: 50
                }} ></View>
                <TouchableOpacity style={{
                  flex: 1, alignItems: 'center', justifyContent: 'center'
                }}
                  onPress={() => _refAlert.current.close(() => {
                    // navigation.navigate("Login"

                    //   )
                    navigation.navigate(_continueRedirect.current)
                  })}
                >
                  <Text style={{
                    fontSize: fontSize?.fontText.f14,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: theme?.mainColor,
                  }} >
                    Agree
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )
        }}
      />
    </ImageBackground >
  );
}
)