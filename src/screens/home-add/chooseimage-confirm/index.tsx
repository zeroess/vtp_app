/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useContext, useState } from 'react';
import { ImageBackground, Text, useWindowDimensions, View } from 'react-native'
import { TouchableOpacity } from '../../../components/core';
import { ThemeContext } from '../../../context/theme';
import { TabBarSize } from '../../../util';
import RangeSlider from 'rn-range-slider';
export default memo(() => {
  const { theme, fontSize } = useContext(ThemeContext)
  const navigation = useNavigation()
  const params = useRoute().params as any
  const { width, height } = useWindowDimensions()
  const [blurAmount, setBlurAmount] = useState(10)

  console.log('blurAmount', blurAmount)

  return (
    <ImageBackground source={params?.image}
      style={{
        width, height,
        justifyContent: 'flex-end'
      }} >

      {/* <BlurView
        style={{
          position: 'absolute',
          top: 0, left: 0, right: 0, bottom: 0
        }}
        blurType="light"
        blurAmount={blurAmount}
        blurRadius={3}
        downsampleFactor={3}
      /> */}
      <View style={{
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
        backgroundColor: 'black', opacity: blurAmount / 100
      }} />
      <View style={{
        marginHorizontal: 24,
        borderRadius: 12,
        backgroundColor: theme?.backgroundColor,
        marginBottom: 16,
        paddingVertical: 8
      }} >
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          color: theme?.textBlack,
          marginBottom: 8
        }} >
          Overlay opacity
        </Text>
        <RangeSlider
          style={{}}
          // min={brightNess.property.min}
          // max={brightNess.property.max}
          // step={brightNess.property.step}
          min={10}
          max={70}
          step={1}
          floatingLabel
          renderThumb={() => <View style={{
            width: 20, height: 20, borderRadius: 20,
            backgroundColor: theme?.mainColor, borderWidth: 4, borderColor: 'white',
            shadowColor: "rgba(35, 42, 52, 0.2)",
            shadowOffset: {
              width: 0,
              height: 0
            },
            shadowRadius: 4,
            shadowOpacity: 1,
            elevation: 4
          }} />}
          renderRail={() => <View style={{ width: '100%', height: 6, backgroundColor: theme?.mainColorSecondary }} />}
          renderRailSelected={() => <View style={{ width: '100%', height: 6, backgroundColor: theme?.mainColor }} />}
          disableRange
          onTouchEnd={() => { }}
          low={blurAmount}
          onValueChanged={setBlurAmount}
        />
      </View>
      <View style={{
        marginHorizontal: 24,
        height: 50,
        borderRadius: 12,
        backgroundColor: theme?.backgroundColor,
        shadowColor: "rgba(0, 0, 0, 0.1)",
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        elevation: 6,
        marginBottom: TabBarSize.paddingBottom + 36,
        flexDirection: 'row'
      }} >
        <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          onPress={navigation.goBack}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "500",
            fontStyle: "normal",
            color: theme?.textBlack
          }} >
            Cancel
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          onPress={() => {
            params?.onChoose({
              ...(params?.data || {}),
              opacity: blurAmount / 100
            })
            // params?.onChoose({
            //   image: params?.image_name,
            //   opacity: blurAmount
            // })
          }}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "500",
            fontStyle: "normal",
            color: theme?.mainColor
          }} >
            Apply
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
}
)