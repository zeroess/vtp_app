/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/core';
import React, { memo, useCallback, useContext, useMemo, useState } from 'react';
import { Image, ScrollView, StatusBar, StyleSheet, Text, TextInput, View } from 'react-native'
import { Icon, TouchableOpacity } from '../../components/core';
import HeaderTheme from '../../components/HeaderTheme';
import { ThemeContext } from '../../context/theme';

import ImagePicker from 'react-native-image-crop-picker';
import { TabBarSize } from '../../util';
import { useDispatch } from 'react-redux';
import { setListHome } from '../../redux/actions/home';
import { showMessage } from 'react-native-flash-message';
import { uploadFile } from '../../api';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import * as images from '../../../assets/images';
import { saveHomeConfig } from '../../redux/actions/config';
import { BACKGROUD_WALLPAPER, WallpaperProps } from '../../constants';
import { CommonContext } from '../../context/Common';
import mutate_insert_home_configs_one from '../../graphql/mutate_insert_home_configs_one';

export default memo(() => {
  const navigation = useNavigation()
  const { theme, fontSize } = useContext(ThemeContext)
  const [name, setName] = useState('')
  const [location, setLocation] = useState<any>()
  const [backgrounImage, setBackgrounImage] = useState<any>(null)
  const dispatch = useDispatch()

  const { currentUser } = useContext(CommonContext)

  const canContinue = useMemo(() => { return name.trim().length > 0 }, [name])

  const _takePhoto = useCallback(async () => {
    try {
      let image: any = await ImagePicker.openCamera({
        includeBase64: true,
        mediaType: 'photo',
        width: 540,
        height: 958,
        cropping: true,
      })
      navigation.navigate('HomeAddChooseImageConfirm', {
        image: { uri: `data:${image.mime};base64,${image.data}` },
        data: {
          uri: `data:${image.mime};base64,${image.data}`,
          type: 'link',
          file: image
        },
        onChoose: (data: any) => {
          navigation.goBack()
          setBackgrounImage(data)
        }
      })
    } catch (error) {

    }
  }, [])
  const _choosePhoto = useCallback(async () => {
    try {
      let image: any = await ImagePicker.openPicker({
        includeBase64: true,
        mediaType: 'photo',
        width: 540,
        height: 958,
        cropping: true,
      })
      navigation.navigate('HomeAddChooseImageConfirm', {
        image: { uri: `data:${image.mime};base64,${image.data}` },
        data: {
          uri: `data:${image.mime};base64,${image.data}`,
          type: 'link',
          file: image
        },
        onChoose: (data: any) => {
          navigation.goBack()
          setBackgrounImage(data)
        }
      })
    } catch (error) {

    }
  }, [])

  const _onSave = useCallback(async () => {
    navigation.navigate('Loading')
    try {
      let _bgImg = { ...(backgrounImage || {}) };
      if (_bgImg.type == 'link') {
        let res = await uploadFile(_bgImg.file)
        console.log('resresres', res)
        if (!res.success) {
          navigation.goBack()
          showMessage({
            type: 'danger',
            message: 'Upload file failed, pls try again.'
          })
          return
        }

        _bgImg = _.omit({
          ..._bgImg,
          uri: res.data.source
        }, ['file'])
      }

      setTimeout(() => {
        showMessage({
          type: 'success',
          message: 'Add home successfully'
        })
        navigation.goBack()
        navigation.goBack()
      }, 1000);
    } catch (error) {
      console.log('error', error)
      navigation.goBack()
      showMessage({
        type: 'danger',
        message: 'Add failed, pls try again.'
      })
    } finally {

    }
  }, [name, backgrounImage])

  return (
    <View style={{
      flex: 1, backgroundColor: theme?.backgroundColor
    }} >
      <StatusBar barStyle={'dark-content'} />
      <HeaderTheme
        title='Add Home'
        left={{
          title: 'Cancel',
          onPress: navigation.goBack,
          style: { color: theme?.buttonBackgroundMain }
        }}
        style={{ backgroundColor: theme?.backgroundColor }}
        right={{
          title: 'Save',
          style: !canContinue ? {
            fontSize: 16,
            fontWeight: "500",
            fontStyle: "normal",
            color: theme?.buttonBackgroundMain
          } : undefined,
          disabled: !canContinue,
          onPress: _onSave
        }}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: TabBarSize.paddingBottom + 8 }} >
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Home Name
        </Text>
        <View style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16
        }} >
          <TextInput style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}
            placeholderTextColor={theme?.textBlackPlaceholder}
            placeholder='Enter home name'
            value={name}
            onChangeText={setName}
          />
        </View>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Location
        </Text>
        <TouchableOpacity style={{
          height: 50,
          borderRadius: 8,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center'
        }}
          onPress={() => {
            navigation.navigate('SelectLocation', { onSelected: setLocation })
          }}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            {!location ? 'Choose Location' : location.name}
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>
        <Text style={{
          fontSize: fontSize?.fontText.f14,
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          color: theme?.textBlack,
          marginHorizontal: 24, marginTop: 24
        }} >
          Home Wallpaper
        </Text>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24, marginTop: 8,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopLeftRadius: 8, borderTopRightRadius: 8,
          borderBottomWidth: StyleSheet.hairlineWidth
        }}
          onPress={_takePhoto}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.mainColor,
            flex: 1
          }}>
            Take Photo
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderBottomWidth: StyleSheet.hairlineWidth, borderTopWidth: StyleSheet.hairlineWidth
        }}
          onPress={_choosePhoto}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Select from Album
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>
        <TouchableOpacity style={{
          height: 50,
          backgroundColor: theme?.buttonBackgroundWhite,
          borderStyle: "solid",
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: theme?.borderColor,
          marginHorizontal: 24,
          paddingHorizontal: 16,
          flexDirection: 'row', alignItems: 'center',
          borderTopWidth: StyleSheet.hairlineWidth,
          borderBottomLeftRadius: 8, borderBottomRightRadius: 8,
        }}
          onPress={() => navigation.navigate('HomeAddChooseImage', {
            onChoose: (data: any) => {
              setBackgrounImage(data)
            }
          })}
        >
          <Text style={{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            color: theme?.textBlack,
            flex: 1
          }}>
            Choose Wallpaper
          </Text>
          <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
        </TouchableOpacity>

        {
          !!backgrounImage && <View style={{
            borderRadius: 8,
            backgroundColor: theme?.buttonBackgroundWhite,
            borderStyle: "solid",
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: theme?.borderColor,
            marginHorizontal: 24, marginTop: 16,
            paddingTop: 36, paddingBottom: 16,
            alignItems: 'center'
          }} >
            <View>
              <FastImage source={backgrounImage.type == 'link' ? { uri: backgrounImage.uri } : (BACKGROUD_WALLPAPER[backgrounImage.uri as WallpaperProps] as any)} style={{ height: 300, width: 300 * 540 / 958, borderRadius: 24 }} resizeMode='contain' />
              <View style={{
                position: 'absolute',
                top: 0, left: 0, right: 0, bottom: 0,
                backgroundColor: 'black', opacity: backgrounImage.opacity || 0.1,
                borderRadius: 24,
              }} />
            </View>
            <TouchableOpacity style={{ padding: 16 }}
              onPress={() => setBackgrounImage(null)}
            >
              <Text style={{
                fontSize: fontSize?.fontText.f16,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                textAlign: "center",
                color: theme?.textDanger,
              }} >
                Remove
              </Text>
            </TouchableOpacity>
          </View>
        }
      </ScrollView>
    </View>
  );
}
)