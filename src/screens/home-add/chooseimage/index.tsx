/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation, useRoute } from '@react-navigation/core';
import React, { memo, useContext, useMemo } from 'react';
import { Image, ScrollView, StatusBar, Text, useWindowDimensions, View } from 'react-native'
import { welcomeBg } from '../../../../assets/images';
import { TouchableOpacity } from '../../../components/core';
import HeaderTheme from '../../../components/HeaderTheme';
import { BACKGROUD_WALLPAPER, WallpaperProps } from '../../../constants';
import { ThemeContext } from '../../../context/theme';
import { TabBarSize } from '../../../util';
export default memo(() => {
  const { theme } = useContext(ThemeContext)
  const navigation = useNavigation()
  const { width } = useWindowDimensions()
  const params = useRoute().params as any

  const [WIDTH_IMAGE, HEIGHT_IMAGE] = useMemo(() => {
    let _width = (width - 32) / 2 - 16
    return [_width, _width * 1624 / 754]
  }, [width])
  return (
    <View style={{
      flex: 1, backgroundColor: theme?.panel?.backgroundColorGray
    }} >
      <StatusBar barStyle={'dark-content'} />
      <HeaderTheme
        title='Choose Wallpaper'
        left={{
          icon: 'icBack',
          onPress: navigation.goBack
        }}
        style={{backgroundColor : theme?.backgroundColor}}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: TabBarSize.paddingBottom + 8, paddingHorizontal: 16 }} >
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }} >
          {
            Object.keys(BACKGROUD_WALLPAPER).map((_key: string, index) => {
              return <TouchableOpacity key={`bg---${index}`}
                onPress={() => navigation.navigate('HomeAddChooseImageConfirm', {
                  image: BACKGROUD_WALLPAPER[_key as WallpaperProps],
                  image_name: _key, onChoose: (data: any) => {
                    navigation.goBack()
                    navigation.goBack()
                    !!params?.onChoose && params?.onChoose(data)
                  },
                  data: {
                    uri: _key,
                    type: 'static',
                  }
                })}
              >
                <Image source={BACKGROUD_WALLPAPER[_key as WallpaperProps]}
                  style={{
                    width: WIDTH_IMAGE, height: HEIGHT_IMAGE,
                    marginHorizontal: 8, borderRadius: 24,
                    marginTop: 16
                  }} />
              </TouchableOpacity>
            })
          }
        </View>
      </ScrollView>
    </View>
  );
}
)