/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { memo } from 'react';
import { Text, View } from 'react-native'
export default memo(() => {
  return (
    <View style={{
      flex: 1, alignItems: 'center', justifyContent: 'center'
    }} >
      <Text>Thông báo</Text>
    </View>
  );
}
)