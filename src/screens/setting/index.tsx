// import { GoogleSignin } from '@react-native-community/google-signin';
import { CommonActions, useNavigation } from '@react-navigation/native';
import React, { memo, useCallback, useContext, useMemo, useState } from 'react';
import { Alert, Image, Switch, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { icon_ava } from '../../../assets/images';
import { Header, Icon, TouchableOpacity } from '../../components/core';
import { AppColor } from '../../constants';
import { CommonContext } from '../../context/Common';
import { ThemeContext } from '../../context/theme';
import { saveColorApp } from '../../redux/actions/config';
import { RootState } from '../../redux/reducers';
export default memo(() => {
    const { fontSize, theme } = useContext(ThemeContext)
    const navigation = useNavigation()
    const { color_app } = useSelector((state: RootState) => state.config)
    const [isEnabledSound, setIsEnabledSound] = useState(false);
    const [isEnabledNotification, setIsEnabledSoundNotification] = useState(false);
    const [isEnabledColorApp, setIsEnabledColorApp] = useState(color_app === AppColor.DARK);
    const toggleSwitchSound = useCallback(() => setIsEnabledSound(!isEnabledSound), [isEnabledSound]);
    const toggleSwitchNotification = useCallback(() => setIsEnabledSoundNotification(!isEnabledNotification), [isEnabledNotification]);
    const toggleSwitchColorApp = useCallback(() => setIsEnabledColorApp(!isEnabledColorApp), [isEnabledColorApp]);
    const dispatch = useDispatch()
    const { logout } = useContext(CommonContext)
    useMemo(() => {
        if (!isEnabledColorApp) {
            dispatch(saveColorApp(AppColor.LIGHT))
        } else {
            dispatch(saveColorApp(AppColor.DARK))
        }
    }, [isEnabledColorApp])

    const _logout = useCallback(() => {
        setTimeout(() => {
            Alert.alert(
                'Confirm',
                'Are you sure to logout?',
                [
                    { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'Continue', onPress: async () => {
                            navigation.navigate('Loading');

                            // try {
                            //     if (await GoogleSignin.isSignedIn())
                            //         await GoogleSignin.signOut()
                            // } catch (error) {

                            // }

                            (!!logout && await logout())
                            navigation.dispatch(CommonActions.reset({
                                index: 0,//the stack index
                                routes: [
                                    { name: 'Welcome' },//to go to initial stack screen
                                ],
                            }))
                        }
                    },
                ]
            )
        }, 500);
    }, [])


    return (
        <View style={{
            flex: 1,
            backgroundColor: theme?.backgroundColor
        }} >
            <Header
                title="Setting"
                style={{
                    backgroundColor: theme?.backgroundHeader,
                    shadowColor: "rgba(0, 0, 0, 0.08)",
                    shadowOffset: {
                        width: 0,
                        height: 4
                    },
                    shadowRadius: 8,
                    shadowOpacity: 1,
                    elevation: 8
                }}
                renderLeft={() => {
                    return (
                        <TouchableOpacity onPress={navigation.goBack} style={{ marginLeft: 24 }}>
                            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.unstableText }} />
                        </TouchableOpacity>
                    )
                }}

            />
            <View style={{ flex: 1, paddingHorizontal: 24, backgroundColor: theme?.secondBackground }}>
                <TouchableOpacity onPress={() => navigation.navigate("EditInfoUser")} style={{ marginTop: 16, padding: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ backgroundColor: theme?.secondBackground, width: 60, height: 60, borderRadius: 30, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={icon_ava} style={{ width: 32, height: 32, resizeMode: 'contain' }} />
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 16 }}>
                        <Text style={{
                            color: theme?.textBlack,
                            fontSize: 24,
                            fontWeight: 'bold',
                            lineHeight: 28
                        }}>Alex</Text>
                        <Text style={{
                            color: theme?.textGray,
                            marginTop: 4
                        }}>alex@gmail.com</Text>
                    </View>
                    <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                </TouchableOpacity>

                <View style={{ marginTop: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, }}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("HomeManagement")}
                        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 0, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>Home Management</Text>
                        <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 1, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>Sound</Text>
                        <Switch
                            trackColor={{ false: "red", true: theme?.switchActiveBackground || "red" }}
                            thumbColor={isEnabledSound ? theme?.switchActive : theme?.backgroundHeader}
                            ios_backgroundColor={theme?.lineColor}
                            onValueChange={toggleSwitchSound}
                            value={isEnabledSound}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 1, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>App Notifications</Text>
                        <Switch
                            trackColor={{ false: "red", true: theme?.switchActiveBackground || "red" }}
                            thumbColor={isEnabledNotification ? theme?.switchActive : theme?.backgroundHeader}
                            ios_backgroundColor={theme?.lineColor}
                            onValueChange={toggleSwitchNotification}
                            value={isEnabledNotification}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 1, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>Dark Mode</Text>
                        <Switch
                            trackColor={{ false: "red", true: theme?.switchActiveBackground || "red" }}
                            thumbColor={isEnabledColorApp ? theme?.switchActive : theme?.backgroundHeader}
                            ios_backgroundColor={theme?.lineColor}
                            onValueChange={toggleSwitchColorApp}
                            value={isEnabledColorApp}
                        />
                    </View>
                </View>



                <View style={{ marginTop: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, }}>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 0, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>About</Text>
                        <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 1, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>Support</Text>
                        <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 16, borderTopWidth: 1, borderColor: theme?.borderColor }}>
                        <Text style={{
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack,
                            fontSize: 16
                        }}>Privacy Settings</Text>
                        <Icon name='icRight' style={{ fontSize: 18, color: theme?.textBlack, }} />
                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={_logout} style={{ marginTop: 24, padding: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, }}>
                    <Text style={{
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.red,
                        textAlign: 'center',
                        fontSize: 16
                    }}>Logout</Text>
                </TouchableOpacity>
            </View>
        </View>

    );
})
