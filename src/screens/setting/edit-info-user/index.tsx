import { useNavigation } from '@react-navigation/native';
import React, { memo, useContext } from 'react';
import { Image, Text, TextInput, View } from 'react-native';
import { icon_ava } from '../../../../assets/images';
import { Header, Icon, TouchableOpacity } from '../../../components/core';
import { ThemeContext } from '../../../context/theme';

export default memo(() => {
    const { fontSize, theme } = useContext(ThemeContext)
    const navigation = useNavigation()
    return (
        <View style={{
            flex: 1,
            backgroundColor: theme?.backgroundColor
        }} >
            <Header
                title="Personal Info"
                style={{
                    backgroundColor: theme?.backgroundHeader,
                    shadowColor: "rgba(0, 0, 0, 0.08)",
                    shadowOffset: {
                        width: 0,
                        height: 4
                    },
                    shadowRadius: 8,
                    shadowOpacity: 1,
                    elevation: 8
                }}
                renderLeft={() => {
                    return (
                        <TouchableOpacity onPress={navigation.goBack} style={{ marginLeft: 24 }}>
                            <Icon name='icBack' style={{ fontSize: fontSize?.fontIconApp, color: theme?.unstableText }} />
                        </TouchableOpacity>
                    )
                }}
                renderRight={() => {
                    return (
                        <TouchableOpacity>
                            <Text style={{ color: theme?.mainColor, fontSize: 16, textAlign: 'right', marginRight: 16 }}>Save</Text>
                        </TouchableOpacity>
                    )
                }}

            />
            <View style={{ flex: 1, paddingHorizontal: 24, backgroundColor: theme?.secondBackground }}>

                <View style={{ marginTop: 16, padding: 16, backgroundColor: theme?.unstableBackground, borderWidth: 1, borderColor: theme?.lineColor, borderRadius: 12, alignItems: 'center' }}>
                    <View style={{ backgroundColor: theme?.secondBackground, width: 80, height: 80, borderRadius: 40, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={icon_ava} style={{ width: 32, height: 32, resizeMode: 'contain' }} />
                    </View>
                    <TouchableOpacity>
                        <Text style={{ color: theme?.mainColor, paddingTop: 16, paddingHorizontal: 16 }}>Change avatar</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{ color: theme?.textGray, marginTop: 24 }}>Name</Text>
                <TextInput
                    placeholder="Nhập tên"
                    value="heloo"
                    style={{ color: theme?.iconBlack, backgroundColor: theme?.unstableBackground, height: 50, paddingHorizontal: 16, borderRadius: 12, borderWidth: 1, borderColor: theme?.lineColor, marginTop: 16 }}
                />

            </View>
        </View>

    );
})
