/*
 * Created by duydatpham@gmail.com on 14/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from '@react-navigation/core';
import React, { memo, useContext, useRef } from 'react';
import { ActivityIndicator, Text, View } from 'react-native'
import { ThemeContext } from '../../context/theme';
export default memo(() => {
  const { theme } = useContext(ThemeContext)
  const navigation = useNavigation()
  // const _refBack = useRef(false)
  // React.useEffect(
  //   () =>
  //     navigation.addListener('beforeRemove', (e) => {
  //       // Prevent default behavior of leaving the screen
  //       e.preventDefault();
  //       if (_refBack.current)
  //         navigation.dispatch(e.data.action)
  //     }),
  //   [navigation]
  // );

  return (
    <View style={{
      flex: 1, alignItems: 'center', justifyContent: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)'
    }} >
      <View style={{ width: 100, height: 100, borderRadius: 8, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }} >
        <ActivityIndicator color={theme?.mainColor} size='large' style={{ marginLeft: 4 }} />
      </View>
    </View>
  );
}
)