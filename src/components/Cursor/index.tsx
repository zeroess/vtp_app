/*
 * Created by duydatpham@gmail.com on 18/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import * as React from "react";
import { StyleSheet } from "react-native";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
import { atan2 } from "./Math";

const {
  Value, event, block, cond, eq, set, add, sub, multiply, sin, cos, debug,
} = Animated;
type Value = typeof Value;

interface CursorProps {
  radius: number;
  angle: Value;
}

export default ({ radius, angle }: CursorProps) => {
  const a = new Value(0);
  const x = new Value(0);
  const y = new Value(0);
  const xOffset = new Value(0);
  const yOffset = new Value(0);
  const translateX = new Value(0);
  const translateY = new Value(0);
  const translationX = new Value(0);
  const translationY = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const onGestureEvent = event(
    [
      {
        nativeEvent: {
          translationX,
          translationY,
          state,
        },
      },
    ],
  );
  return (
    <>
      <Animated.Code>
        {
          () => block([
            cond(eq(state, State.ACTIVE), [
              set(x, add(xOffset, translationX)),
              set(y, add(yOffset, translationY)),
            ]),
            cond(eq(state, State.END), [
              set(xOffset, x),
              set(yOffset, y),
            ]),
            // set(a, atan2(add(multiply(y, -1), radius), sub(x, radius))),
            // set(angle, a),
            set(translateX, add(multiply(radius, cos(a)), radius)),
            set(translateY, add(multiply(-1 * radius, sin(a)), radius)),
          ])
        }
      </Animated.Code>
      <PanGestureHandler onHandlerStateChange={onGestureEvent} {...{ onGestureEvent }}>
        <Animated.View
          style={{
            ...StyleSheet.absoluteFillObject,
            backgroundColor: "white",
            width: 50,
            height: 50,
            borderRadius: 25,
            transform: [
              { translateX },
              { translateY },
            ],
          }}
        />
      </PanGestureHandler>
    </>
  );
};
