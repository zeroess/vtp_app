/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { memo, ReactNode, useContext } from "react";
import { ScrollView, Text, TextStyle, View, ViewStyle, Image } from "react-native";
import { ThemeContext } from "../context/theme";
import { HeaderSize, TabBarSize } from "../util";
import { Icon, TouchableOpacity } from "./core";
import { ic_white_back } from "../../assets/images";

export default memo(({ title, left, right, style, titleStyle, isLightHeader }: {
    title?: string,
    titleStyle?: TextStyle | ViewStyle | undefined,
    left?: { title?: string, icon?: string, onPress?: () => void, style?: TextStyle | ViewStyle | undefined, disabled?: boolean },
    right?: { title?: string, onPress?: () => void, style?: TextStyle | ViewStyle | undefined, disabled?: boolean, render?: () => ReactNode },
    style?: ViewStyle,
    isLightHeader?: boolean
}) => {
    const { theme, fontSize } = useContext(ThemeContext)
    return <View style={[{
        // backgroundColor: theme?.backgroundColor,
        height: HeaderSize.height, paddingTop: HeaderSize.paddingTop,
        flexDirection: 'row', alignItems: 'center'
    }, style]} >
        <View style={{ flex: 1 }} >
            {
                !!left && <TouchableOpacity style={{
                    height: HeaderSize.height - HeaderSize.paddingTop, minWidth: HeaderSize.height - HeaderSize.paddingTop,
                    alignSelf: 'flex-start', alignItems: 'center',
                    justifyContent: 'center', paddingHorizontal: 24,
                }}
                    onPress={left?.onPress}
                    disabled={left?.disabled}
                >
                    {
                        !!left.title && <Text style={[{
                            fontSize: fontSize?.fontText.f16,
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: theme?.textBlack
                        }, left?.style]} >{left?.title}</Text>
                    }
                    {
                        isLightHeader ?
                            <Image source={ic_white_back} /> :
                            !!left.icon &&
                            <Icon style={[{
                                fontSize: fontSize?.fontText.f24,
                                color: theme?.iconBlack
                            }, left?.style as any]} name={left.icon} />
                    }

                </TouchableOpacity>
            }
        </View>
        <Text style={[{
            fontSize: fontSize?.fontText.f16,
            fontWeight: "bold",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "center",
            color: isLightHeader ? theme?.backgroundColor : theme?.textBlack
        }, titleStyle]} >
            {title}
        </Text>
        <View style={{ flex: 1 }} >
            {
                !!right && !right.render && <TouchableOpacity style={{
                    height: HeaderSize.height - HeaderSize.paddingTop, minWidth: HeaderSize.height - HeaderSize.paddingTop,
                    alignSelf: 'flex-end', alignItems: 'center',
                    justifyContent: 'center', paddingHorizontal: 24,
                }}
                    onPress={right?.onPress}
                    disabled={right?.disabled}
                >
                    <Text style={[{
                        fontSize: fontSize?.fontText.f16,
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        color: theme?.mainColor
                    }, right?.style]} >{right?.title}</Text>
                </TouchableOpacity>
            }
            {
                !!right && !!right.render && right.render()
            }
        </View>
    </View>
})