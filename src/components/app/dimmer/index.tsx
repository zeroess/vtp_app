/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { useCallback, useContext, useState, useEffect, useRef } from 'react'
import { Animated, StyleSheet, Text, View, PanResponder } from 'react-native'
import { FontSizeInterface, ThemeContext } from '../../../context/theme'
import ThemeInterface from '../../../context/theme/types'
import { TouchableOpacity } from '../../core'
import { Easing } from 'react-native-reanimated'

interface SwitchProps {

}
const WIDTH_VERTICAL = 104
const HEIGHT_VERTICAL = 200
export default (props: SwitchProps) => {
    const { fontSize, theme } = useContext(ThemeContext)
    const style = createStyle(fontSize, theme)
    const pan = useRef(new Animated.ValueXY()).current;
    const [animatedValue] = useState(new Animated.Value(0))
    // console.log('panpanpan :::', pan)

    const panResponder = useRef(
        PanResponder.create({
            onMoveShouldSetPanResponder: () => true,
            onPanResponderGrant: () => {
                pan.setOffset({
                    x: 0,
                    y: (pan.y as any)?._value
                });
            },
            // onStartShouldSetPanResponder: (evt, gestureState) => true,
            // onStartShouldSetPanResponderCapture: (evt, gestureState) =>true,
            onPanResponderMove: (...args) => {
                console.log("ARGS", { ...args[1] }.dy)
                Animated.timing(animatedValue, {
                    toValue: { ...args[1] }.dy,
                    duration: 100,
                    useNativeDriver : true
                }).start()
                // Animated.event(
                //     [
                //         null,
                //         { dy: pan.y },
                //     ]
                // )
                // pan.y = {...args[1]}.dy
                // pan = {...args[1]}.dy
            }

            ,
            onPanResponderRelease: () => {
                pan.flattenOffset();
            }
        })
    ).current;

    return <View style={{ height: HEIGHT_VERTICAL, width: WIDTH_VERTICAL, backgroundColor: 'red' }}>
        <Animated.View
            style={{
                transform: [{ translateY: pan.y }]
            }}
            {...panResponder.panHandlers}
        >
            <Animated.View style={[style.box, {
                height: HEIGHT_VERTICAL,
                // transform: [{ translateY: animatedValue }]
                transform: [
                    {
                        translateY: animatedValue.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, 1]
                        })
                    },
                ]
            }]} />
        </Animated.View>
    </View>


}

const createStyle = (fontSize?: FontSizeInterface, theme?: ThemeInterface) => {

    const style = StyleSheet.create({
        box: {

            width: WIDTH_VERTICAL,
            backgroundColor: "blue",
            borderRadius: 5
        }
    })

    return style
}
