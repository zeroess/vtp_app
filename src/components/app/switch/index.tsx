/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { useCallback, useContext, useState, useEffect } from 'react'
import { Animated, StyleSheet, Text, View, useWindowDimensions } from 'react-native'
import { FontSizeInterface, ThemeContext } from '../../../context/theme'
import ThemeInterface from '../../../context/theme/types'
import { TouchableOpacity } from '../../core'

interface SwitchProps {
    type: "HORIZONTAL" | "VERTICAL",
    initialValue: boolean, // trạng thái `true => bật , false => tắt`
    onChange?: (isOpen: boolean) => void
}
// const WIDTH_HORIZONTAL = 140
// const HEIGHT_HORIZONTAL = 40
// const WIDTH_VERTICAL = 104
// const HEIGHT_VERTICAL = 200

export default (props: SwitchProps) => {
    const { fontSize, theme } = useContext(ThemeContext)
    const {width} = useWindowDimensions()
    const WIDTH_HORIZONTAL = 140
    const HEIGHT_HORIZONTAL = 40
    const WIDTH_VERTICAL = (width - 48 - 16)/ 3
    const HEIGHT_VERTICAL = (width - 48 - 16)/ 3 * 2 + 8
    const [indexTab, setIndexTab] = useState<number>(props?.initialValue ? 0 : 1)
    const [space] = useState(new Animated.Value(props?.initialValue ? 0 : props?.type === "HORIZONTAL" ? (WIDTH_HORIZONTAL - 4) / 2 : (HEIGHT_VERTICAL - 8) / 2))
    const style = createStyle(fontSize, theme)
    const [value, setValue] = useState<boolean>(props?.initialValue || false)
    const changeTab = useCallback((index: number) => {
        setIndexTab(index)
        if (index === 1) {
            setValue(false)
            Animated.timing(space, {
                duration: 100,
                toValue: props?.type === "HORIZONTAL" ? (WIDTH_HORIZONTAL - 4) / 2 : (HEIGHT_VERTICAL - 8) / 2,
                useNativeDriver: true
            }).start()
            return
        }
        if (index === 0) {
            setValue(true)
            Animated.timing(space, {
                duration: 100,
                toValue: 0,
                useNativeDriver: true
            }).start()
            return
        }
    }, [space])

    useEffect(() => {
        if (!!props?.onChange) props?.onChange(value)
    }, [value, props?.onChange])

    if (props?.type === "HORIZONTAL") {
        return (
            <View style={style.containerHorizontal}>
                {[1, 2].map((item: any, index: number) => {
                    return (
                        <TouchableOpacity key={`tab - ${index}`} style={style.layoutHorizontal} onPress={() => changeTab(index)}></TouchableOpacity>
                    )
                })}
                <Animated.View style={[{
                    transform: [
                        { translateX: space }
                    ],
                }, style.itemHorizontal]}>
                    <TouchableOpacity onPress={() => indexTab === 0 ? changeTab(1) : changeTab(0)} style={[style.button]}>
                        <Text style={style.title}>{indexTab === 0 ? "On" : "Off"}</Text>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        )
    }
    if (props?.type === "VERTICAL") {
        return (
            <View style={style.containerVertical}>
                {[1, 2].map((item: any, index: number) => {
                    return (
                        <TouchableOpacity key={`tab - ${index}`} style={style.layoutVertical} onPress={() => changeTab(index)}></TouchableOpacity>
                    )
                })}
                <Animated.View style={[{
                    transform: [
                        { translateY: space }
                    ],
                }, style.itemVertical]}>
                    <TouchableOpacity onPress={() => indexTab === 0 ? changeTab(1) : changeTab(0)} style={[style.button]}>
                        <Text style={style.title}>{indexTab === 0 ? "On" : "Off"}</Text>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        )
    }
    return <></>


}

const createStyle = (fontSize?: FontSizeInterface, theme?: ThemeInterface) => {
    const {width} = useWindowDimensions()
    const WIDTH_HORIZONTAL = 140
    const HEIGHT_HORIZONTAL = 40
    const WIDTH_VERTICAL = (width - 48 - 16)/ 3
    const HEIGHT_VERTICAL = (width - 48 - 16)/ 3 * 2 + 8
    const style = StyleSheet.create({
        containerVertical: {
            height: HEIGHT_VERTICAL,
            backgroundColor: theme?.panel?.backgroundColorOpacity,
            borderRadius: 24,
            flexDirection: 'column',
            alignItems: 'center',
            width: WIDTH_VERTICAL,
        },
        containerHorizontal: {
            height: HEIGHT_HORIZONTAL,
            backgroundColor: theme?.panel?.backgroundColorOpacity,
            borderRadius: 12,
            flexDirection: 'row',
            alignItems: 'center',
            width: WIDTH_HORIZONTAL,
        },
        layoutHorizontal: {
            width: (WIDTH_HORIZONTAL - 4) / 2,
            height: HEIGHT_HORIZONTAL,
        },
        layoutVertical: {
            width: WIDTH_VERTICAL,
            height: HEIGHT_VERTICAL / 2,
        },
        itemVertical: {
            position: 'absolute',
            top: 4,
            left: 4,
            height: (HEIGHT_VERTICAL - 8) / 2,
            width: WIDTH_VERTICAL - 8,
            backgroundColor: "white",
            borderRadius: 24,
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 2,
            borderColor: "white"
        },
        itemHorizontal: {
            position: 'absolute',
            top: 2,
            left: 2,
            height: (HEIGHT_HORIZONTAL - 4),
            width: (WIDTH_HORIZONTAL - 4) / 2,
            backgroundColor: "white",
            borderRadius: 12,
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 2,
            borderColor: "white"
        },
        title: {
            fontSize: fontSize?.fontText.f16,
            fontStyle: "normal",
            fontWeight: '500',
            letterSpacing: 0,
            color: theme?.panel?.textBlack,
        },
        button: {
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',

        }

    })

    return style
}
