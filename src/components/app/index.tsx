import Switch from './switch'
import Dimmer from './dimmer'
export {
    Switch,
    Dimmer
}