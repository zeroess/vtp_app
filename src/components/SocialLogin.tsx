/*
 * Created by duydatpham@gmail.com on 20/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { memo, useCallback, useContext } from 'react'
import { ViewStyle } from 'react-native'
import { ThemeContext } from '../context/theme'
import { Icon, TouchableOpacity } from './core'

import { showMessage } from 'react-native-flash-message';
// import { GoogleSignin } from '@react-native-community/google-signin';
// import { appleAuth } from '@invertase/react-native-apple-authentication';
import { useNavigation, CommonActions } from '@react-navigation/core';
import { CommonContext } from '../context/Common';


export default memo(({ type, style, country }: { type: 'google' | 'apple', style?: ViewStyle, country: string }) => {
    const { theme } = useContext(ThemeContext)
    const { continueLogin } = useContext(CommonContext)
    const navigation = useNavigation()

    const onGoogleButtonPress = useCallback(async () => {
        try {
            // // Get the users ID token
            // const { idToken, user, } = await GoogleSignin.signIn();
            // console.log('idToken', idToken, user)
            // navigation.navigate('Loading')


            // console.log('resTuyaLogin', resTuyaLogin)

            !!continueLogin && await continueLogin()
            navigation.dispatch(CommonActions.reset({
                index: 0,//the stack index
                routes: [
                    { name: 'Main' },//to go to initial stack screen
                ],
            }))
        } catch (error) {
            console.log('error', error)
            navigation.goBack()
            showMessage({
                message: 'Login failed, pls try again.',
                type: 'danger'
            })
        }
    }, [country])
    const onAppleButtonPress = useCallback(async () => {
        try {
            // const appleAuthRequestResponse = await appleAuth.performRequest({
            //     requestedOperation: appleAuth.Operation.LOGIN,
            //     requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
            // });
            // navigation.navigate('Loading')
            // // Ensure Apple returned a user identityToken
            // if (!appleAuthRequestResponse.identityToken) {
            //     throw 'Apple Sign-In failed - no identify token returned';
            // }
            // console.log('appleAuthRequestResponse', appleAuthRequestResponse)
            // // Create a Firebase credential from the response
            // const { identityToken, email, fullName, user } = appleAuthRequestResponse;
            // // let resTuyaLogin = await loginWithApple({
            // //     accessToken: identityToken || '',
            // //     countryCode: country,
            // //     credential_email: email || 'duydatpham@gmail.com',
            // //     credential_nickname: fullName?.nickname || 'Duy Dat Pham',
            // //     credential_user: user,

            // // })
            // // console.log('resTuyaLogin', resTuyaLogin)

            // !!continueLogin && await continueLogin()
            // navigation.dispatch(CommonActions.reset({
            //     index: 0,//the stack index
            //     routes: [
            //         { name: 'Main' },//to go to initial stack screen
            //     ],
            // }))
        } catch (error) {
            console.log('error', error)
            navigation.goBack()
            showMessage({
                message: 'Gặp lỗi trong quá trình đăng nhập. Xin vui lòng thử lại.',
                type: 'danger'
            })
        }
    }, [country])

    return (
        <TouchableOpacity style={[{
            width: 50,
            height: 50,
            backgroundColor: theme?.buttonBackgroundWhite,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: theme?.borderColor,
            alignItems: 'center', justifyContent: 'center',
            borderRadius: 50,
        }, style]}

            onPress={type == 'apple' ? onAppleButtonPress : (type == 'google' ? onGoogleButtonPress : undefined)}
        >
            {type == 'apple' && <Icon name='icApple' style={{ fontSize: 24 ,color : theme?.iconBlack}} />}
            {type == 'google' && <Icon name='icGoogle' style={{ fontSize: 24 ,color : theme?.iconBlack}} />}
        </TouchableOpacity>
    )
})