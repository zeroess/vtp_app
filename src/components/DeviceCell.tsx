/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { useNavigation } from "@react-navigation/core";
import React, { memo, useCallback, useContext, useEffect, useMemo } from "react";
import { Text, useWindowDimensions, View } from "react-native";
import FastImage from "react-native-fast-image";
import { useSelector } from "react-redux";
import { ThemeContext } from "../context/theme";
import { RootState } from "../redux/reducers";
import { DeviceAttribute } from "../redux/reducers/device";
import { Icon, TouchableOpacity } from "./core";

export default memo(({ padding, device }: { padding: number, device: DeviceAttribute }) => {

    const { theme, fontSize } = useContext(ThemeContext);
    const { width } = useWindowDimensions()
    const navigation = useNavigation()
    const currentStatus = useSelector((state: RootState) => state.device.deviceStatus[device.devId]) || {}
    const home = useSelector((state: RootState) => state.home.currentHome) || {}

    const [WIDTH_CELL] = useMemo(() => {
        let _w = (width - padding * 4) / 2
        return [_w]
    }, [width])

    const statusSwitchDp = useMemo(() => {
        return currentStatus[device.switchDp]
    }, [currentStatus, device])

    const _controlSwitchDp = useCallback(async () => {
        try {

        } catch (error) {
        }

        // let res = await getHomeDetail({ homeId: home.homeId })
        // console.log('res', res, home.homeId)
    }, [device.devId, device.switchDp, statusSwitchDp])

    return <View style={{
        width: WIDTH_CELL,
        padding,
        paddingBottom: padding + 8
    }} >
        <TouchableOpacity style={{
            width: '100%',
            height: 120,
            borderRadius: 12,
            backgroundColor: "#ffffff",
            shadowColor: "rgba(0, 0, 0, 0.1)",
            shadowOffset: {
                width: 0,
                height: 5
            },
            shadowRadius: 20,
            shadowOpacity: 1,
            elevation: 8,
            paddingBottom: 16
        }}
            onPress={async () => {
                navigation.navigate('DeviceControl', { device })
                // try {
                //     let res = await send({
                //         devId: device.devId,
                //         command: {
                //             [2]: 10,
                //             // [1]: 'stop',
                //         },
                //     })
                //     console.log('res', res)
                // } catch (error) {
                // }
            }}
        >
            <View style={{
                flexDirection: 'row',
                marginBottom: 20,
            }} >
                <FastImage source={{ uri: device.iconUrl }} style={{ width: 30, height: 30, margin: 16, marginBottom: 0 }} resizeMode='contain' />
                <View style={{ flex: 1 }} />
                {
                    !!device.switchDp && device.isOnline && <TouchableOpacity style={{ padding: 16 }}
                        onPress={_controlSwitchDp}
                    >
                        <View style={[{
                            width: 22,
                            height: 22,
                            borderRadius: 22,
                            alignItems: 'center', justifyContent: 'center'
                        }, statusSwitchDp ? {
                            backgroundColor: theme?.mainColor
                        } : {
                            borderStyle: "solid",
                            borderWidth: 1,
                            borderColor: theme?.borderColor
                        }]} >
                            <Icon name='icOnOff' style={[{ fontSize: 18, }, statusSwitchDp ? {
                                color: theme?.textWhite
                            } : {
                                color: theme?.borderColor
                            }]} />
                        </View>
                    </TouchableOpacity>
                }
                {
                    !device.isOnline && <Text style={{
                        fontSize: fontSize?.fontText.f12,
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "right",
                        color: theme?.textDanger,
                        margin: 16
                    }} >
                        Offline
                    </Text>
                }
            </View>
            <Text style={{
                fontSize: fontSize?.fontText.f14,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.textBlack, marginHorizontal: 16
            }} numberOfLines={1} >
                {device.name}
            </Text>
            <Text style={{
                fontSize: fontSize?.fontText.f12,
                fontWeight: "normal",
                fontStyle: "normal",
                letterSpacing: 0,
                color: theme?.textBlackSecondary,
                marginTop: 4, marginHorizontal: 16,
            }} >
                No in room
            </Text>
        </TouchableOpacity>
    </View>
})