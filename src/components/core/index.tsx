/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import Header from './header'
import Container from './container'
import Content from './content'
import FlatList from './FlatList'
import TouchableOpacity from './TouchableOpacity'
import Alert from './Alert'
import ActionSheet from './ActionSheet';
import TabBar from './TabBar/TabBar';
import Icon from './Icon';
import Switch from './Switch';

export {
    Header, Container,
    Content, FlatList, TouchableOpacity,
    Alert,
    ActionSheet,
    TabBar,
    Icon,
    Switch
}