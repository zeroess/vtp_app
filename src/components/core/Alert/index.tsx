/**
* Created by duydatpham@gmail.com on Wed Oct 31 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/


import React, { PureComponent } from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity,
  Modal, Animated, Easing, Dimensions, Platform, ViewPropTypes, TextStyle, ViewStyle
} from 'react-native';

const SCREEN = Dimensions.get('window');
const STATUS_BAR_OFFSET = (Platform.OS === 'android' ? -25 : 0);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: SCREEN.width,
    height: SCREEN.height,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  viewButton: {
    height: 42,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'gray'
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtButton: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center'
  },
  viewContent: {
    alignItems: 'center',
    padding: 16
  },
  content: {

  },
  txtTitle: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  txtMessage: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center'
  }
});

interface AlertState {
  scale: Animated.AnimatedValue;
  openVal: Animated.AnimatedValue;
  isAnimating: boolean;
  isOpen: boolean;
  target: {
    x: number,
    y: number,
    opacity: number,
  }
}

class Alert extends PureComponent<AlertProps, AlertState> {
  private dataAlert?: any;
  private timeoutAlertLeftButton?: any;
  private timeoutAlertmiddleButton?: any;
  private timeoutAlertRightButton?: any;


  static defaultProps = {
    origin: {
      x: SCREEN.width / 2,
      y: SCREEN.height / 2,
      width: 0,
      height: 0
    },
    springConfig: { tension: 30, friction: 7 },
    isOpen: false,
    backgroundOverlay: 'black',
    width: Math.min(266, SCREEN.width - 16),
    hasTextInput: false,
    clickToClose: true,
  }

  constructor(props: AlertProps) {
    super(props);
    this.state = {
      isAnimating: false,
      isOpen: this.props.isOpen || false,
      target: {
        x: 0,
        y: 0,
        opacity: 0.6,
      },
      openVal: new Animated.Value(0),
      scale: new Animated.Value(0)
    }

    this.dataAlert = null
  }

  alert = (props: AlertProps) => {
    this.dataAlert = {
      title: props.title ? props.title : null,
      message: props.message ? props.message : null,
      leftButton: props.leftButton ? props.leftButton : null,
      rightButton: props.rightButton ? props.rightButton : null,
      onClose: props.onClose ? props.onClose() : null
    }
    this.open()
  }

  open = () => {
    this.setState({
      isAnimating: true,
      isOpen: true,
      target: {
        x: 0,
        y: 0,
        opacity: 0.6,
      }
    });

    let toScaleConfig = {
      ...this.props.springConfig,
      useNativeDriver: false
    };
    toScaleConfig.toValue = 1;


    Animated.parallel([
      Animated.spring(this.state.scale, toScaleConfig as any),
      Animated.timing(this.state.openVal, {
        toValue: 1,
        duration: 100,
        easing: Easing.quad
      } as Animated.TimingAnimationConfig)
    ]).start(() => {
      this.setState({ isAnimating: false });
      this.props.onOpen && this.props.onOpen()
    });
  }

  close = (onClose: any) => {
    this.setState({
      isAnimating: true
    });
    Animated.parallel([
      Animated.timing(this.state.scale, {
        toValue: 0,
        duration: 100,
        easing: Easing.quad
      } as Animated.TimingAnimationConfig),
      Animated.timing(this.state.openVal, {
        toValue: 0,
        duration: 100,
        easing: Easing.quad
      } as Animated.TimingAnimationConfig)
    ]).start(() => {
      this.setState({
        isAnimating: false,
        isOpen: false,
      });
      this.dataAlert && this.dataAlert.onClose && this.dataAlert.onClose()
      this.dataAlert = null;
      this.props.onClose && this.props.onClose()
      onClose && onClose()
    })
  }

  componentWillReceiveProps(props: AlertProps) {
    if (this.props.isOpen != props.isOpen && props.isOpen) {
      this.open();
    }
  }

  componentWillUnmount() {
    if (this.timeoutAlertLeftButton) {
      clearTimeout(this.timeoutAlertLeftButton)
    }

    if (this.timeoutAlertRightButton) {
      clearTimeout(this.timeoutAlertRightButton)
    }
  }

  renderButton = () => {
    let leftButton: any;
    let rightButton: any;
    let middleButton: any
    if (this.dataAlert) {
      leftButton = this.dataAlert.leftButton
      rightButton = this.dataAlert.rightButton
      middleButton = this.dataAlert.middleButton
    } else {
      leftButton = this.props?.leftButton
      rightButton = this.props.rightButton
      middleButton = this.props.middleButton
    }

    if (!leftButton && !rightButton && !middleButton) return null

    return (
      <View style={[styles.viewButton, { width: Math.min(266, SCREEN.width - 16) }]}>
        {
          leftButton && leftButton.text && (
            <TouchableOpacity
              onPress={() => {
                this.close(null);
                if (this.timeoutAlertLeftButton) {
                  clearTimeout(this.timeoutAlertLeftButton)
                }

                this.timeoutAlertLeftButton = setTimeout(() => {
                  leftButton.onPress && leftButton.onPress()
                }, 220)
              }}
              style={styles.button}
            >
              <Text style={styles.txtButton}>
                {leftButton.text}
              </Text>
            </TouchableOpacity>
          )
        }
        {
          middleButton && middleButton.text && (
            <TouchableOpacity
              onPress={() => {
                this.close(null);
                if (this.timeoutAlertmiddleButton) {
                  clearTimeout(this.timeoutAlertmiddleButton)
                }

                this.timeoutAlertmiddleButton = setTimeout(() => {
                  middleButton.onPress && middleButton.onPress()
                }, 220)
              }}
              style={[
                styles.button,
                leftButton ? { borderLeftWidth: 1, borderLeftColor: 'gray' } : undefined
              ]}
            >
              <Text style={styles.txtButton}>
                {middleButton.text}
              </Text>
            </TouchableOpacity>
          )
        }
        {
          rightButton && rightButton.text && (
            <TouchableOpacity
              onPress={() => {
                if (!rightButton.dontClose) {
                  this.close(null);
                  if (this.timeoutAlertRightButton) {
                    clearTimeout(this.timeoutAlertRightButton)
                  }

                  this.timeoutAlertRightButton = setTimeout(() => {
                    rightButton.onPress && rightButton.onPress()
                  }, 220)
                } else {
                  rightButton.onPress && rightButton.onPress()
                }
              }}
              style={[
                styles.button,
                (leftButton || middleButton) ? { borderLeftWidth: 1, borderLeftColor: 'gray' } : undefined
              ]}
            >
              <Text style={styles.txtButton}>
                {rightButton.text}
              </Text>
            </TouchableOpacity>
          )
        }
      </View>
    )
  }

  renderContent = () => {
    const {
      renderContent
    } = this.props;
    let message = null
    let title = null

    if (this.dataAlert) {
      message = this.dataAlert.message
      title = this.dataAlert.title
    } else {
      message = this.props.message
      title = this.props.title
    }


    if (renderContent) {
      return renderContent()
    }
    // if (!message && !title) return null

    return (
      <View style={styles.viewContent}>
        {
          title && <Text style={styles.txtTitle}>{title}</Text>
        }
        {
          message && <Text style={[styles.txtMessage, title ? { marginTop: 4 } : undefined]}>{message}</Text>
        }
      </View>
    )
  }

  render() {
    const {
      origin,
      backgroundOverlay,
      width,
      style,
      hasTextInput, clickToClose
    } = this.props;

    const {
      isOpen,
      isAnimating,
      openVal,
      scale,
      target,
    } = this.state;

    const lightboxOpacityStyle = {
      opacity: openVal.interpolate({ inputRange: [0, 1], outputRange: [0.1, target.opacity] })
    }

    const openStyle = [{
      left: openVal.interpolate({ inputRange: [0, 1], outputRange: [(origin?.x || 0), target.x] }),
      top: openVal.interpolate({ inputRange: [0, 1], outputRange: [(origin?.y || 0) + STATUS_BAR_OFFSET, target.y + STATUS_BAR_OFFSET] }),
      width: openVal.interpolate({ inputRange: [0, 1], outputRange: [(origin?.width || 0), SCREEN.width] }),
      height: openVal.interpolate({ inputRange: [0, 1], outputRange: [(origin?.height || 0), SCREEN.height] }),
      transform: [
        {
          scale: scale
        }
      ]
    }];

    const background = (
      <Animated.View
        style={[
          styles.background,
          { backgroundColor: backgroundOverlay },
          lightboxOpacityStyle
        ]}
      >
        {
          clickToClose && <TouchableOpacity style={{ flex: 1, backgroundColor: 'transparent' }}
            onPress={() => this.close(null)}
            activeOpacity={1}
          />
        }
      </Animated.View>
    );

    const content = (
      <Animated.View pointerEvents='box-none' style={[openStyle, styles.center]}>
        <View style={
          [
            styles.content,
            {
              width: width,
              borderRadius: 12,
              overflow: 'hidden',
              backgroundColor: 'white',
            }, style
          ]}
        >
          <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.4)' }]} />
          {this.renderContent()}
          {this.renderButton()}
        </View>
      </Animated.View>
    )

    return (
      <Modal visible={isOpen} transparent={true} onRequestClose={() => this.close(null)} >
        {background}
        {content}
      </Modal>
    );
  }
}
interface ActionProps {
  text?: string;
  onPress?: () => {};
  textStyle?: TextStyle;
  dontClose?: boolean;
}

interface AlertProps {
  origin?: {
    x: number,
    y: number,
    width: number,
    height: number,
  };
  springConfig?: Animated.SpringAnimationConfig;
  message?: string;
  title?: string;
  leftButton?: ActionProps;
  middleButton?: ActionProps;
  rightButton?: ActionProps;
  isOpen?: boolean;
  backgroundOverlay?: string;
  width?: number;
  style?: ViewStyle;
  renderContent?: () => {};
  onOpen?: () => {};
  onClose?: () => {};
  hasTextInput?: boolean;
  clickToClose?: boolean;
}

export default Alert;
