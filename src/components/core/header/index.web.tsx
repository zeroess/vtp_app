/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { StatusBar, View, Text, ViewStyle, TextStyle } from 'react-native'
import { TouchableOpacity } from '..';
import { HeaderSize } from '../../../util'

interface ActionProps {
    title?: string;
    onPress?: () => void;
    titleStyle?: TextStyle;
    iconName?: string;
    style?: TextStyle;
    render?: () => {}
}

interface HeaderProps {
    hasShadow?: boolean;
    style?: ViewStyle;
    titleStyle?: TextStyle;
    renderLeft?: () => {};
    renderTitle?: () => {};
    renderRight?: () => {};
    left?: ActionProps;
    right?: ActionProps;
    title?: string;
}


export default (props: HeaderProps) => {
    const { hasShadow, style, left, right, titleStyle, title, renderTitle } = props
    let header = (<View style={[{
        backgroundColor: 'transparent',
        borderBottomColor: 'transparent',
        alignItems: 'center',
        paddingTop: HeaderSize.paddingTop,
        height: HeaderSize.height,
        flexDirection: 'row',
        width: '100%'
    }, style, hasShadow ? {
        position: 'absolute', top: 0, left: 0, right: 0, zIndex: 9999,
        backgroundColor: 'white',
        shadowColor: "rgba(0, 0, 0, 0.08)",
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 1,
        elevation: 4
    } : {}]}>
        <View style={{ flex: 1 }} >
            {
                props.renderLeft ? props.renderLeft() : (
                    !!left ? (
                        <TouchableOpacity onPress={left.onPress} style={{
                            height: '100%', paddingHorizontal: 16,
                            justifyContent: 'center',
                        }}  >
                            {
                                !!left.title ? <Text style={left.titleStyle} >{left.title || ''}</Text> : (
                                    // left.iconName && <Icon style={[{ color: 'black', fontSize: 32 }, left.style]} name={left.iconName} />
                                    <View />
                                )
                            }
                        </TouchableOpacity>
                    ) : undefined
                )
            }
        </View>
        {
            !!renderTitle && !!!title && <View style={{ flex: 2, alignItems: 'center' }} >
                {renderTitle()}
            </View>
        }
        {
            !!title && !!!renderTitle && <View style={{ flex: 2, alignItems: 'center' }} >
                <Text style={[{
                    fontSize: 16,
                    fontWeight: "600",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "center",
                    color: "#34324c"
                }, titleStyle]}
                    numberOfLines={1}
                >
                    {title}
                </Text>
            </View>
        }
        <View style={{ flex: 1 }} >
            {
                props.renderRight ? props.renderRight() : (
                    !!right ? (
                        <TouchableOpacity onPress={right.onPress} style={{
                            height: '100%', paddingHorizontal: 16,
                            justifyContent: 'center',
                        }}  >
                            {
                                !!right.render ? right.render() : (
                                    !!right.title ? <Text style={right.titleStyle || {
                                        fontSize: 18,
                                        fontWeight: "500",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "black"
                                    }} >{right.title || ''}</Text> : (
                                            right.iconName && <View /> //<Icon style={[{ color: 'black', fontSize: 32 }, right.titleStyle]} name={right.iconName} />
                                        )
                                )
                            }
                        </TouchableOpacity>
                    ) : undefined
                )
            }
        </View>
    </View>)
    return header
}