
/**
 * Created by duydatpham@gmail.com on Tue Oct 23 2018
 * Copyright (c) 2018 duydatpham@gmail.com
 */
import DropdownAlert from 'react-native-dropdownalert';
import React, { PureComponent } from 'react'

import { HeaderSize } from '../../../util';

declare const global: {
    window: {
        alertCustom: any
    }
};

export default class extends PureComponent {
    _dropdown: any = React.createRef();

    _showError = (message = '', title = '') => {
        this._dropdown?.current.alertWithType('error', title || '', message, 5000)
    }
    _showWarn = (message = '', title = '') => {
        this._dropdown.current.alertWithType('warn', title || '', message, 5000)
    }
    _showSuccess = (message = '', title = '') => {
        this._dropdown.current.alertWithType('success', title || '', message, 5000)
    }
    _showInfo = (message = '', title = '') => {
        this._dropdown.current.alertWithType('info', title || '', message, 5000)
    }

    componentDidMount() {
        global.window.alertCustom = {
            showError: this._showError,
            showWarn: this._showWarn,
            showSuccess: this._showSuccess,
            showInfo: this._showInfo,
        }
    }

    render() {
        return <DropdownAlert
            ref={this._dropdown} closeInterval={2000}
            defaultContainer={{
                paddingTop: HeaderSize.paddingTop,
                flexDirection: 'row',
                alignItems: 'center',
                height: HeaderSize.height,
                paddingHorizontal: 16
            }}
        />
    }
}