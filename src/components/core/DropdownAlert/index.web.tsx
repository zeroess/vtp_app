
/**
 * Created by duydatpham@gmail.com on Tue Oct 23 2018
 * Copyright (c) 2018 duydatpham@gmail.com
 */
import React, { PureComponent } from 'react'
import { View } from 'react-native';

import { HeaderSize } from '../../../util';

export declare const global: {
    window: {
        env: {
            URL_API: string
        },
        alertCustom: any
    }
};
export default class extends PureComponent {
    private _dropdown?: any;

    _showError = (message = '', title = '') => {
        this._dropdown.alertWithType('error', title || '', message, 5000)
    }
    _showWarn = (message = '', title = '') => {
        this._dropdown.alertWithType('warn', title || '', message, 5000)
    }
    _showSuccess = (message = '', title = '') => {
        this._dropdown.alertWithType('success', title || '', message, 5000)
    }
    _showInfo = (message = '', title = '') => {
        this._dropdown.alertWithType('info', title || '', message, 5000)
    }

    componentDidMount() {
        global.window.alertCustom = {
            showError: this._showError,
            showWarn: this._showWarn,
            showSuccess: this._showSuccess,
            showInfo: this._showInfo,
        }
    }

    render() {
        return <View />
        // return <DropdownAlert
        //     ref={ref => this._dropdown = ref} closeInterval={2000}
        //     defaultContainer={{
        //         paddingTop: HeaderSize.paddingTop,
        //         flexDirection: 'row',
        //         alignItems: 'center',
        //         height: HeaderSize.height,
        //         paddingHorizontal: 16
        //     }}
        // />
    }
}