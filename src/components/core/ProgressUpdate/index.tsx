/*
 * Created by duydatpham@gmail.com on 12/5/2020
 * Copyright © 2020 duydatpham@gmail.com
 */

import React, { PureComponent } from "react"
import { View, Text } from 'react-native'
import style from './style'

export interface ProgressUpdateMethod {
    show: (message: string) => {}
}


export default class ProgressUpdate extends PureComponent {

    state = {
        show: false,
        message: ''
    }



    show(message: string) {
        if (!!!message || message.length == 0) {
            this.setState({
                show: false
            })
            return
        }

        this.setState({
            show: true,
            message
        })
    }

    render() {
        const { show, message } = this.state
        if (!show)
            return null
        return (
            <View style={style.container}>
                <View style={style.containerPopup} >
                    <Text style={{
                        color: 'white'
                    }} >
                        {message}
                    </Text>
                </View>
            </View>
        )
    }
}
