/*
 * Created by duydatpham@gmail.com on 12/5/2020
 * Copyright © 2020 duydatpham@gmail.com
 */

import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    alignItems: 'center', justifyContent: 'center'
  },
  containerPopup: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    alignItems: 'center', justifyContent: 'center',
    padding: 20, borderRadius: 4
  }
});
