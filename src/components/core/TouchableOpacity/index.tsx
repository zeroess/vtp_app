/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { TouchableOpacity, Keyboard, TouchableOpacityProps, GestureResponderEvent } from 'react-native'
import _ from 'lodash';

interface TouchableProps extends TouchableOpacityProps {
    refThis?: (item: any) => {}
}

export default class extends PureComponent<TouchableProps> {
    __onPress = () => {
        if (this.props.onPress) {
            this.props.onPress(this)
        }
    }
    throttlePress = _.throttle(this.__onPress, 500, { 'trailing': false });
    render() {
        const {
            onPress, children,
            refThis,
            ...rest
        } = this.props
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                {...rest}
                ref={!!refThis ? _btn => refThis(_btn) : undefined}
                onPress={() => {
                    Keyboard.dismiss()
                    this.throttlePress()
                }}
            >
                {children}
            </TouchableOpacity>
        )
    }
}