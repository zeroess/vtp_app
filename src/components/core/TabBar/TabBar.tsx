/**
* Created by duydatpham@gmail.com on Mon Jun 04 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions, Platform, ViewPropTypes, Text, StyleProp, ViewProps, TextStyle } from 'react-native';
import { TabBarSize, } from '../../../util';
import { TouchableOpacity } from '..';
import { connect } from 'react-redux'
const styles = StyleSheet.create({
  container: {
    height: TabBarSize.height + TabBarSize.paddingBottom,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(96, 96, 96, 0.2)",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    elevation: -2,
    paddingBottom: TabBarSize.paddingBottom,
    overflow: 'visible',
  },
  addStyleBtn: {
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: "#03b9de",
    shadowColor: "rgba(3, 185, 222, 0.5)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 9,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewContent: {
    flex: 1,
    justifyContent: 'center',
    overflow: 'visible'
  },
  tabbarView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible'
  },
  viewTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 2
  },
  next: {
    position: 'absolute',
    top: 20,
    right: 2,
    backgroundColor: 'transparent'
  },
  prew: {
    position: 'absolute',
    top: 20,
    left: 2,
    backgroundColor: 'transparent'
  },
  icon: {
    width: 28,
    height: 28,
    marginBottom: 2
  },
  separator: {
    height: StyleSheet.hairlineWidth
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  textPerTab: {
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0,
  }
});


const { width } = Dimensions.get('window')

class TabBar extends PureComponent<TabBarProps> {
  static defaultProps = {
    backgroundColor: 'steelblue',
    colorIconActive: 'white',
    colorIconInactive: 'rgba(0, 0, 0, 0.6)',
    titleSize: Platform.OS === "ios" ? 12 : 11,
    swipeTab: false,
    separator: false,
    bgSeparator: 'rgba(0, 0, 0, 0.6)',
    hasLabel: false
  }

  constructor(props: TabBarProps) {
    super(props);
    this.state = {
      orientation: 'portrait',
      page: true,
      show: true,
    }
  }



  render() {
    let {
      style,
      navigation,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      onTabPress
    } = this.props;


    const routes = navigation.state.routes.slice()
    const widthTab = width / routes.length
    const contentsView = (
      <View style={styles.tabbarView}>
        {
          routes.map((route, id) => {
            const { icon, title } = route.routes[0].params
            let focused = navigation.state.index === id
            return (
              <TouchableOpacity
                onPress={() => {
                  onTabPress({ route })
                }}
                style={styles.viewTab}
                activeOpacity={.8}
                key={route.routeName}

              >
                {
                  icon({ ...route.routes[0].params, focused })
                }
                {
                  title ?
                    <Text style={[styles.textPerTab, focused ? { color: activeTintColor } : { color: inactiveTintColor }]}>{title()}</Text>
                    :
                    null
                }

              </TouchableOpacity>
            )
          })
        }
      </View>
    )

    return (
      <View
        style={styles.container}
      >
        <View style={[styles.viewContent, style]}>
          {contentsView}
        </View>

      </View>
    );
  }
}

interface TabBarProps {
  backgroundColor?: string,
  style?: StyleProp<ViewProps>,
  colorIconActive?: string,
  colorIconInactive?: string,
  titleStyle?: TextStyle,
  titleSize?: number,
  swipeTab?: boolean,
  separator?: boolean,
  bgSeparator?: string,
  hasLabel?: boolean
}


// const mapDispatchToProps = {
// }

// const mapStateToProps = (state, props) => {
//   return {
//     language: state.config.language
//   }
// }
// export default Loading;
export default TabBar //connect(mapStateToProps, mapDispatchToProps)(TabBar);

