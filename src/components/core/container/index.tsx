/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { TouchableWithoutFeedback, Keyboard, View, ViewProps } from 'react-native'

interface ContainerProps extends ViewProps {
    dismisKeboard?: boolean;
    children?: any
}

export default (props: ContainerProps) => {
    if (props.dismisKeboard)
        return <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
            <View {...props}>
                {props.children}
            </View>
        </TouchableWithoutFeedback>
    else
        return <View {...props}>
            {props.children}
        </View>
}