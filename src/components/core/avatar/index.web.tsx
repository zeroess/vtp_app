/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { Image, View, Platform, ImageSourcePropType } from 'react-native'

interface AvatarProps {
    avatar: string;
    refThis?: (item: any) => {};
    hasBorder?: boolean;
    size: number;
    resizeMode: 'cover' | 'contain';
    default?: ImageSourcePropType
}

export default class extends React.Component<AvatarProps> {

    static defaultProps: AvatarProps = {
        size: 60,
        resizeMode: 'cover',
        default: undefined,
        avatar: ''
    }

    shouldComponentUpdate(nextProps: AvatarProps) {
        if (this.props.avatar != nextProps.avatar)
            return true
        return false
    }
    render() {
        const { refThis, avatar, resizeMode } = this.props
        let size = this.props.hasBorder ? this.props.size - 2 : this.props.size
        let borderRadiusDefault = Platform.OS == 'android' ? size * 2 : size / 2
        return (
            <View
                ref={!!refThis ? _view => refThis(_view) : undefined}
                style={{
                    width: this.props.size,
                    height: this.props.size,
                    borderRadius: this.props.size / 2,
                    borderWidth: 1,
                    borderColor: "rgb(226, 226, 226)",
                    alignItems: 'center',
                    justifyContent: 'center',
                }} >
                {
                    !!avatar && <View style={{
                        width: size,
                        height: size,
                        borderRadius: (size) / 2,
                        overflow: 'hidden',
                    }} >
                    </View>
                }
                {
                    !!!avatar && <Image
                        borderRadius={borderRadiusDefault}
                        style={{
                            width: size,
                            height: size,
                            // borderRadius: (size) / 2,
                        }} source={this.props.default || require('./avtDf.png')} resizeMode='contain' />
                }
            </View>
        )
    }
}