import React, { memo } from 'react'
import { createIconSetFromIcoMoon, } from 'react-native-vector-icons';
import { IconProps } from 'react-native-vector-icons/Icon';
const iconMoonConfig = require('../../../../assets/fonts/selection.json');

const IconVector = createIconSetFromIcoMoon(iconMoonConfig);

interface IProps extends IconProps {

}

export default memo((props: IProps) => <IconVector {...props} />);