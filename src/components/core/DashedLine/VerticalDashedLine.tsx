import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, StyleProp, ViewProps, LayoutChangeEvent } from 'react-native'
import Svg, { Line } from 'react-native-svg';

class VerticalDashedLine extends React.PureComponent<VerticalDashedLineProps> {
    static defaultProps = {
        color: "black",
        width: 4,
        space: 3,
        style: {},
        height: 5
    }
    state = {
        heightContainer: 0
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { x, y, height, width } = event.nativeEvent.layout;

        this.setState({ heightContainer: height });
    }
    render() {
        const { heightContainer } = this.state
        const { color, width, space, style, height } = this.props
        return (
            <View style={style} onLayout={this.onLayout} >
                <Svg height={heightContainer} width={width} style={{ alignSelf: 'center' }}>
                    <Line
                        stroke={color}
                        strokeDasharray={`${height}, ${space}`}
                        strokeWidth={width}
                        x1="0"
                        y1="0"
                        x2="0"
                        y2={heightContainer}
                    />
                </Svg>
            </View>
        );
    }
}

interface VerticalDashedLineProps {
    /**
     * Color of line
     */
    color: string,
    /**
     * width of line
     */
    width: number,
    /**
     * space of dash
     */
    space: number,
    style: StyleProp<ViewProps>,
    /**
     * height of line
     */
    height: number
}

export default VerticalDashedLine