import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, StyleProp, ViewProps, LayoutChangeEvent } from 'react-native'
import Svg, { Line } from 'react-native-svg';

class HorizontalDashedLine extends React.PureComponent<HorizontalDashedLineProps> {

    static defaultProps = {
        color: "black",
        width: 4,
        space: 3,
        style: {},
        height: 5
    }

    state = {
        widthContainer: 0
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { width } = event.nativeEvent.layout;

        this.setState({ widthContainer: width });
    }
    render() {
        const { widthContainer } = this.state
        const { color, width, space, style, height } = this.props
        return (
            <View style={style} onLayout={this.onLayout} >
                <Svg width={widthContainer} height={height} style={{ alignSelf: 'center' }}>
                    <Line
                        stroke={color}
                        strokeDasharray={`${height}, ${space}`}
                        strokeWidth={width}
                        x1="0"
                        y1="0"
                        x2={widthContainer}
                        y2="0"
                    />
                </Svg>
            </View>
        );
    }
}

interface HorizontalDashedLineProps {
    /**
     * Color of line
     */
    color: string,
    /**
     * width of line
     */
    width: number,
    /**
     * space of dash
     */
    space: number,
    style: StyleProp<ViewProps>,
    /**
     * height of line
     */
    height: number
}

export default HorizontalDashedLine