/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { View, KeyboardAvoidingView, ScrollView, Platform, ViewPropTypes, ViewProps, StyleProp } from 'react-native'
import { TabBarSize, HeaderSize } from '../../../util';

interface ContentProps extends ViewProps {
    hasShadow?: boolean;
    disableScroll?: boolean;
    children?: any;
    refThis?: (item: any) => {};
    contentContainerStyle?: StyleProp<ViewProps>;
}

export default (props: ContentProps) => {
    const { hasShadow, disableScroll, children, style, refThis, ...rest } = props
    // const Container = !disableScroll ? (Platform.OS == 'ios' ? Content : ScrollView) : View
    const Container = !disableScroll ? ScrollView : View

    const newProps: ContentProps = {

    }

    if (disableScroll) {
        newProps.style = [style, hasShadow ? { paddingTop: HeaderSize.height, flex: 1 } : {}]
    } else {
        newProps.contentContainerStyle = [style, hasShadow ? { paddingTop: HeaderSize.height } : {}]
    }
    // if (Platform.OS == 'android')
    return <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" >
        <Container
            ref={view => !!refThis && refThis(view)}
            {...rest}
            showsVerticalScrollIndicator={false}
            {...newProps}  >
            {children}
        </Container>
    </KeyboardAvoidingView>
}