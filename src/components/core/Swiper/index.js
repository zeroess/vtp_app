/**
* Created by nghinv on Sat Jul 07 2018
* Copyright (c) 2018 nghinv
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Animated, ViewPropTypes, PanResponder, Platform } from 'react-native';
import Interactable from 'react-native-interactable';
import { PanGestureHandler, State } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {

  },
  viewContent: {
    position: 'relative',
    overflow: 'hidden'
  },
  viewControl: {
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.6
  }
});

class Swiper extends PureComponent
// <Props> 
{
  constructor(props) {
    super(props);
    this.scale = new Animated.Value(1)
    this.enableAlertEvent = false
    this.currentValue = null
    this.startFrom = null;
    this._currentIndex = null;
    this.refControl = React.createRef()
  }

  _onHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.state == State.BEGIN) {
      this.props.onPan && this.props.onPan(true);
      this._onPanResponderGrant()
    }

    if (nativeEvent.state == State.END) {
      this._onPanResponderRelease()
    }
  }
  

  _onPanResponderGrant = () => {
    console.log('_onPanResponderGrant')
    Animated.spring(this.scale, {
      toValue: this.props.scale,
      friction: 300
    }).start()
  }

  _onSnap = ({ nativeEvent }) => {
    this._currentIndex = nativeEvent.index;
    this.props.onValueChange && this.props.onValueChange(this.getValueFromIndex(nativeEvent.index))
    this.props.onSwiperComplete && this.props.onSwiperComplete(this.getValueFromIndex(nativeEvent.index))
  }

  _onDrag = ({ nativeEvent }) => {
    this.enableAlertEvent = true
    if (nativeEvent.state === 'start') {
      this.startFrom = nativeEvent.y;
    }

    if (nativeEvent.state === 'end') {
      this._currentIndex = nativeEvent.index;
      this.enableAlertEvent = false
      this.props.scaleWhenPress && this.resetScale()

      if (this.startFrom != null && this.startFrom == 0) {
        this.props.onValueChange && this.props.onValueChange(this.getValueFromIndex(0))

        return;
      }

      if (this.startFrom != null && this.startFrom != 0 && nativeEvent.y == 0) {
        this.props.onValueChange && this.props.onValueChange(this.getValueFromIndex(0))
        this.props.onSwiperComplete && this.props.onSwiperComplete(this.getValueFromIndex(0))
      }
    }
  }

  getValue = (val) => {
    const { minLevel, maxLevel, step } = this.props;
    return (val == '0' ? 0 : parseInt(val)) + minLevel
  }

  getValueFromIndex = (index) => {
    const { minLevel, maxLevel, step } = this.props;
    return (minLevel + index * step)
  }

  _onAlertEvent = ({ nativeEvent }) => {
    if (!this.enableAlertEvent) return
    for (var i = 0; i < Object.keys(nativeEvent).length; i++) {
      for (key in Object.keys(nativeEvent)) {
        if (Object.keys(nativeEvent)[key].includes(`alertPoint`)) {
          if (!this.currentValue) {
            this.currentValue = Object.keys(nativeEvent)[key].slice(10)
            this.props.onValueChange && this.props.onValueChange(this.getValue(this.currentValue))
          } else {
            if (this.currentValue == Object.keys(nativeEvent)[key].slice(10)) {
              return
            } else {
              this.currentValue = Object.keys(nativeEvent)[key].slice(10)
              this.props.onValueChange && this.props.onValueChange(this.getValue(this.currentValue))
            }
          }
        }
      }
    }
  }

  _onStop = () => {
    this.props.onPan && this.props.onPan(false);
  }

  _onPanResponderRelease = () => {
    this.props.onPan && this.props.onPan(false);
    this.resetScale()
  }

  resetScale = () => {
    Animated.spring(this.scale, {
      toValue: 1,
      friction: 100
    }).start()
  }

  getSnapPoints = () => {
    const { height, minLevel, maxLevel, step } = this.props;
    let snapPoints = []
    for (var i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const point = { y: -i * height / (maxLevel - minLevel) }
      snapPoints.push(point)
    }

    return snapPoints
  }

  getAlertAreas = () => {
    const { height, minLevel, maxLevel, step } = this.props;
    let alertAreas = []
    for (var i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const alertPoint = { id: `alertPoint${i}`, influenceArea: { top: -i * height / (maxLevel - minLevel) }, type: i }
      alertAreas.push(alertPoint)
    }
    for (var i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const alertPoint = { id: `alertPoint${-i}`, influenceArea: { top: i * height / (maxLevel - minLevel) }, type: i }
      alertAreas.push(alertPoint)
    }

    return alertAreas
  }

  getBoundaries = () => {
    const { height, minLevel, maxLevel } = this.props;

    return { bottom: 0, top: -height }
  }

  getInitialPosition = () => {
    const { height, minLevel, maxLevel, initialValue } = this.props;

    return { y: -initialValue * height / (maxLevel - minLevel) }
  }

  snapToValue = (value) => {
    const { minLevel, step } = this.props;

    const newIndex = Math.floor((value - minLevel) / step);
    if (this._currentIndex !== newIndex) {
      this.refControl && this.refControl.current.snapTo({ index: newIndex })
    }
  }

  render() {
    const {
      height,
      width,
      backgroundControl,
      backgroundColor,
      borderRadius,
      onlyEventWhenStop,
      styleContainer,
      styleControl,
      rectangleBackground 
    } = this.props;

    return (
      <View style={styles.container}>
        <PanGestureHandler
          onHandlerStateChange={this._onHandlerStateChange}
        >
          <Animated.View
            style={[
              styles.viewContent,
              styleContainer,
              {
                height,
                width,
                backgroundColor,
                borderRadius
              },
              Platform.OS == 'ios' ? {
                transform: [
                  {
                    scale: this.scale
                  }
                ]
              } : undefined
            ]}
          >
            
            <Interactable.View
              ref={this.refControl}
              verticalOnly={true}
              animatedNativeDriver
              snapPoints={this.getSnapPoints()}
              boundaries={this.getBoundaries()}
              initialPosition={this.getInitialPosition()}
              onSnap={this._onSnap}
              onDrag={this._onDrag}
              onStop={this._onStop}
              alertAreas={onlyEventWhenStop ? undefined : this.getAlertAreas()}
              onAlert={onlyEventWhenStop ? undefined : this._onAlertEvent}
              dragEnabled={!this.props.isDisable}
            >
              <View style={{ width, height }} />
              
              <View pointerEvents='none' style={[styles.viewControl, styleControl, { width, height, backgroundColor: !this.props.isDisable ?  backgroundControl : backgroundColor , shadowColor: backgroundControl }]} >
              <View style={{height : 4,backgroundColor : rectangleBackground,width : 24,marginTop : 16,alignSelf : 'center',borderRadius : 2.5}}/>
              </View>
              
            </Interactable.View>
          </Animated.View>
        </PanGestureHandler>
      </View>
    );
  }
}

Swiper.defaultProps = {
  height: 315,
  width: 124,
  backgroundControl:  'white' ,
  backgroundColor: 'rgba(255, 255, 255, 0.4)',
  borderRadius: 24,
  onlyEventWhenStop: true,
  minLevel: 0,
  maxLevel: 100,
  step: 1,
  initialValue: 0,
  scaleWhenPress: true,
  scale: 1.1,
  rectangleBackground : "#9ea9b8",
  isDisable : false
}

Swiper.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number.isRequired,
  backgroundControl: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.number,
  onlyEventWhenStop: PropTypes.bool,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number,
  step: PropTypes.number,
  initialValue: PropTypes.number,
  onValueChange: PropTypes.func,
  onSwiperComplete: PropTypes.func,
  scaleWhenPress: PropTypes.bool,
  scale: PropTypes.number,
  styleContainer: ViewPropTypes.style,
  styleControl: ViewPropTypes.style,
  onPan: PropTypes.func,
  isDisable : PropTypes.bool
}

// interface Props {
//   /**
//    * default width = 124
//    */
//   width?: number;
//   /**
//    * default width = 315
//    */
//   height?: number;
//   backgroundControl?: string;
//   backgroundColor?: string;
//   /**
//    * default borderRadius = 38
//    */
//   borderRadius?: number;
//   /**
//    * Only callback event when stop touch. Default true
//    */
//   onlyEventWhenStop?: boolean;
//   /**
//    * default minLevel = 0
//    */
//   minLevel?: number;
//   /**
//    * default maxLevel = 100
//    */
//   maxLevel?: number;
//   /**
//    * default step = 1
//    */
//   step?: number;
//   /**
//    * default initialValue = 0
//    */
//   initialValue?: number;
//   /**
//    * call when change step
//    */
//   onValueChange?: (step: number) => void;
//   /**
//    * call when stop touch
//    */
//   onSwiperComplete?: (step: number) => void;
//   /**
//    * Enable animated scale when press. Default true
//    */
//   scaleWhenPress?: boolean;
//   /**
//    * default scale = 1.1
//    */
//   scale?: number;
//   styleContainer?: StyleProp<ViewStyle>;
//   styleControl?: StyleProp<ViewStyle>;
//   onPan?: (pan?: boolean) => void;
// }

export default Swiper;
