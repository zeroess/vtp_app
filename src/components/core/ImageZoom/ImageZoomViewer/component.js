import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  CameraRoll,
  Dimensions,
  I18nManager,
  Image,
  PanResponder,
  Platform,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  ViewPropTypes,
  ActivityIndicator,
  Modal,
  StatusBar,
  LayoutAnimation,
  Easing
} from 'react-native';
import ImageZoom from '../ImagePanZoom';
import styles, { simpleStyle } from './styles';
import FastImage from 'react-native-fast-image';

const WINDOW_HEIGHT = Dimensions.get('window').height;
const WINDOW_WIDTH = Dimensions.get('window').width;
const STATUS_BAR_OFFSET = (Platform.OS === 'android' ? -25 : 0);
const isIOS = Platform.OS === 'ios';

const CustomLayoutLinear = {
  duration: 200,
  create: {
    type: LayoutAnimation.Types.easeInEaseOut,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: LayoutAnimation.Types.easeInEaseOut,
    springDamping: 0.7,
  },
};

class ImageViewer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageSizes: [],
      imageLoaded: false,
      currentShowIndex: 0,
      show: false,
      target: {
        x: 0,
        y: 0,
        opacity: 1,
      },
      isPanning: false,
      pan: new Animated.Value(0),
      openVal: new Animated.Value(0),
      isAnimating: false,
      showDetail: true
    }

    this.refImageZoom = React.createRef()
    this.fadeAnim = new Animated.Value(0);

    this.standardPositionX = 0;
    this.positionXNumber = 0;
    this.positionX = new Animated.Value(0);

    this.width = 0;
    this.height = 0;

    this.styles = styles(0, 0, 'transparent');

    this.hasLayout = false;

    this.loadedIndex = [];

    this.handleLongPressWithIndex = this.handleLongPressWithIndex.bind(this);
  }

  componentWillMount() {
    this.init(this.props);
  }

  componentDidMount() {
    // Open modal property
    if (this.props.isOpen) {
      this.open();
    }
  }

  open = () => {
    if (isIOS) {
      StatusBar.setHidden(true, 'fade');
    }
    this.setState({
      isAnimating: true,
      target: {
        x: 0,
        y: 0,
        opacity: 1,
      }
    });

    Animated.spring(
      this.state.openVal,
      { toValue: 1, ...this.props.springConfig }
    ).start(() => {
      this.setState({ isAnimating: false });
    });
  }

  close = () => {
    this.props.willClose && this.props.willClose();
    this.setState({
      isAnimating: true,
    });
    if (isIOS) {
      StatusBar.setHidden(false, 'fade');
    }
    Animated.spring(
      this.state.openVal,
      {
        toValue: 0,
        easing: Easing.elastic,
        ...this.props.springConfig
      }
    ).start(() => {
      this.setState({ isAnimating: false }, () => {
        requestAnimationFrame(() => {
          this.props.onClose && this.props.onClose();
          this.props.onSwipeDown && this.props.onSwipeDown()
        })
      });
    });
  }

  handleLongPressWithIndex = (index) => {
    this.handleLongPress(this.props.imageUrls[index])
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.imageUrls != nextProps.imageUrls) {
      this.init(nextProps)
    }

    if (this.props.isOpen != nextProps.isOpen && nextProps.isOpen) {
      this.open();
    }

    if (nextProps.index !== this.state.currentShowIndex) {
      this.setState(
        {
          currentShowIndex: nextProps.index
        },
        () => {
          this.loadImage(nextProps.index || 0);

          this.jumpToCurrentImage();

          Animated.timing(this.fadeAnim, {
            toValue: 1,
            duration: 200
          }).start();
        }
      );
    }
  }

  init(nextProps) {
    if (nextProps.imageUrls.length === 0) {
      this.fadeAnim.setValue(0);
      return this.setState();
    }

    const imageSizes = [];
    nextProps.imageUrls.forEach(imageUrl => {
      imageSizes.push({
        width: imageUrl.width || 0,
        height: imageUrl.height || 0,
        status: 'loading'
      });
    });

    this.setState(
      {
        currentShowIndex: nextProps.index,
        imageSizes
      },
      () => {
        this.loadImage(nextProps.index || 0);

        this.jumpToCurrentImage();

        Animated.timing(this.fadeAnim, {
          toValue: 1,
          duration: 200
        }).start();
      }
    );
  }

  jumpToCurrentImage() {
    this.positionXNumber = -this.width * (this.state.currentShowIndex || 0);
    this.standardPositionX = this.positionXNumber;
    this.positionX.setValue(this.positionXNumber);
  }

  loadImage(index) {
    if (!this.state.imageSizes[index]) {
      return;
    }

    // if (this.loadedIndex.filter(item => item.name === index).length > 0) {
    //   return;
    // }
    this.loadedIndex.push({ name: index });

    const image = this.props.imageUrls[index];
    const imageStatus = { ...this.state.imageSizes[index] };

    // imageSize
    const saveImageSize = () => {
      if (this.state.imageSizes[index] && this.state.imageSizes[index].status !== 'loading') {
        return;
      }

      const imageSizes = this.state.imageSizes.slice();
      imageSizes[index] = imageStatus;
      this.setState({ imageSizes });
    };

    if (this.state.imageSizes[index].status === 'success') {
      return;
    }

    if (this.state.imageSizes[index].width > 0 && this.state.imageSizes[index].height > 0) {
      imageStatus.status = 'success';
      saveImageSize();
      return;
    }

    const sizeLoaded = false;
    let imageLoaded = false;

    // Tagged success if url is started with file:, or not set yet(for custom source.uri).
    if (!image.url || (image && image.url && image.url.startsWith(`file:`))) {
      imageLoaded = true;
    }

    // set width, height image with dimension
    imageStatus.width = Dimensions.get('window').width
    imageStatus.height = Dimensions.get('window').width * 9 / 16;
    imageStatus.status = 'success';
    saveImageSize();

    // Image.getSize(
    //   image.url,
    //   (width, height) => {
    //     console.log('image success')
    //     imageStatus.width = width;
    //     imageStatus.height = height;
    //     imageStatus.status = 'success';
    //     saveImageSize();
    //   },
    //   () => {
    //     try {
    //       const data = Image.resolveAssetSource(image.props.source);
    //       console.log('image success222')
    //       imageStatus.width = data.width;
    //       imageStatus.height = data.height;
    //       imageStatus.status = 'success';
    //       saveImageSize();
    //     } catch (newError) {
    //       // Give up..
    //       console.log('image fail')
    //       imageStatus.status = 'fail';
    //     }
    //   }
    // );
  }

  handleHorizontalOuterRangeOffset = (offsetX = 0) => {
    this.positionXNumber = this.standardPositionX + offsetX;
    this.positionX.setValue(this.positionXNumber);

    const offsetXRTL = !I18nManager.isRTL ? offsetX : -offsetX;

    if (offsetXRTL < 0) {
      if ((this.state.currentShowIndex || 0) < this.props.imageUrls.length - 1) {
        this.loadImage((this.state.currentShowIndex || 0) + 1);
      }
    } else if (offsetXRTL > 0) {
      if ((this.state.currentShowIndex || 0) > 0) {
        this.loadImage((this.state.currentShowIndex || 0) - 1);
      }
    }
  };

  handleResponderRelease = (vx = 0) => {
    const vxRTL = I18nManager.isRTL ? -vx : vx;
    const isLeftMove = I18nManager.isRTL
      ? this.positionXNumber - this.standardPositionX < -(this.props.flipThreshold || 0)
      : this.positionXNumber - this.standardPositionX > (this.props.flipThreshold || 0);
    const isRightMove = I18nManager.isRTL
      ? this.positionXNumber - this.standardPositionX > (this.props.flipThreshold || 0)
      : this.positionXNumber - this.standardPositionX < -(this.props.flipThreshold || 0);

    if (vxRTL > 0.7) {
      this.goBack.call(this);

      if (this.state.currentShowIndex || 0 > 0) {
        this.loadImage((this.state.currentShowIndex || 0) - 1);
      }
      return;
    } else if (vxRTL < -0.7) {
      this.goNext.call(this);
      if (this.state.currentShowIndex || 0 < this.props.imageUrls.length - 1) {
        this.loadImage((this.state.currentShowIndex || 0) + 1);
      }
      return;
    }

    if (isLeftMove) {
      this.goBack.call(this);
    } else if (isRightMove) {
      this.goNext.call(this);
      return;
    } else {
      this.resetPosition.call(this);
      return;
    }
  };

  goBack = () => {
    if (this.refImageZoom) {
      this.refImageZoom.current && this.refImageZoom.current.resetScale()
    }

    if (this.state.currentShowIndex === 0) {
      this.resetPosition.call(this);
      return;
    }

    this.positionXNumber = !I18nManager.isRTL
      ? this.standardPositionX + this.width
      : this.standardPositionX - this.width;
    this.standardPositionX = this.positionXNumber;
    Animated.timing(this.positionX, {
      toValue: this.positionXNumber,
      duration: 100
    }).start();

    const nextIndex = (this.state.currentShowIndex || 0) - 1;

    this.setState(
      {
        currentShowIndex: nextIndex
      },
      () => {
        if (this.props.onChange) {
          this.props.onChange(this.state.currentShowIndex);
        }
      }
    );
  };

  goNext() {
    if (this.refImageZoom) {
      this.refImageZoom.current && this.refImageZoom.current.resetScale()
    }

    if (this.state.currentShowIndex === this.props.imageUrls.length - 1) {
      this.resetPosition.call(this);
      return;
    }

    this.positionXNumber = !I18nManager.isRTL
      ? this.standardPositionX - this.width
      : this.standardPositionX + this.width;
    this.standardPositionX = this.positionXNumber;
    Animated.timing(this.positionX, {
      toValue: this.positionXNumber,
      duration: 100
    }).start();

    const nextIndex = (this.state.currentShowIndex || 0) + 1;

    this.setState(
      {
        currentShowIndex: nextIndex
      },
      () => {
        if (this.props.onChange) {
          this.props.onChange(this.state.currentShowIndex);
        }
      }
    );
  }

  resetPosition() {
    this.positionXNumber = this.standardPositionX;
    Animated.timing(this.positionX, {
      toValue: this.standardPositionX,
      duration: 150
    }).start();
  }

  handleLongPress = (image) => {
    if (this.props.onLongPress) {
      this.props.onLongPress(image);
    }
  };

  handleClick = () => {
    LayoutAnimation.configureNext(CustomLayoutLinear);
    this.setState({ showDetail: !this.state.showDetail });

    if (this.props.onClick) {
      this.props.onClick(this.handleCancel);
    }
  };

  handleDoubleClick = () => {
    if (this.props.onDoubleClick) {
      this.props.onDoubleClick(this.handleCancel);
    }
  };

  handleCancel = () => {
    this.hasLayout = false;
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  };

  handleLayout = (event) => {
    if (event.nativeEvent.layout.width !== this.width) {
      this.hasLayout = true;

      this.width = event.nativeEvent.layout.width;
      this.height = event.nativeEvent.layout.height;
      this.styles = styles(this.width, this.height, this.props.backgroundColor || 'transparent');

      this.forceUpdate();
      this.jumpToCurrentImage();
    }
  };

  renderCloseButton = () => {
    let opacityAnimate = {
      opacity: this[`refImageZoom${this.state.currentShowIndex}`] ? this[`refImageZoom${this.state.currentShowIndex}`].animatedPositionY.interpolate({
        inputRange: [-50, 0, 50],
        outputRange: [0, 1, 0]
      }) : 1
    }

    return (
      <Animated.View
        style={[
          { position: 'absolute', zIndex: 999, top: 0, right: 0 },
          opacityAnimate
        ]}
      >
        <TouchableOpacity style={{ paddingLeft: 12, paddingBottom: 12 }} onPress={this.close}>
          <Text style={[this.styles.closeButton, isIOS ? {} : { marginTop: 12 }]}>×</Text>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  getContent() {
    const screenWidth = this.width;
    const screenHeight = this.height;

    const ImageElements = this.props.imageUrls.map((image, index) => {
      if ((this.state.currentShowIndex || 0) > index + 1 || (this.state.currentShowIndex || 0) < index - 1) {
        return <View key={index} style={{ width: screenWidth, height: screenHeight }} />;
      }

      let width = this.state.imageSizes[index] && this.state.imageSizes[index].width;
      let height = this.state.imageSizes[index] && this.state.imageSizes[index].height;
      const imageInfo = this.state.imageSizes[index];

      if (!imageInfo || !imageInfo.status) {
        return <View key={index} style={{ width: screenWidth, height: screenHeight }} />;
      }

      if (width > screenWidth) {
        const widthPixel = screenWidth / width;
        width *= widthPixel;
        height *= widthPixel;
      }

      if (height > screenHeight) {
        const HeightPixel = screenHeight / height;
        width *= HeightPixel;
        height *= HeightPixel;
      }

      const Wrapper = ({ children, ...others }) => (
        <ImageZoom
          cropWidth={this.width}
          cropHeight={this.height}
          maxOverflow={this.props.maxOverflow}
          horizontalOuterRangeOffset={this.handleHorizontalOuterRangeOffset}
          responderRelease={this.handleResponderRelease}
          onLongPress={() => this.handleLongPressWithIndex(index)}
          onClick={this.handleClick}
          onDoubleClick={this.handleDoubleClick}
          enableSwipeDown={true}
          onSwipeDown={this.handleSwipeDown}
          {...others}
        >
          {children}
        </ImageZoom>
      );
      switch (imageInfo.status) {
        case 'loading':
          return this.renderLoading(index, image.url)
        case 'success':
          if (!image.props) {
            image.props = {};
          }
          if (!image.props.style) {
            image.props.style = {};
          }
          image.props.style = {
            ...this.styles.imageStyle, // User config can override above.
            ...image.props.style,
            width,
            height
          };

          if (typeof image.props.source === 'number') {
            // source = require(..), doing nothing
          } else {
            if (!image.props.source) {
              image.props.source = {};
            }
            image.props.source = {
              uri: image.url,
              ...image.props.source
            };
          }

          return (
            <ImageZoom
              key={index}
              ref={refs => this[`refImageZoom${index}`] = refs}
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height}
              maxOverflow={this.props.maxOverflow}
              horizontalOuterRangeOffset={this.handleHorizontalOuterRangeOffset}
              responderRelease={this.handleResponderRelease}
              onLongPress={() => this.handleLongPressWithIndex(index)}
              onClick={this.handleClick}
              onDoubleClick={this.handleDoubleClick}
              imageWidth={Dimensions.get('window').width}
              imageHeight={Dimensions.get('window').height}
              enableSwipeDown={true}
              enableCenterFocus
              enableHorizontalBounce
              onSwipeDown={this.handleSwipeDown}
              onSwipeUp={this.handleSwipeUp}
            // {...this.props}
            >
              {this._renderImage(image, index)}
            </ImageZoom>
          );
        case 'fail':
          return (
            <Wrapper
              key={index}
              style={this.styles.modalContainer}
              imageWidth={this.props.failImageSource ? this.props.failImageSource.width : screenWidth}
              imageHeight={this.props.failImageSource ? this.props.failImageSource.height : screenHeight}
            >
              {this.props.failImageSource &&
                this.props.renderImage({
                  source: {
                    uri: this.props.failImageSource.url
                  },
                  style: {
                    width: this.props.failImageSource.width,
                    height: this.props.failImageSource.height
                  }
                })}
            </Wrapper>
          );
      }
    });

    let opacityAnimate = {
      opacity: this[`refImageZoom${this.state.currentShowIndex}`] ? this[`refImageZoom${this.state.currentShowIndex}`].animatedPositionY.interpolate({
        inputRange: [-50, 0, 50],
        outputRange: [0, 1, 0]
      }) : 1
    }

    const animateOverlay = {
      backgroundColor: this[`refImageZoom${this.state.currentShowIndex}`] ? this[`refImageZoom${this.state.currentShowIndex}`].animatedPositionY.interpolate({
        inputRange: [-200, -100, 0, 100, 200],
        outputRange: ['rgba(0, 0, 0, 0.6)', 'rgba(0, 0, 0, 0.6)', 'black', 'rgba(0, 0, 0, 0.6)', 'rgba(0, 0, 0, 0.6)']
      }) : 'black'
    }

    return (
      <Animated.View style={[{ zIndex: 9999 }, animateOverlay]}>
        <Animated.View style={[this.styles.container, { opacity: this.fadeAnim, backgroundColor: 'transparent' }]}>
          {this.state.showDetail && this.props.renderHeader(this.state.currentShowIndex)}
          {this.state.showDetail && this.props.closeButton && this.renderCloseButton()}

          {
            this.state.showDetail && (
              <View style={this.styles.arrowLeftContainer}>
                <TouchableWithoutFeedback onPress={this.goBack}>
                  <View>{this.props.renderArrowLeft()}</View>
                </TouchableWithoutFeedback>
              </View>
            )
          }

          {
            this.state.showDetail && (
              <View style={this.styles.arrowRightContainer}>
                <TouchableWithoutFeedback onPress={this.goNext}>
                  <View>{this.props.renderArrowRight()}</View>
                </TouchableWithoutFeedback>
              </View>
            )
          }

          <Animated.View
            style={[
              this.styles.moveBox,
              {
                transform: [{ translateX: this.positionX }],
                width: this.width * this.props.imageUrls.length,
              }
            ]}
          >
            {ImageElements}
          </Animated.View>
          {
            this.state.showDetail && this.props.imageUrls.length > 1 && (
              <Animated.View
                style={[
                  { position: 'absolute', zIndex: 998, top: 0, right: 0, left: 0 },
                  opacityAnimate
                ]}
              >
                {
                  this.props.renderIndicator ? this.props.renderIndicator((this.state.currentShowIndex || 0) + 1, this.props.imageUrls.length) : (
                    <View style={[simpleStyle.count]}>
                      <Text style={[simpleStyle.countText]}>
                        {`${(this.state.currentShowIndex || 0) + 1}/${this.props.imageUrls.length}`}
                      </Text>
                    </View>
                  )
                }
              </Animated.View>
            )
          }

          {this.state.showDetail && this.props.imageUrls[this.state.currentShowIndex || 0] &&
            this.props.imageUrls[this.state.currentShowIndex || 0].originSizeKb &&
            this.props.imageUrls[this.state.currentShowIndex || 0].originUrl && (
              <View style={this.styles.watchOrigin}>
                <TouchableOpacity style={this.styles.watchOriginTouchable}>
                  <Text style={this.styles.watchOriginText}>Image greater than (2M)</Text>
                </TouchableOpacity>
              </View>
            )}
          <Animated.View
            style={[
              { bottom: 0, position: 'absolute', zIndex: 9999 },
              this.props.footerContainerStyle,
              opacityAnimate
            ]}
          >
            {this.state.showDetail && !this.state.isAnimating && this.props.renderFooter(this.state.currentShowIndex, this.props.imageUrls[this.state.currentShowIndex || 0])}
          </Animated.View>
        </Animated.View>
      </Animated.View>
    );
  }

  saveToLocal = () => {
    if (!this.props.onSave) {
      CameraRoll.saveToCameraRoll(this.props.imageUrls[this.state.currentShowIndex || 0].url);
      this.props.onSaveToCamera(this.state.currentShowIndex);
    } else {
      this.props.onSave(this.props.imageUrls[this.state.currentShowIndex || 0].url);
    }
  };

  handleLeaveMenu = () => {

  };

  handleSwipeDown = () => {
    this.close();
    this.handleCancel();
  };

  handleSwipeUp = () => {
    this.close();
    this.handleCancel();
  }

  _renderImage = (image, index) => {
    if (this.state.isAnimating) {
      return <View style={{ flex: 1, backgroundColor: 'black' }} />
    }

    if (this.props.renderImage) {
      this.props.renderImage(image.props, index)
    }

    return (
      <FastImage
        autoResize={false}
        source={{ uri: image.url }}
        style={{ flex: 1 }}
        resizeMode='contain'
      />
    )
  }

  renderLoading = (index, uri) => {
    if (this.props.loadingRender) {
      return this.props.loadingRender()
    }

    return (
      <View key={index} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' color={this.props.colorIndicator} animating={true} />
      </View>
    )
  }

  render() {
    const {
      isOpen,
      origin,
      imageUrls
    } = this.props;

    const {
      openVal,
      target
    } = this.state;
    let childs = React.ReactElement = null;

    childs = (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        {this.getContent()}
      </View>
    );

    const opacityLayoutAnimate = {
      opacity: openVal.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
      })
    }

    const openStyle = [{
      left: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.x, target.x] }),
      top: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.y + STATUS_BAR_OFFSET, target.y + STATUS_BAR_OFFSET] }),
      width: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.width, WINDOW_WIDTH] }),
      height: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.height, WINDOW_HEIGHT] }),
    }];

    return (
      <Modal visible={isOpen} transparent={true} onRequestClose={() => this.close()}>
        <Animated.View
          onLayout={this.handleLayout}
          style={[
            { flex: 1, position: 'absolute', overflow: 'hidden', ...this.props.style },
            openStyle,
            opacityLayoutAnimate
          ]}
        >
          {childs}
        </Animated.View>
      </Modal>
    );
  }
}

ImageViewer.defaultProps = {
  /**
   * Enable show/hidden image
   */
  show: false,

  /**
   * Array of url image
   */
  imageUrls: [],

  /**
   * Flip threshold
   */
  flipThreshold: 40,

  /**
   * Max over flow
   */
  maxOverflow: 300,

  /**
   * Index of image which is showing
   */
  index: 0,

  /**
   * When uri image fail
   */
  failImageSource: undefined,

  /**
   * Background color container
   */
  backgroundColor: 'black',

  /**
   * style props for the footer container
   */
  footerContainerStyle: {},

  /**
   * Menu Context Values
   */
  menuContext: {
    saveToLocal: 'Save to the album',
    cancel: 'Cancel'
  },

  /**
   * Enable save to local
   */
  saveToLocalByLongPress: false,

  style: {},

  /**
   * Call when on long press to image
   */
  onLongPress: () => { },

  /**
   * Call when press to image
   */
  onClick: () => { },

  /**
   * Call when double press image
   */
  onDoubleClick: () => { },

  /**
   * Call when save image to local
   */
  onSave: undefined,

  /**
   * Render header
   */
  renderHeader: () => { },

  /**
   * render footer
   */
  renderFooter: () => { },

  /**
   * Render indicator
   */
  renderIndicator: undefined,

  /**
   * Render image component
   */
  renderImage: (props) => {
    return React.createElement(Image, props)
  },

  /**
   * Call when press left arrow button
   */
  renderArrowLeft: () => { },

  /**
   * Call when press right arrow button
   */
  renderArrowRight: () => { },

  /**
   * Call when show modal
   */
  onShowModal: () => { },

  /**
   * Call when cancel button press
   */
  onCancel: () => { },

  /**
   * function that fires when user swipes down
   */
  onSwipeDown: () => { },

  /**
   * func render loading
   */
  loadingRender: undefined,

  /**
   * On save image
   */
  onSaveToCamera: () => { },

  /**
   * Onchange
   */
  onChange: () => { },
  colorIndicator: '#ccc',
  springConfig: { tension: 30, friction: 7 },
  origin: {
    x: WINDOW_WIDTH / 2,
    y: WINDOW_HEIGHT / 2,
    width: 0,
    height: 0,
  },
  closeButton: true
}

ImageViewer.propTypes = {
  /**
   * Enable show/hidden image
   */
  show: PropTypes.bool,

  /**
   * Array of url image
   */
  imageUrls: PropTypes.array,

  /**
   * Flip threshold
   */
  flipThreshold: PropTypes.number,

  /**
   * Max over flow
   */
  maxOverflow: PropTypes.number,

  /**
   * Index of image which is showing
   */
  index: PropTypes.number,

  /**
   * When uri image fail
   */
  failImageSource: PropTypes.shape({
    url: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    sizeKb: PropTypes.number,
    originSizeKb: PropTypes.number,
    originUrl: PropTypes.number,
    props: PropTypes.any,
    freeHeight: PropTypes.bool,
    freeWidth: PropTypes.bool,
  }),

  /**
   * Background color container
   */
  backgroundColor: PropTypes.string,

  /**
   * style props for the footer container
   */
  footerContainerStyle: ViewPropTypes.style,

  /**
   * Menu Context Values
   */
  menuContext: PropTypes.shape({
    saveToLocal: PropTypes.string,
    cancel: PropTypes.string
  }),

  /**
   * Enable save to local
   */
  saveToLocalByLongPress: PropTypes.bool,

  style: ViewPropTypes.style,

  /**
   * Call when on long press to image
   */
  onLongPress: PropTypes.func,

  /**
   * Call when press to image
   */
  onClick: PropTypes.func,

  /**
   * Call when double press image
   */
  onDoubleClick: PropTypes.func,

  /**
   * Call when save image to local
   */
  onSave: PropTypes.func,

  /**
   * Render header
   */
  renderHeader: PropTypes.func,

  /**
   * render footer
   */
  renderFooter: PropTypes.func,

  /**
   * Render indicator
   */
  renderIndicator: PropTypes.func,

  /**
   * Render image component
   */
  renderImage: PropTypes.func,

  /**
   * Call when press left arrow button
   */
  renderArrowLeft: PropTypes.func,

  /**
   * Call when press right arrow button
   */
  renderArrowRight: PropTypes.func,

  /**
   * Call when show modal
   */
  onShowModal: PropTypes.func,

  /**
   * Call when cancel button press
   */
  onCancel: PropTypes.func,

  /**
   * function that fires when user swipes down
   */
  onSwipeDown: PropTypes.func,

  /**
   * func render loading
   */
  loadingRender: PropTypes.func,

  /**
   * On save image
   */
  onSaveToCamera: PropTypes.func,

  /**
   * Onchange
   */
  onChange: PropTypes.func,
  colorIndicator: PropTypes.string,
  origin: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
  }),
  springConfig: PropTypes.shape({
    tension: PropTypes.number,
    friction: PropTypes.number,
  }),
  closeButton: PropTypes.bool
}

export default ImageViewer;
