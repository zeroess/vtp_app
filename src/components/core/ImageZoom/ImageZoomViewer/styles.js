import { TextStyle, Platform, Dimensions } from 'react-native';

const isIphoneX = () => {
  const dimen = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 || dimen.width === 812)
  );
}

export default (
  width,
  height,
  backgroundColor
) => {
  return {
    modalContainer: {
      backgroundColor,
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'hidden'
    },
    watchOrigin: {
      position: 'absolute',
      width,
      bottom: 20,
      justifyContent: 'center',
      alignItems: 'center'
    },
    watchOriginTouchable: {
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 5,
      paddingBottom: 5,
      borderRadius: 30,
      borderColor: 'white',
      borderWidth: 0.5,
      backgroundColor: 'rgba(0, 0, 0, 0.1)'
    },
    watchOriginText: {
      color: 'white',
      backgroundColor: 'transparent'
    },
    imageStyle: {},
    container: {
      backgroundColor
    },
    moveBox: {
      flexDirection: 'row',
      alignItems: 'center'
    },
    menuContainer: {
      position: 'absolute',
      width,
      height,
      left: 0,
      bottom: 0,
      top: 0,
      right: 0
    },
    menuShadow: {
      position: 'absolute',
      width,
      height,
      backgroundColor: 'black',
      left: 0,
      bottom: 0,
      opacity: 0.2,
      zIndex: 10
    },
    menuContent: {
      position: 'absolute',
      width,
      left: 0,
      bottom: 0,
      zIndex: 11
    },
    operateContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
      height: 40,
      borderBottomColor: '#ccc',
      borderBottomWidth: 1
    },
    operateText: {
      color: '#333'
    },
    loadingTouchable: {
      width,
      height
    },
    loadingContainer: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    arrowLeftContainer: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      justifyContent: 'center'
    },
    arrowRightContainer: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      justifyContent: 'center'
    },
    closeButton: {
      fontSize: 35,
      color: 'white',
      lineHeight: 40,
      width: 40,
      textAlign: 'center',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowRadius: 1.5,
      shadowColor: 'black',
      shadowOpacity: 0.8,
      top: isIphoneX() ? 20 : 12,
    }
  };
};

export const simpleStyle = {
  count: Platform.select({
    ios: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: isIphoneX() ? 36 : 24,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center'
    },
    android: {
      width: 120,
      height: 60,
      left: 0,
      right: 0,
      top: 12,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center'
    }
  }),
  countText: {
    color: 'white',
    fontSize: 16,
    backgroundColor: 'transparent',
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowOffset: {
      width: 0,
      height: 0.5
    },
    textShadowRadius: 0
  }
};
