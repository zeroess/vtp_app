import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  LayoutChangeEvent,
  PanResponder,
  PanResponderInstance,
  Platform,
  PlatformOSType,
  View,
  ViewPropTypes
} from 'react-native';
import styles from './styles';

class ImageViewer extends PureComponent {
  constructor(props) {
    super(props);

    this.lastPositionX = null
    this.positionX = 0;
    this.animatedPositionX = new Animated.Value(0);
    this.lastPositionY = null
    this.positionY = 0;
    this.animatedPositionY = new Animated.Value(0);
    this.scale = 1;
    this.animatedScale = new Animated.Value(1);
    this.zoomLastDistance = null;
    this.zoomCurrentDistance = 0;
    this.imagePanResponder = null;
    this.lastTouchStartTime = 0;
    this.horizontalWholeOuterCounter = 0;
    this.swipeDownOffset = 0;
    this.horizontalWholeCounter = 0;
    this.verticalWholeCounter = 0;
    this.centerDiffX = 0;
    this.centerDiffY = 0;
    this.singleClickTimeout = null;
    this.longPressTimeout = null;
    this.lastClickTime = 0;
    this.doubleClickX = 0;
    this.doubleClickY = 0;
    this.isDoubleClick = false;
    this.isLongPress = false;
    this.isHorizontalWrap = false;
  }

  componentWillMount() {
    this.imagePanResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderTerminationRequest: () => false,
      onPanResponderGrant: evt => {
        this.lastPositionX = null;
        this.lastPositionY = null;
        this.zoomLastDistance = null;
        this.horizontalWholeCounter = 0;
        this.verticalWholeCounter = 0;
        this.lastTouchStartTime = new Date().getTime();
        this.isDoubleClick = false;
        this.isLongPress = false;
        this.isHorizontalWrap = false;

        if (this.singleClickTimeout) {
          clearTimeout(this.singleClickTimeout);
        }

        if (evt.nativeEvent.changedTouches.length > 1) {
          const centerX = (evt.nativeEvent.changedTouches[0].pageX + evt.nativeEvent.changedTouches[1].pageX) / 2;
          this.centerDiffX = centerX - this.props.cropWidth / 2;

          const centerY = (evt.nativeEvent.changedTouches[0].pageY + evt.nativeEvent.changedTouches[1].pageY) / 2;
          this.centerDiffY = centerY - this.props.cropHeight / 2;
        }

        if (this.longPressTimeout) {
          clearTimeout(this.longPressTimeout);
        }
        this.longPressTimeout = setTimeout(() => {
          this.isLongPress = true;
          if (this.props.onLongPress) {
            this.props.onLongPress();
          }
        }, this.props.longPressTime);

        if (evt.nativeEvent.changedTouches.length <= 1) {
          if (new Date().getTime() - this.lastClickTime < (this.props.doubleClickInterval || 0)) {
            this.lastClickTime = 0;
            if (this.props.onDoubleClick) {
              this.props.onDoubleClick();
            }

            clearTimeout(this.longPressTimeout);

            this.doubleClickX = evt.nativeEvent.changedTouches[0].pageX;
            this.doubleClickY = evt.nativeEvent.changedTouches[0].pageY;

            this.isDoubleClick = true;
            if (this.scale > 1 || this.scale < 1) {
              this.scale = 1;
              this.positionX = 0;
              this.positionY = 0;
            } else {
              const beforeScale = this.scale;

              this.scale = 2;

              const diffScale = this.scale - beforeScale;
              this.positionX = (this.props.cropWidth / 2 - this.doubleClickX) * diffScale / this.scale;

              // I remove it becauce positionY will set backgroundColor to transparent
              // this.positionY = (this.props.cropHeight / 2 - this.doubleClickY) * diffScale / this.scale;
              this.positionY = 0;
            }

            this.imageDidMove('centerOn');

            Animated.parallel([
              Animated.timing(this.animatedScale, {
                toValue: this.scale,
                duration: 100
              }),
              Animated.timing(this.animatedPositionX, {
                toValue: this.positionX,
                duration: 100
              }),
              Animated.spring(this.animatedPositionY, {
                toValue: this.positionY,
                tension: 30, friction: 7
              })
            ]).start();
          } else {
            this.lastClickTime = new Date().getTime();
          }
        }
      },
      onPanResponderMove: (evt, gestureState) => {
        if (this.isDoubleClick) {
          return;
        }

        if (evt.nativeEvent.changedTouches.length <= 1) {
          let diffX = gestureState.dx - (this.lastPositionX || 0);
          if (this.lastPositionX === null) {
            diffX = 0;
          }

          let diffY = gestureState.dy - (this.lastPositionY || 0);
          if (this.lastPositionY === null) {
            diffY = 0;
          }

          this.lastPositionX = gestureState.dx;
          this.lastPositionY = gestureState.dy;

          this.horizontalWholeCounter += diffX;
          this.verticalWholeCounter += diffY;

          if (Math.abs(this.horizontalWholeCounter) > 5 || Math.abs(this.verticalWholeCounter) > 5) {
            clearTimeout(this.longPressTimeout);
          }

          if (this.props.panToMove) {
            if (this.swipeDownOffset === 0) {
              if (Math.abs(diffX) > Math.abs(diffY)) {
                this.isHorizontalWrap = true;
              }

              if (this.props.imageWidth * this.scale > this.props.cropWidth) {
                if (this.props.enableHorizontalBounce) {
                  this.positionX += diffX / this.scale;
                  this.animatedPositionX.setValue(this.positionX);
                } else {
                  if (this.horizontalWholeOuterCounter > 0) {
                    if (diffX < 0) {
                      if (this.horizontalWholeOuterCounter > Math.abs(diffX)) {
                        this.horizontalWholeOuterCounter += diffX;
                        diffX = 0;
                      } else {
                        diffX += this.horizontalWholeOuterCounter;
                        this.horizontalWholeOuterCounter = 0;
                        if (this.props.horizontalOuterRangeOffset) {
                          this.props.horizontalOuterRangeOffset(0);
                        }
                      }
                    } else {
                      this.horizontalWholeOuterCounter += diffX;
                    }
                  } else if (this.horizontalWholeOuterCounter < 0) {
                    if (diffX > 0) {
                      if (Math.abs(this.horizontalWholeOuterCounter) > diffX) {
                        this.horizontalWholeOuterCounter += diffX;
                        diffX = 0;
                      } else {
                        diffX += this.horizontalWholeOuterCounter;
                        this.horizontalWholeOuterCounter = 0;
                        if (this.props.horizontalOuterRangeOffset) {
                          this.props.horizontalOuterRangeOffset(0);
                        }
                      }
                    } else {
                      this.horizontalWholeOuterCounter += diffX;
                    }
                  } else {

                  }

                  this.positionX += diffX / this.scale;

                  const horizontalMax = (this.props.imageWidth * this.scale - this.props.cropWidth) / 2 / this.scale;
                  if (this.positionX < -horizontalMax) {
                    this.positionX = -horizontalMax;

                    this.horizontalWholeOuterCounter += -1 / 1e10;
                  } else if (this.positionX > horizontalMax) {
                    this.positionX = horizontalMax;

                    this.horizontalWholeOuterCounter += 1 / 1e10;
                  }
                  this.animatedPositionX.setValue(this.positionX);
                }
              } else {
                this.horizontalWholeOuterCounter += diffX;
              }

              if (this.props.enableHorizontalBounce) {
                if (this.horizontalWholeOuterCounter > (this.props.maxOverflow || 0)) {
                  this.horizontalWholeOuterCounter = this.props.maxOverflow || 0;
                } else if (this.horizontalWholeOuterCounter < -(this.props.maxOverflow || 0)) {
                  this.horizontalWholeOuterCounter = -(this.props.maxOverflow || 0);
                }

                if (this.horizontalWholeOuterCounter !== 0) {
                  if (this.props.horizontalOuterRangeOffset) {
                    this.props.horizontalOuterRangeOffset(this.horizontalWholeOuterCounter);
                  }
                }
              }
            }

            if ((this.props.imageHeight * this.scale > this.props.cropHeight) && this.props.enableOnlySetPositionY) {
              this.positionY += diffY / this.scale;
              this.animatedPositionY.setValue(this.positionY);
            } else {
              if (this.props.enableSwipeDown && !this.isHorizontalWrap) {
                this.swipeDownOffset += diffY;

                if (this.swipeDownOffset > 0) {
                  this.positionY += diffY / this.scale;
                  this.animatedPositionY.setValue(this.positionY);

                  this.scale = this.scale - diffY / 1000;
                  this.animatedScale.setValue(this.scale);
                } else {
                  if (this.props.enableSwipeUp) {
                    this.positionY += diffY / this.scale;
                    this.animatedPositionY.setValue(this.positionY);
  
                    this.scale = this.scale + diffY / 1000;
                    this.animatedScale.setValue(this.scale);
                  }
                }
              }
            }
          }
        } else {
          if (this.longPressTimeout) {
            clearTimeout(this.longPressTimeout);
          }

          if (this.props.pinchToZoom) {
            let minX;
            let maxX;
            if (evt.nativeEvent.changedTouches[0].locationX > evt.nativeEvent.changedTouches[1].locationX) {
              minX = evt.nativeEvent.changedTouches[1].pageX;
              maxX = evt.nativeEvent.changedTouches[0].pageX;
            } else {
              minX = evt.nativeEvent.changedTouches[0].pageX;
              maxX = evt.nativeEvent.changedTouches[1].pageX;
            }

            let minY;
            let maxY;
            if (evt.nativeEvent.changedTouches[0].locationY > evt.nativeEvent.changedTouches[1].locationY) {
              minY = evt.nativeEvent.changedTouches[1].pageY;
              maxY = evt.nativeEvent.changedTouches[0].pageY;
            } else {
              minY = evt.nativeEvent.changedTouches[0].pageY;
              maxY = evt.nativeEvent.changedTouches[1].pageY;
            }

            const widthDistance = maxX - minX;
            const heightDistance = maxY - minY;
            const diagonalDistance = Math.sqrt(widthDistance * widthDistance + heightDistance * heightDistance);
            this.zoomCurrentDistance = Number(diagonalDistance.toFixed(1));

            if (this.zoomLastDistance !== null) {
              const distanceDiff = (this.zoomCurrentDistance - this.zoomLastDistance) / 200;
              let zoom = this.scale + distanceDiff;

              if (zoom < this.props.minScale) {
                zoom = this.props.minScale;
              }
              if (zoom > this.props.maxScale) {
                zoom = this.props.maxScale;
              }

              const beforeScale = this.scale;

              this.scale = zoom;
              this.animatedScale.setValue(this.scale);

              const diffScale = this.scale - beforeScale;
              this.positionX -= this.centerDiffX * diffScale / this.scale;
              this.positionY -= this.centerDiffY * diffScale / this.scale;
              this.animatedPositionX.setValue(this.positionX);
              this.animatedPositionY.setValue(this.positionY);
            }
            this.zoomLastDistance = this.zoomCurrentDistance;
          }
        }

        this.imageDidMove('onPanResponderMove');
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (this.longPressTimeout) {
          clearTimeout(this.longPressTimeout);
        }

        if (this.isDoubleClick) {
          return;
        }

        if (this.isLongPress) {
          return;
        }

        // const stayTime = new Date().getTime() - this.lastTouchStartTime!
        const moveDistance = Math.sqrt(gestureState.dx * gestureState.dx + gestureState.dy * gestureState.dy);
        if (evt.nativeEvent.changedTouches.length === 1 && moveDistance < (this.props.clickDistance || 0)) {
          this.singleClickTimeout = setTimeout(() => {
            if (this.props.onClick) {
              this.props.onClick();
            }
          }, this.props.doubleClickInterval);
        } else {
          if (this.props.responderRelease) {
            this.props.responderRelease(gestureState.vx, this.scale);
          }

          this.panResponderReleaseResolve();
        }
      },
      onPanResponderTerminate: () => {

      }
    });
  }

  resetScale = () => {
    this.positionX = 0;
    this.positionY = 0;
    this.scale = 1;
    this.animatedScale.setValue(1);
  };

  panResponderReleaseResolve = () => {
    if (this.props.enableSwipeDown && this.props.swipeDownThreshold) {
      if (this.swipeDownOffset > this.props.swipeDownThreshold) {
        if (this.props.onSwipeDown) {
          this.props.onSwipeDown();
        }
        // Stop reset.
        return;
      }
    }

    // I use common swipeDownOffset to set swipeUpOffset
    if (this.props.enableSwipeUp && this.props.swipeUpThreshold) {
      if (Math.abs(this.swipeDownOffset) > this.props.swipeUpThreshold) {
        if (this.props.onSwipeUp) {
          this.props.onSwipeUp();
        }
        // Stop reset.
        return;
      }
    }

    if (this.props.enableCenterFocus && this.scale < 1) {
      this.scale = 1;
      Animated.spring(this.animatedScale, {
        toValue: this.scale,
        tension: 30, friction: 7
      }).start();
    }

    if (this.props.imageWidth * this.scale <= this.props.cropWidth) {
      this.positionX = 0;
      Animated.spring(this.animatedPositionX, {
        toValue: this.positionX,
        tension: 30, friction: 7
      }).start();
    }

    if (this.props.imageHeight * this.scale <= this.props.cropHeight) {
      this.positionY = 0;
      Animated.spring(this.animatedPositionY, {
        toValue: this.positionY,
        tension: 30, friction: 7
      }).start();
    }

    if (this.props.imageHeight * this.scale > this.props.cropHeight) {
      // alaway reset positionY to 0
      // const verticalMax = (this.props.imageHeight * this.scale - this.props.cropHeight) / 2 / this.scale;
      // if (this.positionY < -verticalMax) {
      //   this.positionY = -verticalMax;
      // } else if (this.positionY > verticalMax) {
      //   this.positionY = verticalMax;
      // }
      this.positionY = 0;
      Animated.timing(this.animatedPositionY, {
        toValue: this.positionY,
        duration: 100
      }).start();
    }

    if (this.props.enableHorizontalBounce && this.props.imageWidth * this.scale > this.props.cropWidth) {
      const horizontalMax = (this.props.imageWidth * this.scale - this.props.cropWidth) / 2 / this.scale;
      if (this.positionX < -horizontalMax) {
        this.positionX = -horizontalMax;
      } else if (this.positionX > horizontalMax) {
        this.positionX = horizontalMax;
      }
      Animated.timing(this.animatedPositionX, {
        toValue: this.positionX,
        duration: 100
      }).start();
    }

    if (this.props.enableCenterFocus && this.scale === 1) {
      this.positionX = 0;
      this.positionY = 0;
      Animated.timing(this.animatedPositionX, {
        toValue: this.positionX,
        duration: 100
      }).start();
      Animated.timing(this.animatedPositionY, {
        toValue: this.positionY,
        duration: 100
      }).start();
    }

    this.horizontalWholeOuterCounter = 0;

    this.swipeDownOffset = 0;

    this.imageDidMove('onPanResponderRelease');
  };

  componentDidMount() {
    if (this.props.centerOn) {
      this.centerOn(this.props.centerOn);
    }
  }

  componentWillUnmount() {
    if (this.longPressTimeout) {
      clearTimeout(this.longPressTimeout);
    }

    if (this.singleClickTimeout) {
      clearTimeout(this.singleClickTimeout);
    }
  }

  componentWillReceiveProps(nextProps) {
    // Either centerOn has never been called, or it is a repeat and we should ignore it
    if (
      (nextProps.centerOn && !this.props.centerOn) ||
      (nextProps.centerOn && this.props.centerOn && this.didCenterOnChange(this.props.centerOn, nextProps.centerOn))
    ) {
      this.centerOn(nextProps.centerOn);
    }
  }

  imageDidMove(type) {
    if (this.props.onMove) {
      this.props.onMove({
        type,
        positionX: this.positionX,
        positionY: this.positionY,
        scale: this.scale,
        zoomCurrentDistance: this.zoomCurrentDistance
      });
    }
  }

  didCenterOnChange(params, paramsNext) {
    return params.x !== paramsNext.x || params.y !== paramsNext.y || params.scale !== paramsNext.scale;
  }

  centerOn(params) {
    this.positionX = params.x || null;
    this.positionY = params.y || null;
    this.scale = params.scale || null;
    const duration = params.duration || 300;
    Animated.parallel([
      Animated.timing(this.animatedScale, {
        toValue: this.scale,
        duration
      }),
      Animated.timing(this.animatedPositionX, {
        toValue: this.positionX,
        duration
      }),
      Animated.spring(this.animatedPositionY, {
        toValue: this.positionY,
        tension: 30, friction: 7
      })
    ]).start(() => {
      this.imageDidMove('centerOn');
    });
  }

  handleLayout(event) {
    if (this.props.layoutChange) {
      this.props.layoutChange(event);
    }
  }

  reset() {
    this.scale = 1;
    this.animatedScale.setValue(this.scale);
    this.positionX = 0;
    this.animatedPositionX.setValue(this.positionX);
    this.positionY = 0;
    this.animatedPositionY.setValue(this.positionY);
  }

  render() {
    const animateConf = {
      transform: [
        {
          scale: this.animatedScale
        },
        {
          translateX: this.animatedPositionX
        },
        {
          translateY: this.animatedPositionY
        }
      ]
    };

    return (
      <View
        style={[
          styles.container,
          this.props.style,
          {
            width: this.props.cropWidth,
            height: this.props.cropHeight,
          }
        ]}
        {...this.imagePanResponder.panHandlers}
      >
        <Animated.View style={animateConf}>
          <View
            onLayout={this.handleLayout.bind(this)}
            style={{
              width: this.props.imageWidth,
              height: this.props.imageHeight
            }}
          >
            {this.props.children}
          </View>
        </Animated.View>
      </View>
    );
  }
}

ImageViewer.defaultProps = {
  /**
  * Crop width
  */
  cropWidth: 100,
  /**
   * Crop height
   */
  cropHeight: 100,
  /**
   * Width image
   */
  imageWidth: 100,
  /**
   * Height image
   */
  imageHeight: 100,
  /**
   * Enable pan to move
   */
  panToMove: true,
  /**
   * Enable onch to zoom
   */
  pinchToZoom: true,
  /**
   * Click distance
   */
  clickDistance: 10,
  /**
   * Max over flow
   */
  maxOverflow: 100,
  /**
   * Time long press
   */
  longPressTime: Platform.OS === 'android' ? 20000 : 3000,
  /**
   * time double click
   */
  doubleClickInterval: 300,
  style: {},
  /**
   * Swipe down threshold
   */
  swipeDownThreshold: 80,
  /**
   * for enable vertical movement if user doens't want it
   */
  enableSwipeDown: false,
  /**
   * Call when press
   */
  onClick: () => { },
  /**
   * Call when double press
   */
  onDoubleClick: () => { },
  /**
   * Call when long press
   */
  onLongPress: () => { },
  /**
   * 
   */
  horizontalOuterRangeOffset: () => { },
  /**
   * Call when drag left
   */
  onDragLeft: () => { },
  /**
   * Call when responder release
   */
  responderRelease: () => { },
  /**
   * If provided, will be called everytime the map is moved
   */
  onMove: () => { },
  /**
   * If provided, method will be called when the onLayout event fires
   */
  layoutChange: () => { },
  /**
   * function that fires when user swipes down
   */
  onSwipeDown: () => { },
  enableHorizontalBounce: false,
  enableSwipeUp: true,
  swipeUpThreshold: 80,
  enableOnlySetPositionY: false
}

ImageViewer.propTypes = {
  /**
  * Crop width
  */
  cropWidth: PropTypes.number,
  /**
   * Crop height
   */
  cropHeight: PropTypes.number,
  /**
   * Width image
   */
  imageWidth: PropTypes.number,
  /**
   * Height image
   */
  imageHeight: PropTypes.number,
  /**
   * Enable pan to move
   */
  panToMove: PropTypes.bool,
  /**
   * Enable onch to zoom
   */
  pinchToZoom: PropTypes.bool,
  /**
   * Click distance
   */
  clickDistance: PropTypes.number,
  /**
   * Max over flow
   */
  maxOverflow: PropTypes.number,
  /**
   * Time long press
   */
  longPressTime: PropTypes.number,
  /**
   * time double click
   */
  doubleClickInterval: PropTypes.number,
  style: ViewPropTypes.style,
  /**
   * Swipe down threshold
   */
  swipeDownThreshold: PropTypes.number,
  /**
   * for enable vertical movement if user doens't want it
   */
  enableSwipeDown: PropTypes.bool,
  /**
   * Call when press
   */
  onClick: PropTypes.func,
  /**
   * Call when double press
   */
  onDoubleClick: PropTypes.func,
  /**
   * Call when long press
   */
  onLongPress: PropTypes.func,
  /**
   * 
   */
  horizontalOuterRangeOffset: PropTypes.func,
  /**
   * Call when drag left
   */
  onDragLeft: PropTypes.func,
  /**
   * Call when responder release
   */
  responderRelease: PropTypes.func,
  /**
   * If provided, will be called everytime the map is moved
   */
  onMove: PropTypes.func,
  /**
   * If provided, method will be called when the onLayout event fires
   */
  layoutChange: PropTypes.func,
  /**
   * function that fires when user swipes down
   */
  onSwipeDown: PropTypes.func,
  enableHorizontalBounce: PropTypes.bool,
  enableSwipeUp: PropTypes.bool,
  onSwipeUp: PropTypes.func,
  swipeUpThreshold: PropTypes.number,
  enableOnlySetPositionY: PropTypes.bool
}

export default ImageViewer;

