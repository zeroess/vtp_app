/**
*
* Author: duydatpham
* Email: duydatpham@gmail.com
* LoadingIndicator
*
*/

import React, { PureComponent } from 'react';
import { View, ActivityIndicator, Dimensions, Text } from 'react-native'
const { width } = Dimensions.get('window')
interface Props {
  color: string
}

export default class extends PureComponent<Props> {

  state = {
    loading: false,
    title: undefined
  }

  _show = (title: string) => {
    this.setState({
      loading: true,
      title
    })
  }
  _hide = () => {
    this.setState({
      loading: false
    })
  }

  componentDidMount() {
    window.loadingIndicator = {
      show: this._show,
      hide: this._hide
    }
  }

  render() {

    const { loading, title } = this.state
    if (!loading) {
      return null
    }

    return (
      <View style={{
        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.4)', alignItems: 'center', justifyContent: 'center',

      }} >
        <View style={{
          alignItems: 'center', justifyContent: 'center',
          backgroundColor: 'white',
          minHeight: 100, minWidth: 100,
          borderRadius: 8, maxWidth: width * 2 / 3,
        }} >
          <ActivityIndicator size="large" color={this.props.color || '#3e82f7'} />
          {
            !!title && <Text style={{
              paddingHorizontal: 16,
              textAlign: 'center',
              marginTop: 8
            }} >
              {title}
            </Text>
          }
        </View>
      </View>
    );
  }
}
