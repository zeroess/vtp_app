/*
 * Created by duydatpham@gmail.com on 17/7/2020
 * Copyright © 2020 duydatpham@gmail.com
 */

import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet, Text, StyleProp, ViewStyle } from 'react-native';
import FastImage from 'react-native-fast-image';
import _ from 'lodash'
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelTokenSource } from "axios";
import Icon from 'react-native-vector-icons/MaterialIcons';
const STOP_CMD = 'STOOOOP';

interface FileUploadProps {
  url: string;
  baseURL: string;
  uri: string;
  isDisable?: boolean;
  onUploadStart?: (uri: string) => {};
  onUploadSuccess?: (uri1: string, uri2: string) => {};
  width: number;
  height: number;
  onRemove?: (uri: string) => {};
  style?: StyleProp<ViewStyle>;
  borderRadius?: number
}

interface FileUploadState {
  success: boolean,
  error: boolean,
  isNeedUpload: boolean,
  progress: number,
}

class FileUpload extends React.PureComponent<FileUploadProps, FileUploadState> {

  static defaultProps = {
    onUploadStart: (uri: string) => { },
    onUploadSuccess: (uri1: string, uri2: string) => { },
    onRemove: () => { },
    width: 100,
    height: 100,
    borderRadius: 8,
    isDisable: false
  }

  private api: AxiosInstance;
  private cancelTokenSource?: CancelTokenSource;


  constructor(props: FileUploadProps) {
    super(props)
    this.state = {
      success: false,
      error: false,
      isNeedUpload: false,
      progress: 0,
    }
    this.api = axios.create({
      url: props.url,
      baseURL: props.baseURL,
      headers: {
        'accept': 'application/json',
      }
    });
  }

  componentDidMount() {
    this.validateThenUpload(this.props.uri);
  }

  componentWillReceiveProps(nextProps: FileUploadProps) {
    if (this.props.uri != nextProps.uri) {
      console.log('this.props.uri != nextProps.uri', this.props.uri, nextProps.uri)
      if (!!this.cancelTokenSource)
        this.cancelTokenSource?.cancel(STOP_CMD)
      this.cancelTokenSource = undefined;

      this.validateThenUpload(nextProps.uri);
    }
  }

  validateThenUpload = (uri: string) => {
    this.setState({
      success: !(!!uri && !uri.startsWith('http')),
      error: false,
      isNeedUpload: !!uri && !uri.startsWith('http'),
      progress: 0
    }, () => {
      console.log('isNeedUpload', this.state.isNeedUpload)
      this.state.isNeedUpload && !this.props.isDisable && this.upload(uri);
    })
  }

  componentWillUnmount() {
    console.log('componentWillUnmount::', this.props.uri)
    if (!!this.cancelTokenSource)
      this.cancelTokenSource?.cancel(STOP_CMD)
  }

  onProgress = (progress: ProgressEvent) => {
    this.setState({
      progress: Math.ceil(progress.loaded * 100 / (progress.total || 1))
    })
  }

  upload = async (uri: string) => {
    try {
      let formData = new FormData();

      !!this.props.onUploadStart && this.props.onUploadStart(uri)

      formData.append('files', { uri, name: `image----${Date.now()}.jpg`, type: 'multipart/form-data' });

      this.cancelTokenSource = axios.CancelToken.source();
      const response = await this.api.post(this.api.getUri({
        url: this.props.url,
        baseURL: this.props.baseURL,
        headers: {
          'accept': 'application/json',
        },
        maxContentLength: 100000000,
        maxBodyLength: 1000000000
      }), formData, {
        onUploadProgress: this.onProgress,
        cancelToken: this.cancelTokenSource.token,
        maxContentLength: 100000000,
        maxBodyLength: 1000000000
      })
      console.log('response', response)
      if (!!response.data && response.data.status == 0) {
        this.setState({
          error: false,
          success: true,
        })
        !!this.props.onUploadSuccess && this.props.onUploadSuccess(response.data.result[0], uri)
      } else {
        throw new Error('Upload ko thanh cong')
      }

    } catch (error) {
      console.log('errorerrorerror', error)
      if (!error || error.message != STOP_CMD) {
        this.setState({
          error: true,
          success: false
        })
      }
    }
  }



  render() {
    const { width, height, uri, onRemove, style, isDisable, borderRadius } = this.props
    const { isNeedUpload, progress, success, error } = this.state
    return <View style={[{
      width: width,
      height: height,
      overflow: "hidden",
      borderRadius: borderRadius,
      backgroundColor: 'white'
    }, style]} >
      <FastImage source={{ uri }} style={{ width: width, height: height }} resizeMode='cover' />
      {(!!error || !success) && <View style={[StyleSheet.absoluteFillObject, { backgroundColor: 'rgba(255,255,255, 0.7)' }]} />}

      {
        !isDisable && !!isNeedUpload && !success && !error && <View style={{
          position: 'absolute', left: 0, top: 0, right: 0, bottom: 0,
          alignItems: 'center', justifyContent: 'center'
        }}>
          <Text>
            {progress || 0}%
          </Text>
        </View>
      }
      {
        !isDisable && !!error && <View style={{
          position: 'absolute', left: 0, bottom: 0, right: 0, top: 0,
          alignItems: 'center', justifyContent: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            this.validateThenUpload(uri)
          }} >
            <Icon name='backup' />
          </TouchableOpacity>
        </View>
      }
      {
        !!error && <View style={{
          position: 'absolute', left: 4, top: 4,
        }}>
          <Icon name='error' style={{ color: 'red' }} />
        </View>
      }
      {
        !isDisable && (!!isNeedUpload || (!!uri && !isNeedUpload)) && <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, padding: 4 }}
          onPress={() => {
            if (!!this.cancelTokenSource)
              this.cancelTokenSource?.cancel(STOP_CMD)
            !!onRemove && onRemove(uri)
          }}
        >
          <Icon name='cancel' />
        </TouchableOpacity>
      }
    </View>
  }
}

export default FileUpload;
