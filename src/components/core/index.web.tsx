/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import Header from './header'
import Container from './container'
import Content from './content'
import FlatList from './FlatList'
import TouchableOpacity from './TouchableOpacity'
import Alert from './Alert'
import Avatar from './avatar';
import TabBar from './TabBar/TabBar';

export {
    Header, Container,
    Content, FlatList, TouchableOpacity,
    Alert,
    Avatar,
    TabBar,
}