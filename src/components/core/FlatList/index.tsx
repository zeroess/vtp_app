/**
* Created by duydatpham@gmail.com on Tue Oct 23 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/
import React, { PureComponent } from 'react'
import { FlatList, Platform, FlatListProps } from 'react-native'

export interface ItemT {

}
export interface ListProps<ItemT> extends FlatListProps<ItemT> {
    onLoadMore?: () => {}
}
export default class extends PureComponent<ListProps<ItemT>> {
    private allowLoadMore: boolean;
    constructor(props : ListProps<ItemT>){
        super(props);
        this.allowLoadMore = false;
    }

    render() {
        return <FlatList {...this.props}
            onRefresh={this.props.onRefresh}
            onEndReached={() => {
                if (!this.allowLoadMore)
                    return
                this.allowLoadMore = false
                this.props.onLoadMore && this.props.onLoadMore()
            }}
            style={{}}
            contentContainerStyle={this.props.style}
            onEndReachedThreshold={Platform.OS === 'android' ? 0.05 : -0.1}
            initialNumToRender={15}
            onMomentumScrollBegin={() => {
                this.allowLoadMore = true
            }}
            onScrollBeginDrag={() => {
                this.allowLoadMore = true
            }}
            showsVerticalScrollIndicator={false}
        // ListEmptyComponent
        // extraData
        />
    }
}