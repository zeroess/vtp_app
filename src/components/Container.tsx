/*
 * Created by duydatpham@gmail.com on 20/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */
import React, { memo, useContext, useMemo } from 'react'
import { ImageBackground, ScrollView, useWindowDimensions, View, ViewStyle } from 'react-native'
import FastImage from 'react-native-fast-image'
import { welcomeBg } from '../../assets/images'
import { ThemeContext } from '../context/theme'
import { TabBarSize } from '../util'

export default memo(({ hasScroll, children, style, onScroll }: { hasImgBackground?: boolean, hasScroll?: any, children?: any, style?: ViewStyle, onScroll?: any }) => {
    const { width, height } = useWindowDimensions()
    const { theme } = useContext(ThemeContext)

    const [ContainerView, styleLevel1, ContainerViewLevel2, styleLevel2, sourceBg]: any = useMemo(() => {
        console.log('theme?.backgroundImageUrl', theme?.backgroundImageUrl)
        let _ContainerView, _styleLevel1, _ContainerViewLevel2, _styleLevel2;

        if (!!theme?.backgroundImageUrl) {
            _ContainerView = View;
            _styleLevel1 = [{ width, height, paddingBottom: hasScroll ? 0 : (TabBarSize.height + TabBarSize.paddingBottom) }]
        } else {
            _ContainerView = View;
            _styleLevel1 = [{ width, height, paddingBottom: hasScroll ? 0 : (TabBarSize.height + TabBarSize.paddingBottom), backgroundColor: theme?.backgroundColor }]
        }
        if (hasScroll) {
            _ContainerViewLevel2 = ScrollView;
            _styleLevel2 = { paddingBottom: hasScroll ? 0 : TabBarSize.height + TabBarSize.paddingBottom }
        } else {
            _ContainerViewLevel2 = View;
            _styleLevel2 = { flex: 1 }
        }


        return [_ContainerView, _styleLevel1, _ContainerViewLevel2, _styleLevel2, theme?.backgroundImageUrl]
    }, [theme?.backgroundImageUrl, hasScroll])
    return <ContainerView style={styleLevel1}
    >
        {
            !!sourceBg && <FastImage source={sourceBg} style={{
                position: 'absolute',
                top: 0, left: 0, width, height,
            }} resizeMode='cover' />
        }
        {
            !!sourceBg && <View style={{
                position: 'absolute',
                top: 0, left: 0, right: 0, bottom: 0,
                backgroundColor: 'black', opacity: theme?.backgroundOpacity || 0.1,
            }} />
        }
        <ContainerViewLevel2 scrollEventThrottle={0} onScroll={onScroll}
            style={[!hasScroll && [styleLevel2, style]]} contentContainerStyle={[hasScroll && [styleLevel2, style]]}
            showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}
        >
            {children}
        </ContainerViewLevel2>
    </ContainerView>
})