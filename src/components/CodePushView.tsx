/*
 * Created by duydatpham@gmail.com on 18/08/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { PureComponent } from 'react'
import { AppState, Platform, StyleSheet, Text, View } from 'react-native'
import codePush from 'react-native-code-push';

export default class extends PureComponent<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            message: ''
        }
    }


    _checkUpdate = () => {
        codePush.sync({
            deploymentKey: Platform.select({
                android: 'JRFgvLuHo7rlZiw2cskBMC_h10Jraz20mn1_Kw',
                ios: 'Yfj3qoi_yFAwrbDmduamFEGW9mi0sbzu7WMNqH'
            }),
            installMode: codePush.InstallMode.IMMEDIATE,
        }, this.codePushStatusDidChange, this.codePushDownloadDidProgress).then(res => {
            console.log(`componentDidMount:codePush`, res)
        });
    }

    async componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
        this._checkUpdate()

    }

    _handleAppStateChange = (nextAppState: any) => {
        if (nextAppState === 'active') {
            this._checkUpdate()
        }
    };


    codePushDownloadDidProgress = (progress: any) => {
        console.log('codePush::', progress.receivedBytes + " of " + progress.totalBytes + " received.");
        this.setState({ message: `Downloading : ${progress && progress.receivedBytes && progress.totalBytes && progress.totalBytes != 0 ? Math.floor(progress.receivedBytes * 100 / progress.totalBytes) : 0}%` })
    }



    codePushStatusDidChange = (status: codePush.SyncStatus) => {
        console.log('codePush::status', status)
        switch (status) {
            case codePush.SyncStatus.CHECKING_FOR_UPDATE:
                console.log("codePush::Checking for updates.");
                break;
            case codePush.SyncStatus.DOWNLOADING_PACKAGE:
                console.log("codePush::Downloading package.");
                this.setState({ message: 'Downloading' })
                break;
            case codePush.SyncStatus.INSTALLING_UPDATE:
                console.log("codePush::Installing update.");
                this.setState({ message: 'Installing, pls don\'t close app...' })
                break;
            case codePush.SyncStatus.UP_TO_DATE: {
                console.log("codePush::Up-to-date.");
                this.setState({ message: '' })
                break;
            }
            case codePush.SyncStatus.UPDATE_INSTALLED:
                console.log("codePush::Update installed.");
                this.setState({ message: 'Restarting, pls don\'t close app...' })
                setTimeout(() => {
                    codePush.restartApp()
                }, 500)
                break;
        }
    }
    render() {
        const { message } = this.state
        if (!message) {
            return null;
        }
        return <View style={[StyleSheet.absoluteFill, {
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            alignItems: 'center', justifyContent: 'center'
        }]} >
            <View style={{
                backgroundColor: 'rgba(0, 0, 0, 0.7)',
                alignItems: 'center', justifyContent: 'center',
                padding: 20, borderRadius: 4
            }} >
                <Text style={{
                    color: 'white'
                }} >
                    {message}
                </Text>
            </View>
        </View>
    }
}