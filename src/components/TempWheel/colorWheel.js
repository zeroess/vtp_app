/*
 * Created by duydatpham@gmail.com on 25/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { Component } from 'react'
import {
  Animated,
  Image,
  Dimensions,
  PanResponder,
  StyleSheet,
  View,
  Platform,
} from 'react-native';

export default class ColorWheel extends Component {
  static defaultProps = {
    thumbSize: 50,
    initialDeg: 0,
    onChange: () => { },
    precision: 0,
    fromDeg: -120,
    toDeg: -30,
  }

  constructor(props) {
    super(props)
    this.state = {
      offset: { x: 0, y: 0 },
      currentDeg: props.fromDeg || 0,
      pan: new Animated.ValueXY(),
      radius: 0
    }
  }

  // UNSAFE_componentWillReceiveProps(nextProps) {
  //   if (nextProps.initialDeg !== this.state.currentDeg) {
  //     this.forceUpdate(nextProps.initialDeg)
  //     this.setState({
  //       currentDeg: nextProps.initialDeg
  //     })
  //   }
  // }


  changeDeg(_deg) {
    let from = 360 + this.props.fromDeg;
    let alpha = from - _deg * (from - this.props.toDeg)

    if (alpha > 180) {
      alpha = alpha - 360
    }

    this.forceUpdate(alpha)
    this.setState({
      currentDeg: alpha
    })

  }


  componentDidMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponderCapture: ({ nativeEvent }) => {
        if (this.outBounds(nativeEvent)) {
          this._touched = false;
          return
        }
        const { deg, radius } = this.calcPolar(nativeEvent);
        if (Math.abs(1 - radius) * this.state.radius > this.props.thumbSize || (this.props.fromDeg + 5 < deg && this.props.toDeg - 5 > deg)) {
          this._touched = false;
          return
        }
        this._touched = true

        if ((this.props.fromDeg < deg && this.props.toDeg > deg)) {
          return
        }
        this.updateColor({ nativeEvent })
        const { left, top } = this.calcCartesian(deg, (radius > 1) ? 1 : radius);
        this.state.pan.setValue({
          x: left - this.props.thumbSize / 2,
          y: top - this.props.thumbSize / 2,
        })
        return true
      },
      onStartShouldSetPanResponder: () => this._touched || false,
      onMoveShouldSetPanResponderCapture: () => this._touched || false,
      onPanResponderGrant: () => this._touched || false,
      onPanResponderMove: (event, gestureState) => {
        const { deg, radius } = this.calcPolar(gestureState);
        if (this.props.fromDeg < deg && this.props.toDeg > deg) {
          return
        }
        const { left, top } = this.calcCartesian(deg, (radius > 1) ? 1 : radius);

        this.state.pan.setValue({
          x: left - this.props.thumbSize / 2,
          y: top - this.props.thumbSize / 2,
        })

        this.updateColor(event, gestureState);
      },
      onMoveShouldSetPanResponder: () => this._touched || false,
      onPanResponderRelease: ({ nativeEvent }) => {
        this.state.pan.flattenOffset()
        const { radius, deg } = this.calcPolar(nativeEvent)
        const { currentDeg } = this.state;
        this.props.onChange && this.props.onChange(this.calcDeg(currentDeg))
      },
    })

    setTimeout(() => {
      this.measureOffset()
    }, 200);
  }

  calcDeg(_raw) {
    let alpha = _raw;
    if (_raw < 0) {
      if (_raw < -90) {
        alpha = 360 + _raw;
      } else {

      }
    }
    let from = 360 + this.props.fromDeg;
    alpha = from - alpha;
    return Math.abs(parseFloat(parseFloat(String(alpha / (from - this.props.toDeg))).toFixed(3)))
  }

  onLayout() {
    this.measureOffset()
  }

  measureOffset() {
    /*
    * const {x, y, width, height} = nativeEvent.layout
    * onlayout values are different than measureInWindow
    * x and y are the distances to its previous element
    * but in measureInWindow they are relative to the window
    */
    this.self?.measureInWindow((x, y, width, height) => {
      const absX = x % 250
      const radius = 250 / 2
      const offset = {
        x: absX + width / 2,
        y: y % 250 + height / 2,
      }

      this.setState({
        offset,
        radius,
        height,
        width,
        top: y % 250,
        left: absX,
      })
      this.forceUpdate(this.state.currentDeg)
    })
  }

  calcPolar(gestureState) {
    const {
      pageX, pageY, moveX, moveY,
    } = gestureState
    const [x, y] = [pageX || moveX, pageY || moveY]
    const [dx, dy] = [x - this.state.offset.x, y - this.state.offset.y]
    return {
      deg: Math.atan2(dy, dx) * (-180 / Math.PI),
      // pitagoras r^2 = x^2 + y^2 normalized
      radius: Math.sqrt(dy * dy + dx * dx) / this.state.radius,
    }
  }

  outBounds(gestureState) {
    const { radius } = this.calcPolar(gestureState)
    return radius > 1
  }

  calcCartesian(deg) {
    const r = this.state.radius - this.props.thumbSize / 2 // was normalized
    const rad = Math.PI * deg / 180
    const x = r * Math.cos(rad)
    const y = r * Math.sin(rad)
    return {
      left: this.state.width / 2 + x,
      top: this.state.height / 2 - y,
    }
  }

  updateColor = ({ nativeEvent }) => {
    const { deg, radius } = this.calcPolar(nativeEvent)
    // const currentDeg = colorsys.hsv2Hex({ h: deg, s: 100 * ((radius < 1) ? radius : 1), v: 100 })
    this.setState({ currentDeg: deg })
  }

  forceUpdate = deg => {
    const { left, top } = this.calcCartesian(deg)
    this.setState({ currentDeg: deg })
    this.state.pan.setValue({
      x: left - this.props.thumbSize / 2,
      y: top - this.props.thumbSize / 2,
    })
  }

  render() {
    const { radius } = this.state
    const thumbStyle = [
      styles.circle,
      this.props.thumbStyle,
      {
        width: this.props.thumbSize,
        height: this.props.thumbSize,
        borderRadius: this.props.thumbSize / 2,
        opacity: this.state.offset.x === 0 ? 0 : 1,
        alignItems: 'center', justifyContent: 'center'
      },
    ]

    const panHandlers = this._panResponder && this._panResponder.panHandlers || {}

    return (
      <View
        ref={node => {
          this.self = node
        }}
        {...panHandlers}
        onLayout={nativeEvent => this.onLayout(nativeEvent)}
        style={[styles.coverResponder, this.props.style]}
        pointerEvents='box-none'
      >
        <Image
          style={[styles.img, { width: 250 }]}
          source={require('./bg.png')}
          resizeMode='contain'
        />
        <Animated.View style={[this.state.pan.getLayout(), thumbStyle]}>
        </Animated.View>

        <View style={{
          position: 'absolute', width: 80, height: 80, borderRadius: 80,
          backgroundColor: '#2bceff',
          alignItems: 'center', justifyContent: 'center'
        }} >
          <Image source={require('./icLightIncandescent.png')} style={{ width: 24, height: 24, }} resizeMode='contain' />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  coverResponder: {
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
    overflow: 'visible'
  },
  img: {
    alignSelf: 'center',
    position: 'absolute',
    top: -1
  },
  circle: {
    position: 'absolute',
    backgroundColor: Platform.select({
      ios: 'transparent',
      android: '#05a081'
    }),
    borderWidth: 3,
    borderColor: 'white',
    elevation: 3,
    shadowColor: 'rgb(46, 48, 58)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    zIndex: 101
  },
})
