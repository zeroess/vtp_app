/*
 * Created by duydatpham@gmail.com on 21/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { Platform } from 'react-native';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';

export const requestLocation = async () => {
    let resLocation = await check(Platform.select({
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
    }) as any)
    if (RESULTS.GRANTED == resLocation) {
        return true;
    }
    let resRequest = await request(Platform.select({
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
    }) as any)
    if (RESULTS.GRANTED == resRequest) {
        return true;
    }


    return
}