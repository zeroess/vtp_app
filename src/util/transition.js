import CardStackStyleInterpolator from './CardStackStyleInterpolator'

export const transitionConfig = () => {
    // if (Actions.currentScene == 'settingip')
    return {
        // transitionSpec: {
        //     duration: 250
        // },
        screenInterpolator: sceneProps => {
            const { position, layout, scene, index, scenes } = sceneProps

            const thisSceneIndex = scene.index
            const height = layout.initHeight
            const width = layout.initWidth

            // We can access our navigation params on the scene's 'route' property
            var thisSceneParams = scene.route.params || {}


            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
                outputRange: [width, 0, 0],
            })

            // const translateY = position.interpolate({
            //     inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
            //     outputRange: [height, 0, 0],
            // })

            // const opacity = position.interpolate({
            //     inputRange: [thisSceneIndex - 1, thisSceneIndex - 0.5, thisSceneIndex],
            //     outputRange: [0, 1, 1],
            // })

            // const scale = position.interpolate({
            //     inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
            //     outputRange: [4, 1, 1]
            // })

            const slideFromRight = { transform: [{ translateX }] }
            // const scaleWithOpacity = { opacity, transform: [{ scaleX: scale }, { scaleY: scale }] }
            // const slideInFromBottom = { transform: [{ translateY }] }
            if (thisSceneParams) {
                if (thisSceneParams.direction == 'horizontal')
                    return CardStackStyleInterpolator.forHorizontal(sceneProps)
                if (thisSceneParams.direction == 'vertical-fade')
                    return CardStackStyleInterpolator.forFadeFromBottomAndroid(sceneProps)
                if (thisSceneParams.direction == 'vertical')
                    return CardStackStyleInterpolator.forVertical(sceneProps)
                // return slideFromRight
                if (thisSceneParams.direction == 'fade')
                    return CardStackStyleInterpolator.forFade(sceneProps)
            }
            return slideFromRight//CardStackStyleInterpolator.forHorizontal(sceneProps)
        },
    }
}