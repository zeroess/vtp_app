/**
* Created by duydatpham@gmail.com on Mon Jul 30 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import moment from 'moment';
import { Dimensions, Platform, StatusBar } from 'react-native'
const dimen = Dimensions.get('window');

export function isIphoneX() {
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        ((dimen.height === 780 || dimen.width === 780)
            || (dimen.height === 812 || dimen.width === 812)
            || (dimen.height === 844 || dimen.width === 844)
            || (dimen.height === 896 || dimen.width === 896)
            || (dimen.height === 926 || dimen.width === 926))
    );
}


export const HeaderSize = {
    height: Platform.OS === 'ios' ? (isIphoneX() ? 94 : 74) : (54),
    paddingTop: (isIphoneX() ? 40 : (Platform.OS === 'ios' ? 20 : (0))),
}

export const TabBarSize = {
    height: 60,
    paddingBottom: isIphoneX() ? 24 : 0,
}

export const validateEmail = (email) => {
    if (!email) {
        return false
    }
    let re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    return re.test(email)
}

export const formatNumberToCurrency = (n = 0, toFixed = 2) => {
    let reg = /(\d)(?=(\d{3})+(?:\.\d+)?$)/g

    let number = parseFloat(n).toFixed(toFixed)
    if (parseInt(n) - number == 0) {
        number = parseInt(n)
    }

    return number.toString().replace(reg, '$&.');
}

export const suggestMoney = (value) => {
    return [1, 2, 5, 10].map(vv => vv * Math.pow(10, String(parseInt(value)).length - 1))
}

export const numberToString = (number, fix = 2) => {
    return `00000${number}`.slice(-fix)
}

export const parsePhonenumber = (number) => {
    if (number.startsWith('84'))
        return number
    if (number.startsWith('0'))
        return `84${number.slice(1, number.length)}`
    return `84${number}`
}

export const parsePhonenumber2 = (number) => {
    if (number.startsWith('84'))
        return `0${number.slice(2, number.length)}`
    if (number.startsWith('+84'))
        return `0${number.slice(3, number.length)}`
    return number
}

export const convertHsvToHex = ({ h, s, v }) => {
    return `${`0000${h.toString(16)}`.slice(-4)}${`0000${(s * 10).toString(16)}`.slice(-4)}${`0000${(v * 10).toString(16)}`.slice(-4)}`
}

export const convertHexToHsv = (str) => {
    if (!str) {
        return { h: 0, s: 0, v: 0 }
    }
    return {
        h: parseInt(str.substring(0, 4), 16),
        s: Math.floor(parseInt(str.substring(4, 8), 16) / 10),
        v: Math.floor(parseInt(str.substring(8, 16), 16) / 10),
    }
}
const WEEK_LIST = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
export const WEEK_LIST_SHORT = ["S", "M", "T", "W", "T", "F", "S"];
export const getStatusTextRepeat = (str) => {
    if (!str || str == '0000000') {
        return 'Once'
    }
    if (str == '1111111') {
        return 'Every day'
    }
    return str.split('').map((__, i) => __ == '1' ? WEEK_LIST[i] : null).filter(___ => !!___).join(', ')
}

export const parseSceneData = ({
    h, s, v,
    bright = 0,
    frequency = 0,
    temperature = 0
}) => {
    let sceneNumber = '00'
    let unitChangeTime = `00${frequency}`.slice(-2)
    let unitChangeMode = '00'
    let white = `0000${bright.toString(16)}`.slice(-4)
    let temp = `0000${temperature.toString(16)}`.slice(-4)

    return [sceneNumber, unitChangeTime, unitChangeMode, convertHsvToHex({ h, s, v }), white, temp].join('')
}