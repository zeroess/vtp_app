/**
* Created by duydatpham@gmail.com on Wed Aug 22 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import { parseSceneData } from "."
import { modeParty, modeReadingbook, modeRomantic, modeWorking } from "../../assets/images"


export const HOME_MEMBER_ROLE = {
    OWNER: 2,
    ADMIN: 1,
    MEMBER: 0
}

export const HOME_MEMBER_ROLE_NAME = {
    [HOME_MEMBER_ROLE.OWNER]: 'Home Owner',
    [HOME_MEMBER_ROLE.ADMIN]: 'Home Admin',
    [HOME_MEMBER_ROLE.MEMBER]: 'Home Member',
}

export const WORK_MODE = [
    {
        title: 'Reading',
        image: modeReadingbook,
        payload: parseSceneData({
            "h": 38.0,
            "s": 8.0,
            "v": 255.0
        })
    },
    {
        title: 'Romantic',
        image: modeRomantic,
        payload: parseSceneData({
            "h": 226.0,
            "s": 176.0,
            "v": 181.0
        })
    },
    {
        title: 'Working',
        image: modeWorking,
        payload: parseSceneData({
            "h": 16.0,
            "s": 255.0,
            "v": 207.0
        })
    },
    {
        title: 'Party',
        image: modeParty,
        payload: parseSceneData({
            "h": 210.0,
            "s": 169.0,
            "v": 146.0,
            "bright": 100,
            "frequency": 76,
            "temperature": 84
        })
    },
]