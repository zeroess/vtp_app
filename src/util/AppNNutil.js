import React, { Component } from "react";
import { Dimensions, Text, View, Alert as ReactAlert } from "react-native";
import { TouchableOpacity, Alert, FlatList } from "../components/core";
import RowItemGradientButton from '../components/core/RowItemGradientButton';
import CreateNewPopup from "../components/core/CreateNewPopup";
import LinearGradient from "react-native-linear-gradient";
import { TabBarSize } from "../util";
import { Actions } from "react-native-router-flux";
const { width } = Dimensions.get("window");
function setState(objectSelf, value, stateAtt, subStateAtt) {
  if (subStateAtt) {
    objectSelf.setState({
      ...objectSelf.state,
      [subStateAtt]: { ...objectSelf.state[subStateAtt], [stateAtt]: value },
    });
  } else {
    objectSelf.setState({
      ...objectSelf.state,
      [stateAtt]: value,
    });
  }
}
function createActionSheetOptions(object, propAtt, stateAtt, subStateAtt) {
  const arrayName = object.props[propAtt];
  const options = arrayName.map((data) => {
    return {
      title: data.name,
      itemId: data.id,
      onPress: (item) => {
        const v = arrayName.find((e) => e.id == item.itemId);
        if (subStateAtt) {
          object.setState({
            ...object.state,
            [subStateAtt]: { ...object.state[subStateAtt], [stateAtt]: v },
          });
        } else {
          object.setState({
            ...object.state,
            [stateAtt]: v,
          });
        }
      },
    };
  });
  return options;
}
function createActionSheetOptionsWith(
  object,
  propAtt,
  stateAtt,
  subStateAtt,
  funcGetName
) {
  const arrayName = object.props[propAtt];
  const options = arrayName.map((data) => {
    return {
      title: funcGetName ? funcGetName(data) : data.name,
      itemId: data.id,
      onPress: (item) => {
        const v = arrayName.find((e) => e.id == item.itemId);
        if (subStateAtt) {
          object.setState({
            ...object.state,
            [subStateAtt]: { ...object.state[subStateAtt], [stateAtt]: v },
          });
        } else {
          object.setState({
            ...object.state,
            [stateAtt]: v,
          });
        }
      },
    };
  });
  return options;
}
function renderAlertCreateNew(props, popup, functionName, callBack) {
  return (
    <Alert
      ref={(alert) => (props[popup.name] = alert)}
      hasTextInput
      clickToClose={false}
      width={Math.min(320, width - 48)}
      renderContent={() => {
        return (
          <CreateNewPopup
            title={popup.title}
            fields={popup.fields}
            onEnterClick={async (data) => {
              try {
                window.loadingIndicator.show();
                let res = await window.connection[functionName](data);
                if (res.status) {
                  callBack();
                  props[popup.name].close();
                  window.alertCustom.showSuccess(
                    popup.successMessage || "Tạo thành công"
                  );
                }
              } catch (error) {
                console.log("error", error);
              } finally {
                window.loadingIndicator.hide();
              }
            }}
          ></CreateNewPopup>
        );
      }}
    ></Alert>
  );
}
function bottomButtonSave(
  object,
  createFunction,
  updateFunction,
  uploadData,
  callBack
) {
  return (
    <TouchableOpacity
      onPress={async () => {
        try {
          window.loadingIndicator.show();
          const isUpdate = object.props.isUpdate;
          let res = null;
          if (isUpdate) {
            res = await window.connection[updateFunction](
              uploadData.id,
              uploadData
            );
          } else {
            res = await window.connection[createFunction](uploadData);
          }
          if (res.status) {
            let infoId = uploadData.id;
            if (!isUpdate) {
              window.alertCustom.showSuccess(`Tạo thông tin thành công.`);
              infoId = res.data.id;
            } else {
              window.alertCustom.showSuccess(`Sửa thông tin thành công.`);
            }
            if (callBack) {
              callBack({ infoId: infoId });
            } else {
              Actions.pop();
            }
          }
        } catch (error) {
          console.log("error", error);
        } finally {
          window.loadingIndicator.hide();
        }
      }}
    >
      <LinearGradient
        style={{
          paddingTop: 12,
          alignItems: "center",
          paddingBottom: Math.max(12, 4 + TabBarSize.paddingBottom),
        }}
        start={{ x: 0, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
        colors={["#E51701", "#FF9561"]}
      >
        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              color: "white",
              fontSize: 18,
            }}
          >
            Lưu
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
}
function bottomButtonDouble(object, data, functionDelete, editCallBack) {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity
        style={{
          width: "50%",
          backgroundColor: "#dddddd",
          paddingTop: 12,
          alignItems: "center",
          paddingBottom: Math.max(12, 4 + TabBarSize.paddingBottom),
        }}
        onPress={() => {
          ReactAlert.alert(
            "Xác nhận",
            "Bạn chắc chắn muốn xoá thông tin này?",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "OK",
                onPress: async () => {
                  try {
                    window.loadingIndicator.show();
                    let res = await window.connection[functionDelete](data.id);
                    if (res.status) {
                      Actions.pop();
                      window.alertCustom.showSuccess(
                        `Xoá thông tin thành công.`
                      );
                    }
                  } catch (error) {
                    console.log("error", error);
                  } finally {
                    window.loadingIndicator.hide();
                  }
                },
              },
            ]
          );
        }}
      >
        <Text
          style={{
            color: "rgb(239, 58, 47)",
            fontSize: 18,
          }}
        >
          Xoá
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: "50%",
          backgroundColor: "rgb(239, 58, 47)",
          paddingTop: 12,
          alignItems: "center",
          paddingBottom: Math.max(12, 4 + TabBarSize.paddingBottom),
        }}
        onPress={() => {
          editCallBack({
            isUpdate: true,
            editData: data,
          });
        }}
      >
        <Text
          style={{
            color: "white",
            fontSize: 18,
          }}
        >
          Chỉnh sửa
        </Text>
      </TouchableOpacity>
    </View>
  );
}
function bottomButtonCreate(callBack) {
  return (
    <TouchableOpacity onPress={callBack}>
      <LinearGradient
        style={{
          paddingTop: 12,
          alignItems: "center",
          paddingBottom: Math.max(12, 4 + TabBarSize.paddingBottom),
        }}
        start={{ x: 0, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
        colors={["#E51701", "#FF9561"]}
      >
        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              color: "white",
              fontSize: 18,
            }}
          >
            Tạo mới
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
}
function rowDetail(data, renderItem, functionDetail, functionDelete, deleteCallBack ) { 
  const {id} = data;
  return (
    <RowItemGradientButton
      onPress={() => {
        functionDetail({ infoId: id });
      }}
      onPressDelete={() => {
        ReactAlert.alert("Xác nhận", "Bạn chắc chắn muốn xoá?", [
          {
            text: "Huỷ",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "Tiếp tục",
            onPress: async () => {
              try {
                window.loadingIndicator.show();
                let res = await window.connection[functionDelete](id);
                if (res.status) {
                  deleteCallBack();
                  window.alertCustom.showSuccess(`Xoá thành công.`);
                }
              } catch (error) {
                console.log("error", error);
              } finally {
                window.loadingIndicator.hide();
              }
            },
          },
        ]);
      }}
      onPressEdit={() => {
        functionDetail({ infoId: id });
      }}
      title={renderItem.title(data) || ""}
      subtitles={renderItem.subtitles(data)}
    />
  );
}
function listDetail(objectSelf, data, loadMore, renderItem, functionDetail, functionDelete) {
  return (
    <FlatList
      data={data.data}
      renderItem={({ item, index }) => {        
        return rowDetail(
          item,
          renderItem,
          functionDetail,
          functionDelete,
          loadMore
        );
      }}
      keyExtractor={(item, index) => `listKey----${index}`}
      style={{
        paddingTop: 16,
      }}
      refreshing={data.loading}
      onRefresh={() => {
        loadMore({ page: 1 });
      }}
      onLoadMore={() => {
        if (data.loading || !data.next_page_url) {
          return;
        }
        loadMore({ page: data.page + 1 });
      }}
      extraData={objectSelf.props}
    />
  );
}
export default {
  createActionSheetOptions,
  createActionSheetOptionsWith,
  renderAlertCreateNew,
  bottomButtonSave,
  bottomButtonDouble,
  bottomButtonCreate,
  listDetail,
  setState
};
