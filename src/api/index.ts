/*
 * Created by duydatpham@gmail.com on 06/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { Action } from "react-fetching-library"


export interface BaseAction extends Action<any> {
    jwt?: string
}


export const uploadFile = async (image: any) => {
    const formdata = new FormData()
    formdata.append('file', {
        uri: image.path,
        name: image.path.split('/').pop(),
        originalname: image.path.split('/').pop(),
        type: 'multipart/form-data'
    })
    formdata.append('type', 'file')

    let res = await fetch(`https://util.upbase.vn/files/upload`, {
        method: 'POST',
        headers: {
            'x-upbase-functions-secret': "Upbase@!@"
        },
        body: formdata
    }).then(res => res.json())

    return res;
}