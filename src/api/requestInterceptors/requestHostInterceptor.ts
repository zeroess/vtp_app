/*
 * Created by duydatpham@gmail.com on 14/09/2020
 * Copyright (c) 2020 duydatpham@gmail.com
 */

import { RequestInterceptor } from 'react-fetching-library';

export const requestHostInterceptor: (host: string) => RequestInterceptor = host => () => async action => {
  let endpoint = action.endpoint
  if (!endpoint.startsWith('http')) {
    endpoint = `${host}${action.endpoint}`
  }
  return {
    ...action,
    endpoint
  };
};
