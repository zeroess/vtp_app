/*
 * Created by duydatpham@gmail.com on 24/12/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { BaseAction } from "..";


export const fetchAddress = (
    { latlng = '' }: { latlng?: string }
): BaseAction => ({
    method: 'GET',
    endpoint: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=AIzaSyCpjkvJTsJ0Lrei5Thmdne2s_7rX8jl6JM`,
});