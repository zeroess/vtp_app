/*
 * Created by duydatpham@gmail.com on 12/4/2020
 * Copyright © 2020 duydatpham@gmail.com
 */

import React from 'react'
import { Text, TextInput, StyleSheet, Platform } from 'react-native'
console.disableYellowBox = true;
export const typography = () => {
    const oldTextRender = Text.render
    Text.render = function (...args) {
        const origin = oldTextRender.call(this, ...args)
        return React.cloneElement(origin, {
            style: [styles.defaultText, origin.props.style],
        })
    }


    const oldTextInputRender = TextInput.render
    TextInput.render = function (...args) {
        const origin = oldTextInputRender.call(this, ...args)
        return React.cloneElement(origin, {
            style: [styles.defaultText, origin.props.style],
        })
    }

    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;
}

const styles = StyleSheet.create({
    defaultText: {
        fontFamily: 'SF Pro Text',
        // paddingTop: Platform.OS == 'ios' ? 4 : 0,
    }
});