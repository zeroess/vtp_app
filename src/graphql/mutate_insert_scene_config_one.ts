import gql from 'graphql-tag';

export default gql`
mutation insert_scene_config_one($icon_name: String!, $id: String!) {
  insert_scene_config_one(object: {icon_name: $icon_name, id: $id}) {
    icon_name
    id
  }
}

`;
