import gql from 'graphql-tag';

export default gql`
query home_configs_by_user_id($_eq: String!) {
  home_configs(where: {user_id: {_eq: $_eq}}) {
    background
    id
    user_id
  }
}

`;
