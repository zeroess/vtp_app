import gql from 'graphql-tag';

export default gql`
mutation update_home_configs_by_pk($_set: home_configs_set_input!, $id: numeric!) {
  update_home_configs_by_pk(pk_columns: {id: $id}, _set: $_set) {
    background
    id
    user_id
  }
}

`;
