import gql from 'graphql-tag';

export default gql`
query scene_config_by_pk($id: String!) {
  scene_config_by_pk(id: $id) {
    icon_name
    id
  }
}

`;
