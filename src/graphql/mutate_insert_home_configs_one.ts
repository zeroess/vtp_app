import gql from 'graphql-tag';

export default gql`
mutation insert_home_configs_one($background: jsonb = "", $id: numeric = "", $user_id: String = "") {
  insert_home_configs_one(object: {user_id: $user_id, id: $id, background: $background}) {
    background
    id
    user_id
  }
}

`;
