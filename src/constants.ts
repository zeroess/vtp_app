/*
 * Created by duydatpham@gmail.com on 07/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { IC_SCENE, wp1, wp2, wp3, wp4, wp5 } from "../assets/images";

export type WallpaperProps = 'wp1' | 'wp2' | 'wp3' | 'wp4' | "wp5"

export const BACKGROUD_WALLPAPER = {
    'wp1': wp1,
    'wp2': wp2,
    'wp3': wp3,
    'wp4': wp4,
    'wp5': wp5
}

export const SCENE_TYPE = {
    'TAP_TO_RUN': 0,
    'SCHEDULE': 1,
    'AUTO': 2,
}
export const SCENE_TYPE_NAME = {
    [SCENE_TYPE.TAP_TO_RUN] : 'Launch Tap to Run',
    [SCENE_TYPE.SCHEDULE] : 'Schedule',
    [SCENE_TYPE.AUTO] : 'When device status change',
}
export const SCENE_TYPE_ICON_NAME = {
    [SCENE_TYPE.TAP_TO_RUN] : 'icTab',
    [SCENE_TYPE.SCHEDULE] : 'icSchedule',
    [SCENE_TYPE.AUTO] : 'icControl',
}

export enum AppColor {
    LIGHT = "LIGHT",
    DARK = "DARK"
}

export const TAP_TO_RUN_ICON = {
    icScene1: IC_SCENE[0],
    icScene2: IC_SCENE[1],
    icScene3: IC_SCENE[2],
    icScene4: IC_SCENE[3],
    icScene5: IC_SCENE[4],
}

export enum STATUS_DEVICE_FUNCTION {
    FIX = "FIX",
    ADD = "ADD"
}

export const TIME_ZONE_ID = "Asia/Ho_Chi_Minh"
export const DATE : Array<{code : string,value : string}> = [{
    code : "M",
    value : "Monday",
  },
  {
    code : "T",
    value : "Tuesday",
  },
  {
    code : "W",
    value : "Wednesday",
  },
  {
    code : "T",
    value : "Thursday",
  },
  {
    code : "F",
    value : "Friday",
  },
  {
    code : "S",
    value : "Saturday",
  },
  {
    code : "S",
    value : "Sunday",
  }]