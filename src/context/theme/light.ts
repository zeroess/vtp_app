/*
 * Created by duydatpham@gmail.com on 11/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { welcomeBg } from "../../../assets/images";
import ThemeInterface from "./types";


const LIGHT: ThemeInterface = {
    iconDeviceColor: "gray",
    backgroundColor: 'white',
    backgroundColorOpacity: 'rgba(249,249,249,0.4)',

    backgroundImageUrl: undefined,
    textGray: 'rgba(158,169,184,1)',
    backgroundTab: '#12091F',
    tabBackground: 'rgba(255, 255, 255, 0.8)',
    mainColor: 'rgba(255,158,0,1)',
    mainColorSecondary: 'rgba(5, 160, 129, 0.1)',

    textWhite: 'white',
    textBlack: '#232a34',
    textBlackPlaceholder: '#b5bdca',
    red: 'red',
    textBlackSecondary: '#9ea9b8',
    stepActive : "#1ed930",

    textDanger: "#ff4d18",

    iconBlack: '#232a34',
    // tabTextActiveColor : '#FA8C0A',
    buttonBackgroundMain: 'rgba(255,158,0,1)',
    buttonBackgroundWhite: 'white',
    buttonBackgroundWhiteDisable: 'rgba(35, 42, 52, 0.1)',
    buttonBackgroundBlackDisable: 'rgba(0,0,0,0.5)',

    tabIconActiveColor: 'rgba(255,158,0,1)',
    tabIconInactiveColor: "#767B81",

    tabTextActiveColor: 'rgba(255,158,0,1)',
    tabTextInactiveColor: "#767B81",

    modalBackgroundColor: 'white',
    modalBackgroundOverlayColor: 'black',

    dividerColor: '#edf0f4',
    borderColor: '#dde2ea',

    secondBackground: '#E8EDF1',
    switchActiveBackground: 'rgba(255,158,0,1)',
    switchActive: 'white',
    unstableBackground: 'white',
    lineColor: '#dde2ea',
    unstableText: "#232a34",
    backgroundHeader: 'white',

    home: {
        textWhite: 'white'
    },
    panel: {
        textWhite: 'white',
        buttonBackgroundBlackDisable: 'rgba(0,0,0,0.5)',
        backgroundColor: "white",
        tabTextActiveColor: 'rgba(255,158,0,1)',
        textBlackSecondary: '#9ea9b8',
        textBlack: 'black',
        backgroundColorOpacity: 'rgba(249,249,249,0.4)',
        mainColor: 'rgba(255,158,0,1)',
        backgroundColorGray: '#F2F2F2',
    },

    smart: {
        stepperInactiveBackgroundColor: '#9ea9b8'
    },
    backgroundColorListItem: '#EDF0F4',
}

export default LIGHT;