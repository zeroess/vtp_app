/*
 * Created by duydatpham@gmail.com on 22/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { BACKGROUD_WALLPAPER, WallpaperProps, AppColor } from '../../constants';
import rootReducer, { RootState } from '../../redux/reducers';
import LIGHT from './light';
import DARK from './dark';

import ThemeInterface from './types';

export interface FontSizeInterface {
  fontIconApp: number
  fontIconDevice: number
  fontText: {
    "f10": number,
    "f12": number,
    "f14": number,
    "f16": number,
    "f18": number,
    "f24": number,
  }
}

type ContextProps = {
  theme?: ThemeInterface
  fontSize?: FontSizeInterface
};

export const ThemeContext = React.createContext<Partial<ContextProps>>({});

export const ThemeProvider = ({ children }: any) => {
  const configHome = useSelector((state: RootState) => state.config.homeConfig[state.home.currentHome?.homeId])
  const { color_app } = useSelector((state: RootState) => state.config)
  const [theme, setTheme] = useState<ThemeInterface>(LIGHT)

  // useEffect(() => {
  //   setTheme(color_app === AppColor.LIGHT ? LIGHT : DARK)
  // }, [color_app])

  const [fontSize] = useState<FontSizeInterface>({
    fontIconApp: 24,
    fontIconDevice: 24,
    fontText: {
      "f10": 10,
      "f12": 12,
      "f14": 14,
      "f16": 16,
      "f18": 18,
      "f24": 24,
    }
  })

  useEffect(() => {
    setTheme({
      ...(color_app === AppColor.LIGHT ? LIGHT : DARK),
      backgroundImageUrl: (configHome?.type == 'link' ? { uri: configHome?.uri } : (BACKGROUD_WALLPAPER[configHome?.uri as WallpaperProps] as any) || BACKGROUD_WALLPAPER['wp5']),
      backgroundOpacity: configHome?.opacity || 0.1
    })
  }, [configHome, color_app])
  return (
    <ThemeContext.Provider
      value={{
        theme, fontSize
      }}>
      {children}
    </ThemeContext.Provider>
  );
};
