/*
* Created by duydatpham@gmail.com on 11/09/2021
* Copyright (c) 2021 duydatpham@gmail.com
*/
import { ImageStoreStatic, ImageURISource } from "react-native";
export default interface ThemeInterface {
    backgroundColor: string
    
    backgroundImageUrl?: string | ImageURISource | ImageStoreStatic
    backgroundOpacity?: number,
    backgroundColorOpacity?: string,
    // tabTextActiveColor ?: string,
    textGray?: string,
    iconDeviceColor: string
    backgroundTab?: string,
    mainColor: string
    mainColorSecondary: string
    tabBackground: string,
    textWhite: string
    textBlack: string
    textBlackPlaceholder: string
    secondBackground?: string
    textDanger: string
    unstableBackground?: string
    textBlackSecondary: string
    stepActive : string
    iconBlack: string

    buttonBackgroundMain: string
    buttonBackgroundWhite: string
    buttonBackgroundWhiteDisable: string
    buttonBackgroundBlackDisable: string
    modalBackgroundColor: string
    modalBackgroundOverlayColor: string

    tabIconActiveColor: string
    tabIconInactiveColor: string

    tabTextActiveColor: string
    tabTextInactiveColor: string

    dividerColor: string
    borderColor: string
    switchActiveBackground: string,
    switchActive: string,
    lineColor: string,
    unstableText: string,
    backgroundHeader: string,
    red : string,
    //Theme in home
    home?: {
        textWhite: string
    }
    panel?: {
        textWhite?: string,
        buttonBackgroundBlackDisable?: string,
        backgroundColor?: string,
        tabTextActiveColor?: string,
        textBlackSecondary?: string,
        textBlack?: string,
        backgroundColorOpacity?: string,
        mainColor?: string,
        backgroundColorGray: string
    },
    smart?: {
        stepperInactiveBackgroundColor?: string
    },

    backgroundColorListItem?: string
}