/*
 * Created by duydatpham@gmail.com on 11/09/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { welcomeBg } from "../../../assets/images";
import ThemeInterface from "./types";


const DARK: ThemeInterface = {
    iconDeviceColor: "gray",
    backgroundColor: 'black',
    backgroundColorOpacity: 'rgba(249,249,249,0.4)',

    backgroundImageUrl: undefined,
    textGray: 'rgba(158,169,184,1)',
    backgroundTab: '#12091F',
    tabBackground: 'rgba(0, 0, 0, 0.5)',
    mainColor: 'rgba(255,158,0,1)',
    mainColorSecondary: 'rgba(5, 160, 129, 0.1)',

    textWhite: '#232a34',
    textBlack: 'white',
    textBlackPlaceholder: '#b5bdca',
    textBlackSecondary: '#9ea9b8',
    stepActive : "#1ed930",

    textDanger: "#ff4d18",

    iconBlack: 'white',
    buttonBackgroundMain: 'rgba(255,158,0,1)',
    buttonBackgroundWhite: 'rgba(255, 255, 255, 0.1)',
    buttonBackgroundWhiteDisable: 'rgba(35, 42, 52, 0.1)',
    buttonBackgroundBlackDisable: 'rgba(0,0,0,0.5)',

    tabIconActiveColor: 'rgba(255,158,0,1)',
    tabIconInactiveColor: "#939494",

    tabTextActiveColor: 'rgba(255,158,0,1)',
    tabTextInactiveColor: "#939494",

    modalBackgroundColor: 'white',
    modalBackgroundOverlayColor: 'black',

    dividerColor: '#edf0f4',
    borderColor: 'rgba(255, 255, 255, 0.1)',


    secondBackground: 'black',  // fixed
    unstableBackground: "rgba(255,255,255,0.1)",
    switchActiveBackground: 'rgba(255,255,255,0.1)',
    switchActive: 'rgba(255,158,0,1)',
    lineColor: 'rgba(255,255,255,0.1)',
    unstableText: 'white',
    backgroundHeader: 'black',
    red: 'red',

    home: {
        textWhite: 'white'
    },
    panel: {
        textWhite: 'white',
        buttonBackgroundBlackDisable: 'rgba(0,0,0,0.5)',
        backgroundColor: "white",
        tabTextActiveColor: 'rgba(255,158,0,1)',
        textBlackSecondary: '#9ea9b8',
        textBlack: 'black',
        backgroundColorOpacity: 'rgba(249,249,249,0.4)',
        mainColor: 'rgba(255,158,0,1)',
        backgroundColorGray: 'black',
    },
    smart: {
        stepperInactiveBackgroundColor: 'rgba(255, 255, 255, 0.1)'
    },

    backgroundColorListItem: 'black',
}

export default DARK;

