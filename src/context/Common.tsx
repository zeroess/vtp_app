/*
 * Created by duydatpham@gmail.com on 22/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import AsyncStorage from '@react-native-community/async-storage';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import query_home_configs_by_user_id from '../graphql/query_home_configs_by_user_id';
import { saveHomeConfig } from '../redux/actions/config';
import { fetchDeviceByHome } from '../redux/actions/device';
import { setListHome } from '../redux/actions/home';

type ContextProps = {
  currentUser: any,
  continueLogin: () => void
  logout: () => void,
};

export const CommonContext = React.createContext<Partial<ContextProps>>({});

export const CommonProvider = ({ children }: any) => {
  const dispatch = useDispatch()
  const [currentUser, setCurrentUser] = useState<any>()
  const [loading, setLoading] = useState<any>(true)

  const continueLogin = useCallback(async () => {

    await AsyncStorage.setItem('logined', "email")
  }, [])

  const _logout = useCallback(async () => {
    setCurrentUser(null)
    await AsyncStorage.clear()
  }, [])

  useEffect(() => {
    (async () => {
      try {
        console.log('user, homes::1')
        let logined = await AsyncStorage.getItem('logined')
        if (!logined) {
          throw Error('logined=false')
        }

      } catch (error) {
        console.log('error', error)
      } finally {
        setLoading(false)
      }
    })()
  }, [])

  useEffect(() => {
    if (!!currentUser) {
      console.log('currentUser.uid', currentUser.uid);
      (async () => {
      })()
    }
  }, [currentUser])

  return (
    <CommonContext.Provider
      value={{
        continueLogin,
        currentUser,
        logout: _logout,
      }}>
      {!loading && children}
    </CommonContext.Provider>
  );
};
