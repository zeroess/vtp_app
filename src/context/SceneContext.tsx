/*
 * Created by duydatpham@gmail.com on 22/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { useCallback, useEffect, useRef, useState } from 'react';
import { STATUS_DEVICE_FUNCTION, TAP_TO_RUN_ICON } from '../constants';
import { IC_SCENE } from '../../assets/images';

type ContextProps = {
  tasks: Array<any>
  setTasks: any
  type: number
  setType: any,
  whenTasks : Array<any>,
  setWhenTasks : any,
  setStatusDeviceFunction : any,
  statusDeviceFunction : string,
  setNameScene : any,
  nameScene : string,
  iconScene : any,
  setIconScene : any,
  scheduleTask : any,
  setScheduleTask : any,
};

export const SceneContext = React.createContext<Partial<ContextProps>>({});
export const SceneContextProvider = ({ children }: any) => {
  const [tasks, setTasks] = useState([])
  const [whenTasks, setWhenTasks] = useState([])
  const [scheduleTask, setScheduleTask] = useState([])
  const [type, setType] = useState(0)
  const [statusDeviceFunction,setStatusDeviceFunction] = useState(STATUS_DEVICE_FUNCTION.ADD) // trạng thái để naivgate màn hình , nếu sửa hoặc thêm device ở màn `detail` ? statusDeviceFunction === "FIX" : statusDeviceFunction === "ADD" (sửa `when` thì sẽ ko push sang màn `do` nữa)
  const [nameScene,setNameScene] = useState<string>("Name Scene")
  const [iconScene,setIconScene] = useState<string>(IC_SCENE[0])
  return (
    <SceneContext.Provider
      value={{
        tasks,
        setTasks,
        type, setType,
        statusDeviceFunction,setStatusDeviceFunction,
        whenTasks,setWhenTasks,
        nameScene,setNameScene,
        iconScene,setIconScene,
        scheduleTask, setScheduleTask,
      }}>
      {children}
    </SceneContext.Provider>
  );
};
