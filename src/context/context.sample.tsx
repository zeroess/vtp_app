/*
 * Created by duydatpham@gmail.com on 22/07/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import React, { useCallback, useEffect, useRef, useState } from 'react';

type ContextProps = {
};

export const CommonContext = React.createContext<Partial<ContextProps>>({});

export const CommonProvider = ({ children }: any) => {

  return (
    <CommonContext.Provider
      value={{
      }}>
      {children}
    </CommonContext.Provider>
  );
};
