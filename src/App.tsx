/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { PureComponent, useEffect, useState } from 'react';
import {
  View,
  Platform, UIManager, ActivityIndicator, Dimensions, StatusBar
} from 'react-native';
import Navigation from './navigation';

import { Provider } from 'react-redux';

import { ClientContextProvider } from 'react-fetching-library';
import { Client } from './api/Client';
import configStore from './redux/config-store';

import FlashMessage, { showMessage } from "react-native-flash-message";
const { width, height } = Dimensions.get('window')

import { ThemeProvider } from './context/theme';
import CodePushView from './components/CodePushView';
import { typography } from './typography'
import { HeaderSize } from './util';
import { CommonProvider } from './context/Common';
import DeviceStatusSubscription from './components/DeviceStatusSubscription';
import { requestLocation } from './util/location';


typography();

interface AppProps {

}
interface AppState {
  store?: any
}

class App extends PureComponent<AppProps, AppState> {

  constructor(props: AppProps) {
    super(props)
    this.state = {
      store: undefined
    }
  }

  async componentDidMount() {
    if (Platform.OS != 'web') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
      const SplashScreen = require('react-native-smart-splash-screen').default
      SplashScreen.close({
        animationType: SplashScreen.animationType.scale,
        duration: 850,
        // delay: 500,
      })
    }

    let _store = await configStore()
    console.log('_store = ', _store)
    this.setState({ store: _store })

    // requestLocation()

  }

  render() {
    return (
      <View style={Platform.select({
        web: {
          height, width: width / 2,
          // minHeight: 960, minWidth: 480,
          alignSelf: 'center'
        },
        ios: {
          flex: 1
        },
        android: {
          flex: 1
        }
      })} >
        {
          !this.state.store ? (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
              <ActivityIndicator />
            </View>
          ) : (
            <Provider store={this.state.store}>
              <ThemeProvider>
                <CommonProvider>
                  <ClientContextProvider client={Client}>
                    <Navigation />
                    {/* <DeviceStatusSubscription /> */}
                  </ClientContextProvider>
                </CommonProvider>
              </ThemeProvider>
            </Provider>
          )
        }
        <FlashMessage position="top" duration={5000}
          style={{
            // paddingTop: HeaderSize.paddingTop,
            minHeight: HeaderSize.height,
            justifyContent: 'center'
          }} />
        <CodePushView />
      </View >
    );
  }
};

export default App;
