/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from "../actions";
import { DELETE_CURRENT_HOME, FETCH_ROOM_BY_HOME_COMPLETE, SELECT_CURRENT_HOME, SET_LIST_HOME, UPDATE_HOME } from "../actions/home";

export interface HomeState {
    homeList: Array<any>
    currentHome?: any,
    roomList: any
}

const initialState: HomeState = {
    homeList: [],
    currentHome: undefined,
    roomList: []
}

export default function config(state: HomeState = initialState, action: ActionBase) {
    switch (action.type) {
        case FETCH_ROOM_BY_HOME_COMPLETE: {
            let roomList = (action.payload as any)
            roomList.sort((r1: any, r2: any) => r1.displayOrder - r2.displayOrder)
            return {
                ...state,
                roomList
            }
        }
        case UPDATE_HOME: {
            return {
                ...state,
                homeList: state.homeList.map(_home => {
                    if (_home.homeId != action.payload.homeId) {
                        return _home
                    }
                    return {
                        ..._home,
                        ...action.payload
                    }
                }),
                currentHome: action.payload.homeId == state.currentHome.homeId ? {
                    ...state.currentHome,
                    ...action.payload
                } : state.currentHome
            }
        }
        case DELETE_CURRENT_HOME: {
            let homeList = state.homeList.filter(_home => _home.homeId != state.currentHome.homeId)
            return {
                ...state,
                homeList,
                currentHome: homeList[0]
            }
        }
        case SELECT_CURRENT_HOME: {
            return {
                ...state,
                currentHome: action.payload
            }
        }
        case SET_LIST_HOME: {
            let currentHome = state.currentHome;
            if (!currentHome && action.payload.length > 0) {
                currentHome = action.payload[0]
            }
            return {
                ...state,
                homeList: action.payload || [],
                currentHome
            }
        }
        default:
            break;
    }
    return state
}