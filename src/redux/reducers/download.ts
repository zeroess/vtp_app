/*
 * Created by duydatpham@gmail.com on 26/08/2020
 * Copyright (c) 2020 duydatpham@gmail.com
 */


import { ActionBase } from "../actions";
import { DOWNLOAD_FILE_FINISHED, DOWNLOAD_FILE_PROGRESS } from "../sagas/helper";
import { DOWNLOAD_FILE } from "../actions/download";

export interface DownloadFileState {
    downloading: any;
}

const initialState: DownloadFileState = {
    downloading: {}
}

export default function config(state: DownloadFileState = initialState, action: ActionBase) {
    switch (action.type) {
        case DOWNLOAD_FILE: {
            const { id, progress } = action.payload
            return {
                ...state,
                downloading: {
                    ...state.downloading,
                    [id]: 0
                }
            }
        }
        case DOWNLOAD_FILE_PROGRESS: {
            const { _id, progress } = action.payload
            return {
                ...state,
                downloading: {
                    ...state.downloading,
                    [_id]: Math.min(progress, 100)
                }
            }
        }
        case DOWNLOAD_FILE_FINISHED: {
            const { _id } = action.payload
            return {
                ...state,
                downloading: {
                    ...state.downloading,
                    [_id]: undefined
                }
            }
        }
        default:
            break;
    }
    return state
}