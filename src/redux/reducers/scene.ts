/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from "../actions";
import { FETCH_SCENE_LIST_COMPLETE, FETCH_ENABLE_AUTOMATION_COMPLETE, FETCH_DISABLE_AUTOMATION_COMPLETE } from "../actions/scene";

export interface DeviceState {
    sceneList: Array<any>,
}

const initialState: DeviceState = {
    sceneList: [],
}

export default function config(state: DeviceState = initialState, action: ActionBase) {
    switch (action.type) {

        case FETCH_SCENE_LIST_COMPLETE: {
            return {
                ...state,
                sceneList: action?.payload
            }
        }
        case FETCH_ENABLE_AUTOMATION_COMPLETE: {
            const { sceneId, status } = action.payload
            return {
                ...state,
                sceneList: state.sceneList.map(_scene => {
                    if (_scene.id == sceneId)
                        return {
                            ..._scene,
                            enabled: status
                        }
                    return _scene
                })
            }
        }
        case FETCH_DISABLE_AUTOMATION_COMPLETE: {
            const { sceneId, status } = action.payload
            return {
                ...state,
                sceneList: state.sceneList.map(_scene => {
                    if (_scene.id == sceneId)
                        return {
                            ..._scene,
                            enabled: status
                        }
                    return _scene
                })
            }
        }
        default:
            break;
    }
    return state
}