/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import _ from "lodash";
import { ActionBase } from "../actions";
import { SAVE_TOKEN, SaveTokenAction, LOG_OUT, FETCH_USER_INFO_COMPLETE, SAVE_HOME_CONFIG, ADD_COLOR_TO_FAVORITE, REMOVE_COLOR_TO_FAVORITE, SAVE_COLOR_APP } from "../actions/config";
import { DOWNLOAD_FILE_FINISHED } from "../sagas/helper";
import { AppColor } from "../../constants";

export interface InfoUser {
    username: string;
    phone: string,
    email: string,
    full_name: string,
    info: string,
    address: string,
    avatar: string,
}

export interface ConfigState {
    access_token?: string;
    info_user?: InfoUser;
    homeConfig: any,
    favoriteColors: Array<string>,
    color_app : string
}

const initialState: ConfigState = {
    access_token: '',
    info_user: undefined,
    homeConfig: {},
    favoriteColors: ["#75f1ff", '#4a41ff', '#ff8ed5'],
    color_app : AppColor?.LIGHT
}

export default function config(state: ConfigState = initialState, action: ActionBase) {
    switch (action.type) {
        case SAVE_HOME_CONFIG: {
            const { homeId, config } = action.payload || {}
            if (!homeId) {
                return state;
            }
            return {
                ...state,
                homeConfig: {
                    ...state.homeConfig,
                    [homeId]: config
                }
            }
        }
        case REMOVE_COLOR_TO_FAVORITE: {
            return {
                ...state,
                favoriteColors: state.favoriteColors.filter(_color => _color != action.payload)
            }
        }
        case ADD_COLOR_TO_FAVORITE: {
            return {
                ...state,
                favoriteColors: state.favoriteColors.concat(action.payload)
            }
        }
        case FETCH_USER_INFO_COMPLETE: {
            return {
                ...state,
                info_user: action.payload
            }
        }
        case SAVE_TOKEN: {
            return {
                ...state,
                ...((action as SaveTokenAction).payload || {})
            }
        }
        case LOG_OUT: {
            return {
                ...(_.pick(state, ['favoriteColors'])),
                access_token: '',
                info_user: undefined
            }
        }
        case SAVE_COLOR_APP: {
            return {
                ...state,
                color_app: action.payload
            }
        }
        
        default:
            break;
    }
    return state
}