/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from '@react-native-community/async-storage'
import { StateType } from 'typesafe-actions'

import config from './config'
import home from './home'
import device from './device'
import scene from './scene'

const setupConfig = {
    key: 'account',
    storage: storage,
}
const rootReducer = combineReducers({
    config: persistReducer(setupConfig, config),
    home, device,scene
})

export type RootState = StateType<typeof rootReducer>

export default rootReducer;