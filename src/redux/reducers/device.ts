/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import _ from "lodash";
import { ActionBase } from "../actions";
import { DEVICE_CHANGE_STATUS, FETCH_CONDITION_DEV_LIST_COMPLETE, FETCH_DEVICE_BY_HOME_COMPLETE, FETCH_DEVICE_CONDITION_OPERATION_LIST_COMPLETE, ADD_DEVICES, REMOVE_DEVICES, CHANGE_STATUS_WAITTING_SCAN } from "../actions/device";

export interface DeviceAttribute {
    devId: string
    parentId: string
    iconUrl: string
    homeId: number
    switchDp: number
    name: string
    category: string
    schema: Array<any>
    schemaExt: Array<any>
    isOnline: boolean
    isCloudOnline: boolean
    isLocalOnline: boolean
    isMeshBleOnline: boolean
    isNewFirmware: boolean
    isShare: boolean
    roomId: number
    isGateway: boolean
    isZigbee: boolean
}

export interface DeviceState {
    deviceList: Array<DeviceAttribute>,
    deviceStatus: any,
    conditionDevList: Array<any>,
    deviceConditionOperationList: Array<any>,
    waittingScane: boolean
}

const initialState: DeviceState = {
    deviceList: [],
    deviceStatus: {},
    conditionDevList: [],
    deviceConditionOperationList: [],
    waittingScane: true
}

export default function config(state: DeviceState = initialState, action: ActionBase) {
    switch (action.type) {
        case CHANGE_STATUS_WAITTING_SCAN: {
            return {
                ...state,
                waittingScane: action.payload
            }
        }
        case FETCH_DEVICE_BY_HOME_COMPLETE: {
            const deviceStatus = { ...state.deviceStatus };
            let deviceList: Array<DeviceAttribute> = (action.payload || []).map((_device: any) => {
                deviceStatus[_device.devId] = _device.dps
                let schema = JSON.parse(_device.schema)
                let schemaExt = JSON.parse(_device.schemaExt);
                return {
                    ..._.pick(_device, [
                        'devId', 'iconUrl', 'homeId',
                        'switchDp', 'name', 'category',
                        'isCloudOnline', 'isLocalOnline',
                        'isMeshBleOnline', 'isNewFirmware',
                        'isOnline', 'isShare',
                        'roomId', 'parentId'
                    ]),
                    schema,
                    schemaExt: schema.filter((_point: any) => schemaExt.some((_ext: any) => _ext.id == _point.id)),
                    category: _device.category || _device.deviceCategory,
                    isGateway: !!_device?.meta?.zigBleSubEnable,
                    isZigbee: !!_device?.moduleMap?.zigbee
                }
            });
            return {
                ...state,
                deviceList,
                deviceStatus
            }
        }

        case ADD_DEVICES: {
            let hasAdd = false;
            const deviceStatus = { ...state.deviceStatus };
            let deviceList = state.deviceList.concat(action.payload.filter((dev: any) => !state.deviceList.some(_dev => _dev.devId == dev.devId)).map((_device: any) => {
                deviceStatus[_device.devId] = _device.dps
                let schema = JSON.parse(_device.schema)
                let schemaExt = JSON.parse(_device.schemaExt);
                hasAdd = true;
                return {
                    ..._.pick(_device, [
                        'devId', 'iconUrl', 'homeId',
                        'switchDp', 'name', 'category',
                        'isCloudOnline', 'isLocalOnline',
                        'isMeshBleOnline', 'isNewFirmware',
                        'isOnline', 'isShare',
                        'roomId', 'parentId'
                    ]),
                    schema,
                    schemaExt: schema.filter((_point: any) => schemaExt.some((_ext: any) => _ext.id == _point.id)),
                    category: _device.category || _device.deviceCategory,
                    isGateway: !!_device?.meta?.zigBleSubEnable,
                    isZigbee: !!_device?.moduleMap?.zigbee
                }
            }));
            if (hasAdd) {
                return {
                    ...state,
                    deviceList,
                    deviceStatus,
                    waittingScane: false
                }
            }
            return state;
        }
        case REMOVE_DEVICES: {
            let deviceList = state.deviceList.filter(_dev => !action.payload.some((_dev2: any) => _dev2.devId == _dev.devId))
            if (deviceList.length != state.deviceList.length) {
                return {
                    ...state,
                    deviceList,
                }
            }
            return state;
        }
        case FETCH_CONDITION_DEV_LIST_COMPLETE: {
            return {
                ...state,
                conditionDevList: action?.payload
            }
        }
        case FETCH_DEVICE_CONDITION_OPERATION_LIST_COMPLETE: {
            return {
                ...state,
                deviceConditionOperationList: action?.payload
            }
        }
        case DEVICE_CHANGE_STATUS: {
            let changed = false;
            const { devId, status } = action.payload;
            let currentStatus = state.deviceStatus[devId] || {}
            Object.keys(status).forEach(_key => {
                if (currentStatus[_key] != status[_key]) {
                    changed = true;
                }
            })

            if (!changed) {
                return state
            }
            return {
                ...state,
                deviceStatus: {
                    ...state.deviceStatus,
                    [devId]: {
                        ...currentStatus,
                        ...status
                    }
                }
            }
        }
        default:
            break;
    }
    return state
}