/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from "../actions";

export interface HomeState {
    homeList: Array<any>
    currentHome?: any
}

const initialState: HomeState = {
    homeList: [],
    currentHome: undefined
}

export default function config(state: HomeState = initialState, action: ActionBase) {
    switch (action.type) {
        default:
            break;
    }
    return state
}