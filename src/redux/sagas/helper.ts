/*
 * Created by duydatpham@gmail.com on 26/08/2020
 * Copyright (c) 2020 duydatpham@gmail.com
 */
import { eventChannel, END } from 'redux-saga';
// import RNFetch from 'rn-fetch-blob';
import { Platform } from 'react-native';


export const DOWNLOAD_FILE_PROGRESS = '__DOWNLOAD_FILE_PROGRESS__'
export const DOWNLOAD_FILE_FINISHED = '__DOWNLOAD_FILE_FINISHED__'
export const DOWNLOAD_FILE_ERROR = '__DOWNLOAD_FILE_ERROR__'

export interface ItemDownload {
    url: string;
    filePath: string;
}

// const folderPath = (Platform.OS == 'android' ? (RNFetch.fs.dirs.DownloadDir) : RNFetch.fs.dirs.DocumentDir)
export function downloadFileSagaHelper(_id: string, list: Array<ItemDownload>, otherPayload: any) {
    // console.log('downloadFileSagaHelper')
    // return eventChannel(emitter => {
    //     (async () => {
    //         try {
    //             let index = 0;
    //             let result = [];
    //             do {
    //                 result[index] = await RNFetch
    //                     .config({
    //                         path: `${folderPath}${list[index].filePath}`,
    //                         addAndroidDownloads: {
    //                             notification: false,
    //                             useDownloadManager: true,
    //                             title: 'Đang tải file ',
    //                             description: 'File downloaded by download manager.',
    //                             path: `${folderPath}${list[index].filePath}`,
    //                             mime: 'audio/mpeg'
    //                         },
    //                     })
    //                     .fetch('GET', list[index].url)
    //                     .progress({ interval: 500 }, (received, total) => {
    //                         let progress = index * 100.0 / list.length + (received * 100) / total / list.length;
    //                         console.log('DOWNLOAD_FILE::progress', received, total, progress)
    //                         // I chose to emit actions immediately     
    //                         emitter({
    //                             type: DOWNLOAD_FILE_PROGRESS,
    //                             payload: {
    //                                 _id, progress: progress
    //                             },
    //                         });
    //                     });
    //                 index++;
    //                 emitter({
    //                     type: DOWNLOAD_FILE_PROGRESS,
    //                     payload: {
    //                         _id, progress: index * 100.0 / list.length
    //                     },
    //                 });
    //             } while (index < list.length)
    //             console.log('DOWNLOAD_FILE::res', result[0].base64())
    //             result[0].base64().then(value=>{
    //                 console.log(value)

    //             })
    //             emitter({
    //                 type: DOWNLOAD_FILE_FINISHED,
    //                 payload: {
    //                     _id, files: result.map(_res => `${Platform.OS == 'ios' ? 'file:/' : ''}${_res.path()}`),
    //                     otherPayload
    //                 },
    //             });
    //             // END event has to be sent to signal that we are done with this channel
    //             emitter(END);
    //         } catch (error) {
    //             console.log('DOWNLOAD_FILE::error', error)
    //             emitter({
    //                 type: DOWNLOAD_FILE_ERROR,
    //                 payload: { _id, error },
    //             });
    //             // END event has to be sent to signal that we are done with this channel
    //             emitter(END);
    //         }
    //     })();

    //     // Promise.all(list.map(item => {
    //     //     const name = item.filePath;
    //     //     console.log('DOWNLOAD_FILE::name', name)
    //     //     return RNFetch.config({
    //     //         path: `${dirs.DocumentDir}${name}`,
    //     //         addAndroidDownloads: {
    //     //             useDownloadManager: true,
    //     //             title: 'My download',
    //     //             notification: true,
    //     //         },
    //     //     })
    //     //         .fetch('GET', item.url)
    //     //         .progress({ interval: 200 }, (received, total) => {
    //     //             currentProgress[name] = (received * 100) / total / list.length;
    //     //             console.log('DOWNLOAD_FILE::progress', currentProgress)
    //     //             // I chose to emit actions immediately     
    //     //             emitter({
    //     //                 type: DOWNLOAD_FILE_PROGRESS,
    //     //                 payload: {
    //     //                     _id, currentProgress: Object.values(currentProgress).reduce((total: any, num: any) => {
    //     //                         return total + num;
    //     //                     })
    //     //                 },
    //     //             });
    //     //         })
    //     // })).then(res => {
    //     //     console.log('DOWNLOAD_FILE::res', res)
    //     //     emitter({
    //     //         type: DOWNLOAD_FILE_FINISHED,
    //     //         payload: { _id, files: res.map(_res => _res.path()) },
    //     //     });
    //     //     // END event has to be sent to signal that we are done with this channel
    //     //     emitter(END);
    //     // })
    //     //     .catch(error => {
    //     //         console.log('DOWNLOAD_FILE::error', error)
    //     //         emitter({
    //     //             type: DOWNLOAD_FILE_ERROR,
    //     //             payload: { _id, error },
    //     //         });
    //     //         // END event has to be sent to signal that we are done with this channel
    //     //         emitter(END);
    //     //     });
    //     // RNFetch.config({
    //     //     path: `${dirs.DocumentDir}${}`,
    //     //     addAndroidDownloads: {
    //     //         useDownloadManager: true,
    //     //         title: 'My download',
    //     //         notification: true,
    //     //     },
    //     // })
    //     //     .fetch('GET', url)
    //     //     .progress({ interval: 200 }, (received, total) => {
    //     //         currentProgress = currentProgress + (received * 100) / total / list.length;
    //     //         console.log('DOWNLOAD_FILE::progress', currentProgress)
    //     //         // I chose to emit actions immediately     
    //     //         emitter({
    //     //             type: DOWNLOAD_FILE_PROGRESS,
    //     //             payload: { _id, currentProgress },
    //     //         });
    //     //     })
    //     //     .then(res => {
    //     //         console.log('DOWNLOAD_FILE::res', res)
    //     //         emitter({
    //     //             type: DOWNLOAD_FILE_FINISHED,
    //     //             payload: { _id, filePath: res.path() },
    //     //         });
    //     //         // END event has to be sent to signal that we are done with this channel
    //     //         emitter(END);
    //     //     })
    //     //     .catch(error => {
    //     //         console.log('DOWNLOAD_FILE::error', error)
    //     //         // Optional but you can deal with the error here
    //     //         throw error;
    //     //     });

    //     // The returned method can be called to cancel the channel
    //     return () => {
    //         //   task.cancel();
    //         console.log('cancel')
    //     };
    // });

}
