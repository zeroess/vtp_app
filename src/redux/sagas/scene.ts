/**
* Created by duydatpham on Thu May 31 2018
* Copyright (c) 2018 duydatpham
*/

import { put, takeEvery, takeLatest } from "redux-saga/effects";
import { fetchSceneListComplete, FETCH_SCENE_LIST_ACTION, FETCH_ENABLE_AUTOMATION_ACTION, fetchEnableAutomationComplete, FETCH_DISABLE_AUTOMATION_ACTION, fetchDisableAutomationComplete } from "../actions/scene";
import { showMessage } from "react-native-flash-message";

function* _fetchSceneList(action: any) {
  try {
  } catch (error) {
    yield put(fetchSceneListComplete([]))
  } finally {
    // Optional
  }
}

function* _fetchEnableAutomation(action: any) {
  try {
  } catch (error) {
    yield put(fetchEnableAutomationComplete({ sceneId: action.payload, status: false }))
    showMessage({
      type: 'success',
      message: "enableSmartScene fail"
    })
  } finally {
    // Optional
  }
}

function* _fetchDisableAutomation(action: any) {
  try {
  } catch (error) {
    yield put(fetchDisableAutomationComplete({ sceneId: action.payload, status: true }))
    showMessage({
      type: 'success',
      message: "enableSmartScene fail"
    })
  } finally {
    // Optional
  }
}

export function* watchFetchDevice() {
  yield takeLatest(FETCH_SCENE_LIST_ACTION, _fetchSceneList)
  yield takeLatest(FETCH_ENABLE_AUTOMATION_ACTION, _fetchEnableAutomation)
  yield takeLatest(FETCH_DISABLE_AUTOMATION_ACTION, _fetchDisableAutomation)
}

