/**
* Created by duydatpham on Thu May 31 2018
* Copyright (c) 2018 duydatpham
*/

import { takeLatest, select, put, call, take, takeEvery } from "redux-saga/effects";
import { delay } from 'redux-saga/effects';
import { ActionBase } from "../actions";
import { downloadFileSagaHelper } from "./helper";
import { DOWNLOAD_FILE, DownloadFileAction } from "../actions/download";

function* downloadFileSaga(action: DownloadFileAction) {
  const { id, list, otherPayload } = action.payload;

  try {
    const channel = yield call(downloadFileSagaHelper, id, list, otherPayload);
    // take(END) will cause the saga to terminate by jumping to the finally block
    while (true) {
      // Remember, our helper only emits actions
      // Thus we can directly "put" them
      const action = yield take(channel);
      yield put(action);
    }
  } catch (error) {
    console.log('error', error)
  } finally {
    // Optional
  }
}
export function* watchDownloadFile() {
  yield takeEvery(DOWNLOAD_FILE, downloadFileSaga)
}

