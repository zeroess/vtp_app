/**
* Created by duydatpham on Thu May 31 2018
* Copyright (c) 2018 duydatpham
*/

import { put, takeEvery } from "redux-saga/effects";
import { fetchConditionDevListComplete, fetchDeviceByHomeComplete, fetchDeviceConditionOperationListComplete, FETCH_CONDITION_DEV_LIST, FETCH_DEVICE_BY_HOME, FETCH_DEVICE_CONDITION_OPERATION_LIST } from "../actions/device";
import { fetchRoom } from "../actions/home";

function* _fetchDevice(action: any) {
  console.log('_fetchDevice:home:payload', action.payload)
  try {
  } catch (error) {
    yield put(fetchDeviceByHomeComplete([]))
  } finally {
    // Optional
    yield put(fetchRoom(action.payload))
  }
}

function* _fetchConditionDevList(action: any) {
  try {
  } catch (error) {
    yield put(fetchConditionDevListComplete([]))
  } finally {
    // Optional
  }
}

function* _fetchDeviceConditionOperationList(action: any) {
  try {
  } catch (error) {
    yield put(fetchDeviceConditionOperationListComplete([]))
  } finally {
    // Optional
  }
}
export function* watchFetchDevice() {
  yield takeEvery(FETCH_DEVICE_BY_HOME, _fetchDevice)
  yield takeEvery(FETCH_CONDITION_DEV_LIST, _fetchConditionDevList)
  yield takeEvery(FETCH_DEVICE_CONDITION_OPERATION_LIST, _fetchDeviceConditionOperationList)
}

