/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import { all, fork } from 'redux-saga/effects';
import * as device from './device'
import * as home from './home'
import * as scene from './scene'
export default function* rootSaga() {
    yield all([
        ...Object.values(device),
        ...Object.values(home),
        ...Object.values(scene),
    ].map(fork));
}