/**
* Created by duydatpham on Thu May 31 2018
* Copyright (c) 2018 duydatpham
*/

import { put, takeEvery, takeLatest } from "redux-saga/effects";
import { fetchRoomComplete, FETCH_ROOM_BY_HOME } from "../actions/home";

function* _fetchRoom(action: any) {
  try {
  } catch (error) {
    yield put(fetchRoomComplete([]))
  } finally {
    // Optional
  }
}
export function* watchFetchRoom() {
  yield takeLatest(FETCH_ROOM_BY_HOME, _fetchRoom)
}

