
/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import { ActionBase } from ".";

export interface SaveTokenAction extends ActionBase {
    payload: object;
}

export const SAVE_TOKEN = 'action/config/SAVE_TOKEN'
export const LOG_OUT = 'action/config/LOG_OUT'

export const FETCH_USER_INFO = 'action/config/FETCH_USER_INFO'
export const FETCH_USER_INFO_COMPLETE = 'action/config/FETCH_USER_INFO_COMPLETE'

export const SAVE_HOME_CONFIG = 'action/config/SAVE_HOME_CONFIG'
export const SAVE_COLOR_APP = 'action/config/SAVE_COLOR_APP'
export const ADD_COLOR_TO_FAVORITE = 'action/config/ADD_COLOR_TO_FAVORITE'
export const REMOVE_COLOR_TO_FAVORITE = 'action/config/REMOVE_COLOR_TO_FAVORITE'


export const removeColorToFavorite = (payload: any) => {
    return {
        type: REMOVE_COLOR_TO_FAVORITE,
        payload
    }
}

export const addColorToFavorite = (payload: any) => {
    return {
        type: ADD_COLOR_TO_FAVORITE,
        payload
    }
}

export const fetchUserInfo = () => {
    return {
        type: FETCH_USER_INFO,
    }
}
export const fetchUserInfoComplete = (payload: any) => {
    return {
        type: FETCH_USER_INFO_COMPLETE,
        payload
    }
}

export const saveToken = (payload: object): SaveTokenAction => {
    return {
        type: SAVE_TOKEN,
        payload
    }
}

export const saveHomeConfig = (payload: object) => {
    return {
        type: SAVE_HOME_CONFIG,
        payload
    }
}

export const logout = () => {
    return {
        type: LOG_OUT,
    }
}

export const saveColorApp = (payload : any) => {
    return {
        type : SAVE_COLOR_APP,
        payload
    }
}
