
/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from ".";

export interface SaveTokenAction extends ActionBase {
    payload: object;
}


export const FETCH_SCENE_LIST_ACTION = 'action/device/FETCH_SCENE_LIST_ACTION'
export const FETCH_SCENE_LIST_COMPLETE = 'action/device/FETCH_SCENE_LIST_COMPLETE'
export const FETCH_ENABLE_AUTOMATION_ACTION = 'action/device/FETCH_ENABLE_AUTOMATION_ACTION'
export const FETCH_ENABLE_AUTOMATION_COMPLETE = 'action/device/FETCH_ENABLE_AUTOMATION_COMPLETE'
export const FETCH_DISABLE_AUTOMATION_ACTION = 'action/device/FETCH_DISABLE_AUTOMATION_ACTION'
export const FETCH_DISABLE_AUTOMATION_COMPLETE = 'action/device/FETCH_DISABLE_AUTOMATION_COMPLETE'

export const fetchSceneListAction = (payload: string) => {
    return {
        type: FETCH_SCENE_LIST_ACTION,
        payload
    }
}
export const fetchSceneListComplete = (payload: any) => {
    return {
        type: FETCH_SCENE_LIST_COMPLETE,
        payload
    }
}

export const fetchEnableAutomationAction = (payload: string) => {
    return {
        type: FETCH_ENABLE_AUTOMATION_ACTION,
        payload
    }
}
export const fetchEnableAutomationComplete = (payload: any) => {
    return {
        type: FETCH_ENABLE_AUTOMATION_COMPLETE,
        payload
    }
}

export const fetchDisableAutomationAction = (payload: string) => {
    return {
        type: FETCH_DISABLE_AUTOMATION_ACTION,
        payload
    }
}
export const fetchDisableAutomationComplete = (payload: any) => {
    return {
        type: FETCH_DISABLE_AUTOMATION_COMPLETE,
        payload
    }
}