import { ActionBase } from "."
import { ItemDownload } from "../sagas/helper"

/*
 * Created by duydatpham@gmail.com on 26/08/2020
 * Copyright (c) 2020 duydatpham@gmail.com
 */
export const DOWNLOAD_FILE = 'action/download/DOWNLOAD_FILE'


export interface DownloadFileAction extends ActionBase {
    payload: {
        id: string;
        list: Array<ItemDownload>;
        otherPayload?: any
    };
}

export const download = (id: string, list: Array<ItemDownload>, otherPayload: any): DownloadFileAction => {
    return {
        type: DOWNLOAD_FILE,
        payload: { id, list, otherPayload }
    }
}