/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

export interface ActionBase {
    type: string;
    payload?: any
}