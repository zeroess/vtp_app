
/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from ".";

export interface SaveTokenAction extends ActionBase {
    payload: object;
}

export const FETCH_DEVICE_BY_HOME = 'action/device/FETCH_DEVICE_BY_HOME'
export const FETCH_DEVICE_BY_HOME_COMPLETE = 'action/device/FETCH_DEVICE_BY_HOME_COMPLETE'
export const FETCH_CONDITION_DEV_LIST = 'action/device/FETCH_CONDITION_DEV_LIST'
export const FETCH_CONDITION_DEV_LIST_COMPLETE = 'action/device/FETCH_CONDITION_DEV_LIST_COMPLETE'

export const FETCH_DEVICE_CONDITION_OPERATION_LIST = 'action/device/FETCH_DEVICE_CONDITION_OPERATION_LIST'
export const FETCH_DEVICE_CONDITION_OPERATION_LIST_COMPLETE = 'action/device/FETCH_DEVICE_CONDITION_OPERATION_LIST_COMPLETE'

export const DEVICE_CHANGE_STATUS = 'action/device/DEVICE_CHANGE_STATUS'

export const ADD_DEVICES = 'action/device/ADD_DEVICES'
export const REMOVE_DEVICES = 'action/device/REMOVE_DEVICES'

export const CHANGE_STATUS_WAITTING_SCAN = 'action/device/CHANGE_STATUS_WAITTING_SCAN'


export const changeStatusWaittingScan = (payload: any) => {
    return {
        type: CHANGE_STATUS_WAITTING_SCAN,
        payload
    }
}

export const deviceChangeStatus = (payload: any) => {
    return {
        type: DEVICE_CHANGE_STATUS,
        payload
    }
}

export const addDevices = (payload: any) => {
    return {
        type: ADD_DEVICES,
        payload
    }
}

export const removeDevices = (payload: any) => {
    return {
        type: REMOVE_DEVICES,
        payload
    }
}

export const fetchDeviceByHome = (payload: any) => {
    return {
        type: FETCH_DEVICE_BY_HOME,
        payload
    }
}
export const fetchDeviceByHomeComplete = (payload: any) => {
    return {
        type: FETCH_DEVICE_BY_HOME_COMPLETE,
        payload
    }
}

export const fetchConditionDevList = (payload: any) => {
    return {
        type: FETCH_CONDITION_DEV_LIST,
        payload
    }
}
export const fetchConditionDevListComplete = (payload: any) => {
    return {
        type: FETCH_CONDITION_DEV_LIST_COMPLETE,
        payload
    }
}

export const fetchDeviceConditionOperationList = (payload: string) => {
    return {
        type: FETCH_DEVICE_CONDITION_OPERATION_LIST,
        payload
    }
}
export const fetchDeviceConditionOperationListComplete = (payload: any) => {
    return {
        type: FETCH_DEVICE_CONDITION_OPERATION_LIST_COMPLETE,
        payload
    }
}