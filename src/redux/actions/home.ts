
/*
 * Created by duydatpham@gmail.com on 04/10/2021
 * Copyright (c) 2021 duydatpham@gmail.com
 */

import { ActionBase } from ".";

export interface SaveTokenAction extends ActionBase {
    payload: object;
}

export const SET_LIST_HOME = 'action/home/SET_LIST_HOME'
export const SELECT_CURRENT_HOME = 'action/home/SELECT_CURRENT_HOME'
export const DELETE_CURRENT_HOME = 'action/home/DELETE_CURRENT_HOME'
export const UPDATE_HOME = 'action/home/UPDATE_HOME'

export const FETCH_ROOM_BY_HOME = 'action/home/FETCH_ROOM_BY_HOME'
export const FETCH_ROOM_BY_HOME_COMPLETE = 'action/home/FETCH_ROOM_BY_HOME_COMPLETE'


export const fetchRoom = (payload: any) => {
    return {
        type: FETCH_ROOM_BY_HOME,
        payload
    }
}
export const fetchRoomComplete = (payload: any) => {
    return {
        type: FETCH_ROOM_BY_HOME_COMPLETE,
        payload
    }
}

export const setListHome = (payload: any) => {
    return {
        type: SET_LIST_HOME,
        payload
    }
}

export const selectCurrentHome = (payload: any) => {
    return {
        type: SELECT_CURRENT_HOME,
        payload
    }
}

export const deleteCurrentHome = () => {
    return {
        type: DELETE_CURRENT_HOME,
    }
}

export const updateCurrentHome = (payload: any) => {
    return {
        type: UPDATE_HOME,
        payload
    }
}