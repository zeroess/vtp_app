/**
* Created by duydatpham@gmail.com on Thu Oct 18 2018
* Copyright (c) 2018 duydatpham@gmail.com
*/

import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';
import reducer from './reducers';
import sagas from './sagas';
import { createLogger } from 'redux-logger'
import { Platform } from 'react-native';

const isDebuggingInChrome = Platform.OS == 'web' || __DEV__;

var logger = createLogger({
    predicate: (getState: any, action: any) => isDebuggingInChrome && action.type != 'action/device/DEVICE_CHANGE_STATUS',
    collapsed: true,
    duration: true
})

const sagaMiddleware = createSagaMiddleware();

export default () => {
    return new Promise((reslove) => {
        const store = createStore(
            reducer,
            undefined,
            compose(
                applyMiddleware(sagaMiddleware, logger)
            )
        )

        persistStore(
            store,
            undefined,
            () => {
                reslove(store)
            }
        )

        sagaMiddleware.run(sagas);
    })
}
