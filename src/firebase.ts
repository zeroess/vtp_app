// import firebase from '@react-native-firebase/app'

import { firebase, FirebaseAuthTypes, } from '@react-native-firebase/auth';

const sAuth = firebase.auth();
// sAuth.languageCode = 'vi';


export const signInWithEmailAndPassword = (
  email: string,
  password: string
): Promise<FirebaseAuthTypes.UserCredential> => {
  return sAuth.signInWithEmailAndPassword(email, password);
};

export const signInWithCredential = (credential: FirebaseAuthTypes.AuthCredential) => {
  return sAuth.signInWithCredential(credential);
};

export const auth = sAuth;
export default firebase;
